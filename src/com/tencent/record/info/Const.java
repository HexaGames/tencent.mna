/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : Const.java<br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : Constants And Designtime Configs of Whole Wns Frameworks<br>
 */
package com.tencent.record.info;

import com.tencent.record.debug.TraceLevel;

import java.io.File;

/**
 * @author 
 */
public final class Const {
    /**
     * 调试常量
     */
    public static interface Debug {
        public boolean Enabled = true; // 调试全局开关
        public boolean FileTracerEnabled = true; // 文件日志开关
        public boolean InfiniteTraceFile = false; // 文件日志无限记录开关
        public boolean LogcatTracerEnabled = false; // Logcat开关
        public boolean NeedAttached = false; // 是否允许服务等待调试器
        public boolean ShowErrorCode = false; // 错误提示信息显示错误码

        public String FileTracerName = "CloudSdk.File.Tracer";
        public String ClientFileTracerName = "CloudSdk.Client.File.Tracer";
        public String FileRoot = "Tencent" + File.separator + "CloudSdk" + File.separator + "Logs";
        public String FileExt = ".CloudSdk.log";
        public String ClientFileExt = ".CloudSdk.log";
        public long MinSpaceRequired = 8 * 1024 * 1024; // 外存最小空间要求：8M
        public int FileBlockSize = 256 * 1024; // 固定分片大小：256K
        public int DataThreshold = 8 * 1024; // 数据量阈值：默认8K，单位字符
        public int TimeThreshold = 10 * 1000; // 时间阈值：

        public String FileBlockCount = "debug.file.blockcount";
        public String FileKeepPeriod = "debug.file.keepperiod";
        public String FileTraceLevel = "debug.file.tracelevel";

        public int DefFileBlockCount = 24; // 6MB
        public int DefFileTraceLevel = TraceLevel.INFO_AND_ABOVE; // 全部打印
        public long DefFileKeepPeriod = 7 * 24 * 60 * 60 * 1000L; // 7天
    }

}
