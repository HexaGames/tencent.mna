/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : Option.java<br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : Shared Option Values For Wns Service and Client<br>
 */
package com.tencent.record.info;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

import java.util.Map;

/**
 * 模块的、固化的、可共享的、关于WNS的配置信息
 *
 * @author 
 */
public final class Option {
    // ------------------------------------------------------------------------------
    // 功能实现 via android.content.SharedPreferences
    // ------------------------------------------------------------------------------
    private static final String TAG = "options.for." + Global.getPackageName();
    private static SharedPreferences preferences = Global.getSharedPreferences(TAG, 0);
    private static SharedPreferences.Editor editor = preferences.edit();

    public static Map<String, ?> getAll() {
        return preferences.getAll();
    }

    public static String getString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    public static int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    public static long getLong(String key, long defValue) {

        return preferences.getLong(key, defValue);
    }

    public static float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    public static boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    public static boolean contains(String key) {
        return preferences.contains(key);
    }

    public static void startListen(OnSharedPreferenceChangeListener listener) {
        preferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public static void stopListen(OnSharedPreferenceChangeListener listener) {
        preferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public static Editor putString(String key, String value) {
        return editor.putString(key, value);
    }

    public static Editor putInt(String key, int value) {
        return editor.putInt(key, value);
    }

    public static Editor putLong(String key, long value) {
        return editor.putLong(key, value);
    }

    public static Editor putFloat(String key, float value) {
        return editor.putFloat(key, value);
    }

    public static Editor putBoolean(String key, boolean value) {
        return editor.putBoolean(key, value);
    }

    public static Editor remove(String key) {
        return editor.remove(key);
    }

    public static Editor clear() {
        return editor.clear();
    }

    public static boolean commit() {
        return editor.commit();
    }

}
