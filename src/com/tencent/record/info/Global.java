/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : Base.java <br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved. <br>
 * Description : Like "System" class in Java <br>
 *
 */
package com.tencent.record.info;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 全局运行时环境<br>
 * <br>
 * 该类静态包装了{@link android.content.ContextWrapper}的全部方法，可以在不存在或者不方便传递
 * {@code Context} 的情况下使用当前的{@code Application}作为{@code Context}<br>
 * <p/>
 * <pre>
 * e.g.
 *
 * public boolean updateNetworkInfo()
 * {
 * 	#//获得Android连通性服务实例
 * 	ConnectivityManager manager = (ConnectivityManager) Global.getSystemService(Context.CONNECTIVITY_SERVICE);
 *
 * 	NetworkInfo info = manager.getActiveNetworkInfo();
 * }
 * </pre>
 * <p/>
 * ① 若没有自定义{@code Application}的需要，请在AndroidManifest.xml中设定其 android:name
 * 属性为com.tencent.base.BaseApplication<br>
 * <br>
 * ② 若已经使用其他的{@code Application}的子类作为自己的Application，请在使用BASE库之前， 在其
 * {@code Application.onCreate()} 方法中调用 {@code Global.init(Application)} <br>
 * <br>
 * 若没有初始化{@code Global}，使用本类的静态方法会得到{@link BaseLibException} 的运行时异常，请检查
 * {@Application}的初始化代码或AndroidManifest.xml中的声明
 *
 * @author 
 */
public final class Global {
    private static Context context;

    public final static void init(Context ctx) {
        setContext(ctx);
    }

    public final static Context getContext() {
        if (context == null) {
            return null;
        }

        return context;
    }

    public final static void setContext(Context context) {
        Global.context = context;
    }

    /**
     * 判断当前进程是否是主进程<br>
     * <br>
     * <i>子进程的名称包含':'，以此为依据</i>
     *
     * @return 是主进程，或不是主进程<s>，这是一个问题</s>
     */
    public final static boolean isMainProcess() {
        String processName = currentProcessName();

        return processName == null ? false : (processName.indexOf(':') < 1);
    }

    /**
     * 获得当前进程的进程名 <br>
     * <br>
     * <b>这个过程包含用轮询实现，所以不要总是使用</b>
     *
     * @return 当前进程的进程名，任何异常情况将得到 null
     */
    public final static String currentProcessName() {
        ActivityManager manager = (ActivityManager) Global.getSystemService(Context.ACTIVITY_SERVICE);

        if (manager == null) {
            return null;
        }

        List<RunningAppProcessInfo> processInfos = manager.getRunningAppProcesses();

        if (processInfos == null) {
            return null;
        }

        int pid = Process.myPid();

        for (RunningAppProcessInfo processInfo : processInfos) {
            if (pid == processInfo.pid) {
                return processInfo.processName;
            }
        }

        return null;
    }

    /*
     * 下面为 Android.Context 的同名静态方法包装 ↓
     */
    public final static AssetManager getAssets() {
        return getContext().getAssets();
    }

    public final static Resources getResources() {
        return getContext().getResources();
    }

    public final static PackageManager getPackageManager() {
        return getContext().getPackageManager();
    }

    public final static ContentResolver getContentResolver() {
        return getContext().getContentResolver();
    }

    public final static Looper getMainLooper() {
        return getContext().getMainLooper();
    }

    public final static Context getApplicationContext() {
        return getContext().getApplicationContext();
    }

    public final static void setTheme(int resid) {
        getContext().setTheme(resid);
    }

    public final static Resources.Theme getTheme() {
        return getContext().getTheme();
    }

    public final static ClassLoader getClassLoader() {
        return getContext().getClassLoader();
    }

    public final static String getPackageName() {
        return getContext().getPackageName();
    }

    public final static ApplicationInfo getApplicationInfo() {
        return getContext().getApplicationInfo();
    }

    public final static String getPackageResourcePath() {
        return getContext().getPackageResourcePath();
    }

    public final static String getPackageCodePath() {
        return getContext().getPackageCodePath();
    }

    public final static SharedPreferences getSharedPreferences(String name, int mode) {
        return getContext().getSharedPreferences(name, mode);
    }

    public final static FileInputStream openFileInput(String name) throws FileNotFoundException {
        return getContext().openFileInput(name);
    }

    public final static FileOutputStream openFileOutput(String name, int mode) throws FileNotFoundException {
        return getContext().openFileOutput(name, mode);
    }

    public final static boolean deleteFile(String name) {
        return getContext().deleteFile(name);
    }

    public final static File getFileStreamPath(String name) {
        return getContext().getFileStreamPath(name);
    }

    public final static String[] fileList() {
        return getContext().fileList();
    }

    public final static File getFilesDir() {
        return getContext().getFilesDir();
    }

    public final static File getExternalFilesDir(String type) {
        return getContext().getExternalFilesDir(type);
    }

    public final static File getCacheDir() {
        return getContext().getCacheDir();
    }

    public final static File getExternalCacheDir() {
        return getContext().getExternalCacheDir();
    }

    public final static File getDir(String name, int mode) {
        return getContext().getDir(name, mode);
    }

    @Deprecated
    public final static Drawable getWallpaper() {
        return getContext().getWallpaper();
    }

    @Deprecated
    public final static Drawable peekWallpaper() {
        return getContext().peekWallpaper();
    }

    @Deprecated
    public final static int getWallpaperDesiredMinimumWidth() {
        return getContext().getWallpaperDesiredMinimumWidth();
    }

    @Deprecated
    public final static int getWallpaperDesiredMinimumHeight() {
        return getContext().getWallpaperDesiredMinimumHeight();
    }

    @Deprecated
    public final static void setWallpaper(Bitmap bitmap) throws IOException {
        getContext().setWallpaper(bitmap);
    }

    @Deprecated
    public final static void setWallpaper(InputStream data) throws IOException {
        getContext().setWallpaper(data);
    }

    @Deprecated
    public final static void clearWallpaper() throws IOException {
        getContext().clearWallpaper();
    }

    public final static void startActivity(Intent intent) {
        getContext().startActivity(intent);
    }

    public final static void startIntentSender(IntentSender intent, Intent fillInIntent, int flagsMask,
                                               int flagsValues, int extraFlags) throws IntentSender.SendIntentException {
        getContext().startIntentSender(intent, fillInIntent, flagsMask, flagsValues, extraFlags);
    }

    public final static void sendBroadcast(Intent intent) {
        getContext().sendBroadcast(intent);
    }

    public final static void sendBroadcast(Intent intent, String receiverPermission) {
        getContext().sendBroadcast(intent, receiverPermission);
    }

    public final static void sendOrderedBroadcast(Intent intent, String receiverPermission) {
        getContext().sendOrderedBroadcast(intent, receiverPermission);
    }

    public final static void sendOrderedBroadcast(Intent intent, String receiverPermission,
                                                  BroadcastReceiver resultReceiver, Handler scheduler, int initialCode, String initialData,
                                                  Bundle initialExtras) {
        getContext().sendOrderedBroadcast(intent, receiverPermission, resultReceiver, scheduler, initialCode,
                initialData, initialExtras);
    }

    public final static void sendStickyBroadcast(Intent intent) {
        getContext().sendStickyBroadcast(intent);
    }

    public final static void sendStickyOrderedBroadcast(Intent intent, BroadcastReceiver resultReceiver,
                                                        Handler scheduler, int initialCode, String initialData, Bundle initialExtras) {
        getContext().sendStickyOrderedBroadcast(intent, resultReceiver, scheduler, initialCode, initialData,
                initialExtras);
    }

    public final static void removeStickyBroadcast(Intent intent) {
        getContext().removeStickyBroadcast(intent);
    }

    public final static Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        return getContext().registerReceiver(receiver, filter);
    }

    public final static Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter,
                                                String broadcastPermission, Handler scheduler) {
        return getContext().registerReceiver(receiver, filter, broadcastPermission, scheduler);
    }

    public final static void unregisterReceiver(BroadcastReceiver receiver) {
        getContext().unregisterReceiver(receiver);
    }

    public final static ComponentName startService(Intent service) {
        return getContext().startService(service);
    }

    public final static boolean stopService(Intent name) {
        return getContext().stopService(name);
    }

    public final static boolean bindService(Intent service, ServiceConnection conn, int flags) {
        return getContext().bindService(service, conn, flags);
    }

    public final static void unbindService(ServiceConnection conn) {
        getContext().unbindService(conn);
    }

    public final static boolean startInstrumentation(ComponentName className, String profileFile, Bundle arguments) {
        return getContext().startInstrumentation(className, profileFile, arguments);
    }

    public final static Object getSystemService(String name) {
        return getContext().getSystemService(name);
    }

    public final static int checkPermission(String permission, int pid, int uid) {
        return getContext().checkPermission(permission, pid, uid);
    }

    public final static int checkCallingPermission(String permission) {
        return getContext().checkCallingPermission(permission);
    }

    public final static int checkCallingOrSelfPermission(String permission) {
        return getContext().checkCallingOrSelfPermission(permission);
    }

    public final static void enforcePermission(String permission, int pid, int uid, String message) {
        getContext().enforcePermission(permission, pid, uid, message);
    }

    public final static void enforceCallingPermission(String permission, String message) {
        getContext().enforceCallingPermission(permission, message);
    }

    public final static void enforceCallingOrSelfPermission(String permission, String message) {
        getContext().enforceCallingOrSelfPermission(permission, message);
    }

    public final static void grantUriPermission(String toPackage, Uri uri, int modeFlags) {
        getContext().grantUriPermission(toPackage, uri, modeFlags);
    }

    public final static void revokeUriPermission(Uri uri, int modeFlags) {
        getContext().revokeUriPermission(uri, modeFlags);
    }

    public final static int checkUriPermission(Uri uri, int pid, int uid, int modeFlags) {
        return getContext().checkUriPermission(uri, pid, uid, modeFlags);
    }

    public final static int checkCallingUriPermission(Uri uri, int modeFlags) {
        return getContext().checkCallingUriPermission(uri, modeFlags);
    }

    public final static int checkCallingOrSelfUriPermission(Uri uri, int modeFlags) {
        return getContext().checkCallingOrSelfUriPermission(uri, modeFlags);
    }

    public final static int checkUriPermission(Uri uri, String readPermission, String writePermission, int pid,
                                               int uid, int modeFlags) {
        return getContext().checkUriPermission(uri, readPermission, writePermission, pid, uid, modeFlags);
    }

    public final static void enforceUriPermission(Uri uri, int pid, int uid, int modeFlags, String message) {
        getContext().enforceUriPermission(uri, pid, uid, modeFlags, message);
    }

    public final static void enforceCallingUriPermission(Uri uri, int modeFlags, String message) {
        getContext().enforceCallingUriPermission(uri, modeFlags, message);
    }

    public final static void enforceCallingOrSelfUriPermission(Uri uri, int modeFlags, String message) {
        getContext().enforceCallingOrSelfUriPermission(uri, modeFlags, message);
    }

    public final static void enforceUriPermission(Uri uri, String readPermission, String writePermission, int pid,
                                                  int uid, int modeFlags, String message) {
        getContext().enforceUriPermission(uri, readPermission, writePermission, pid, uid, modeFlags, message);
    }

    public final static Context createPackageContext(String packageName, int flags)
            throws PackageManager.NameNotFoundException {
        return getContext().createPackageContext(packageName, flags);
    }

    public final static boolean isRestricted() {
        return getContext().isRestricted();
    }
}
