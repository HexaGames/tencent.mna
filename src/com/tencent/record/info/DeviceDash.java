/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : DeviceDash.java<br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : Device Info for Lazy Develops of Clients<br>
 */
package com.tencent.record.info;

/**
 * 设备信息获取类
 * <p/>
 * 为了保证入口的简洁和代码的可读性，请使用{@link com.tencent.base.os.Device#getInfo()}方法
 *
 * @author , 
 */
public class DeviceDash {
//    private static final DeviceDash instance = new DeviceDash();
//
//    public static DeviceDash getInstance()
//    {
//        return instance;
//    }
//
//    public DeviceDash()
//    {
//    }
//
//
//    private String getStorageInfo()
//    {
//        StorageInfo innerInfo = StorageDash.getInnerInfo();
//        StorageInfo extInfo = StorageDash.getExternalInfo();
//
//        String resu = String.format("{IN : %s |EXT: %s}", (innerInfo == null) ? "N/A" : innerInfo.toString(),
//                (extInfo == null) ? "N/A" : extInfo.toString());
//
//        return resu;
//    }
}
