/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : StorageInfo.java <br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved. <br>
 * Description : The Storage Info Concept <br>
 */
package com.tencent.record.info;

import android.os.StatFs;

import java.io.File;

/**
 * 存储器信息
 *
 * @autho 
 * @see StorageDash
 */
public class StorageInfo {
    private File rootPath;
    private long totalSize;
    private long availableSize;

    public File getRootPath() {
        return rootPath;
    }

    public void setRootPath(File rootPath) {
        this.rootPath = rootPath;
    }

    /**
     * 获得存储器总容量
     *
     * @return -
     */
    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    /**
     * 获得存储器可用容量
     *
     * @return -
     */
    public long getAvailableSize() {
        return availableSize;
    }

    public void setAvailableSize(long availableSize) {
        this.availableSize = availableSize;
    }

    /**
     * 从文件对象获得存储器信息
     *
     * @param path 文件对象
     * @return -
     */
    public static StorageInfo fromFile(File path) {
        StorageInfo info = new StorageInfo();

        info.setRootPath(path);

        StatFs fileSystem = new StatFs(path.getAbsolutePath());

        long blockSize = fileSystem.getBlockSize();
        long totalBlocks = fileSystem.getBlockCount();
        long availableBlocks = fileSystem.getAvailableBlocks();

        info.setTotalSize(totalBlocks * blockSize);
        info.setAvailableSize(availableBlocks * blockSize);

        return info;
    }

    @Override
    public String toString() {
        return String.format("[%s : %d / %d]", getRootPath().getAbsolutePath(), getAvailableSize(), getTotalSize());
    }
}
