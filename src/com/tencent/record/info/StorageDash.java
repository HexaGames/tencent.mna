/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : DeviceStorage.java<br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : The Storage Dashboard of Device<br>
 */
package com.tencent.record.info;

import android.os.Environment;

/**
 * 存储器信息收集类 <br>
 * <br>
 * 为了保证入口的简洁和代码的可读性，请使用{@link com.tencent.base.os.Device.Storage}
 *
 * @author 
 */
public class StorageDash {
    /**
     * 是否有外部存储
     *
     * @return -
     */
    public static boolean hasExternal() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    /**
     * 是否有只读的外部存储
     *
     * @return -
     */
    public static boolean hasExternalReadable() {
        String state = Environment.getExternalStorageState();

        return Environment.MEDIA_MOUNTED.equals(state) || (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state));
    }

    /**
     * 获得外部存储器的信息
     *
     * @return -
     */
    public static StorageInfo getExternalInfo() {
        if (!hasExternalReadable()) {
            return null;
        }

        return StorageInfo.fromFile(Environment.getExternalStorageDirectory());
    }

    /**
     * 获得内部存储器的信息
     *
     * @return -
     */
    public static StorageInfo getInnerInfo() {
        return StorageInfo.fromFile(Global.getFilesDir());
    }
}
