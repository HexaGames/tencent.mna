/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : WnsClientLog.java<br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : Wns Client Log Tracer<br>
 */
package com.tencent.record.debug;

import com.tencent.record.info.Global;

/**
 * WNS客户端接口
 *
 * @author 
 */
public class WnsClientLog extends WnsTracer {
    public static WnsClientLog instance = null;

    public static WnsClientLog getInstance() {
        if (Global.getContext() == null) {
            instance = null;
            return null;
        }

        if (instance == null) {
            synchronized (WnsClientLog.class) {
                if (instance == null) {
                    instance = new WnsClientLog();
                }
            }
        }

        return instance;
    }

    // ------------------------------------------------------------------------------
    // 日志打印方法
    // ------------------------------------------------------------------------------
    public static final void v(String tag, String msg) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            inst.trace(TraceLevel.VERBOSE, tag, msg, null);
        }
    }

    public static final void v(String tag, String msg, Throwable tr) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            inst.trace(TraceLevel.VERBOSE, tag, msg, tr);
        }
    }

    public static final void d(String tag, String msg) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().trace(TraceLevel.DEBUG, tag, msg, null);
        }
    }

    public static final void d(String tag, String msg, Throwable tr) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().trace(TraceLevel.DEBUG, tag, msg, tr);
        }
    }

    public static final void i(String tag, String msg) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().trace(TraceLevel.INFO, tag, msg, null);
        }
    }

    public static final void i(String tag, String msg, Throwable tr) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().trace(TraceLevel.INFO, tag, msg, tr);
        }
    }

    public static final void w(String tag, String msg) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().trace(TraceLevel.WARN, tag, msg, null);
        }
    }

    public static final void w(String tag, String msg, Throwable tr) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().trace(TraceLevel.WARN, tag, msg, tr);
        }
    }

    public static final void e(String tag, String msg) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().trace(TraceLevel.ERROR, tag, msg, null);
        }
    }

    public static final void e(String tag, String msg, Throwable tr) {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().trace(TraceLevel.ERROR, tag, msg, tr);
        }
    }

    /**
     * 确保日志全部写入文件
     */
    public static void ensureLogsToFile() {
        WnsClientLog inst = getInstance();
        if (inst != null) {
            getInstance().flush();
        }
    }

    public WnsClientLog() {
        super();

        fileTracer = new FileTracer(CLIENT_CONFIG);
    }
}
