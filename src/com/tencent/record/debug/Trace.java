/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : Trace.java<br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : Define the Tracer for classes of com.tencent.base.* <br>
 */
package com.tencent.record.debug;

/**
 * 系统级日志追踪，用于追踪 com.tencent.base.* 的日志 <br>
 * <br>
 * 默认使用一个单独的{@code LogcatTracer} <br>
 * <br>
 * 在使用者准备自己的追踪器完毕之后，可以调用方法{@code Trace.setSystemTracer()}设置追踪器以便打印
 * com.tencent.base.* 包类的日志信息，方便查看和调试 com.tencent.base.*包<br>
 * <br>
 * <br>
 * <b>但注意，{@code FileTracer} 及其相关类也会使用SystemTracer打印日志，所以不要使用 {@code FileTracer}
 * 作为SystemTracer，否则可能引发重复调用</b>
 *
 * @author 
 */
public class Trace implements TraceLevel {
    public static void v(String tag, String msg) {
        v(tag, msg, null);
    }

    public static void v(String tag, String msg, Throwable tr) {
        if (systemTracer != null) {
            systemTracer.trace(VERBOSE, Thread.currentThread(), System.currentTimeMillis(), tag, msg, tr);
        }
    }

    public static void d(String tag, String msg) {
        d(tag, msg, null);
    }

    public static void d(String tag, String msg, Throwable tr) {
        if (systemTracer != null) {
            systemTracer.trace(DEBUG, Thread.currentThread(), System.currentTimeMillis(), tag, msg, tr);
        }
    }

    public static void i(String tag, String msg) {
        i(tag, msg, null);
    }

    public static void i(String tag, String msg, Throwable tr) {
        if (systemTracer != null) {
            systemTracer.trace(INFO, Thread.currentThread(), System.currentTimeMillis(), tag, msg, tr);
        }
    }

    public static void w(String tag, String msg) {
        w(tag, msg, null);
    }

    public static void w(String tag, String msg, Throwable tr) {
        if (systemTracer != null) {
            systemTracer.trace(WARN, Thread.currentThread(), System.currentTimeMillis(), tag, msg, tr);
        }
    }

    public static void e(String tag, String msg) {
        e(tag, msg, null);
    }

    public static void e(String tag, String msg, Throwable tr) {
        if (systemTracer != null) {
            systemTracer.trace(ERROR, Thread.currentThread(), System.currentTimeMillis(), tag, msg, tr);
        }
    }

    /**
     * com.tencent.base.* 使用的日志追踪器
     */
    private static volatile Tracer systemTracer = new LogcatTracer();

    /**
     * 设置所有 com.tencent.base.* 使用的日志追踪器<br>
     * <br>
     * 默认使用一个单独的{@code LogcatTracer} <br>
     * <br>
     * 在使用者准备自己的追踪器完毕之后，可以调用该方法设置追踪器以便打印 com.tencent.base.* 包类的日志信息，方便查看和调试
     * com.tencent.base.*包<br>
     * <br>
     * <br>
     * <b>但注意，{@code FileTracer} 及其相关类也会使用SystemTracer打印日志，所以不要使用
     * {@code FileTracer} 作为SystemTracer，否则可能引发重复调用</b>
     *
     * @param tracer 日志追踪器对象
     */
    public static void setSystemTracer(Tracer tracer) {
        systemTracer = tracer;
    }

    /**
     * 获得com.tencent.base.* 包使用的日志追踪器<br>
     * <br>
     * 若有与 com.tencent.base.*包同级别的包需要打印日志，可以尝试使用SystemTracer
     *
     * @return
     */
    public static Tracer getSystemTracer() {
        return systemTracer;
    }
}
