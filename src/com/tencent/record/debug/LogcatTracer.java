/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : LogcatTracer.java <br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : Android Logcat Wrapped Tracer <br>
 */
package com.tencent.record.debug;

import android.util.Log;

import com.tencent.record.util.StrUtils;

/**
 * Android Logcat日志追踪器<br>
 * <br>
 * <p/>
 * 一个方便的{@code android.util.Log}的包装类，用于将日志信息输出到Logcat<br>
 * <br>
 * 若仅仅需要Logcat的功能，建议使用 {@code LogcatTracer.Instance}而非创建一个新的实例
 *
 * @author 
 */
public final class LogcatTracer extends Tracer {
    public static final LogcatTracer Instance = new LogcatTracer();

    @Override
    protected void doTrace(int level, Thread thread, long time, String tag, String msg, Throwable tr) {
        switch (level) {
            case TraceLevel.VERBOSE: {
                Log.v(tag, msg, tr);
            }
            break;
            case TraceLevel.DEBUG: {
                Log.d(tag, msg, tr);
            }
            break;
            case TraceLevel.INFO: {
                Log.i(tag, msg, tr);
            }
            break;
            case TraceLevel.WARN: {
                Log.w(tag, msg, tr);
            }
            break;
            case TraceLevel.ERROR: {
                Log.e(tag, msg, tr);
            }
            break;
            case TraceLevel.ASSERT: {
                // 我去年买了个表，搞了一堆新API不能用搞它干什么
                // Log.wtf(tag, msg, tr);
                Log.e(tag, msg, tr);
            }
            break;
            default:
                break;
        }
    }

    @Override
    protected void doTrace(String formattedTrace) {
        Log.v(StrUtils.EMPTY, formattedTrace);
    }
}
