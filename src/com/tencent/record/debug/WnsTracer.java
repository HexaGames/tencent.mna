/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : WnsTracer.java <br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : Abstract Tracer for WNS and its Client <br>
 */
package com.tencent.record.debug;

import android.content.SharedPreferences;
import android.os.Environment;

import com.tencent.record.info.Const;
import com.tencent.record.info.Global;
import com.tencent.record.info.StorageDash;
import com.tencent.record.info.StorageInfo;

import java.io.File;

/**
 * WNS日志追踪器
 *
 * @author 
 * @see com.tencent.wns.debug.WnsLog
 * @see com.tencent.wns.client.WnsClientLog
 */
public class WnsTracer implements TraceLevel, SharedPreferences.OnSharedPreferenceChangeListener {
    /**
     * 设置最大的单日日志文件大小
     *
     * @param maxSize 单位：字节
     */
    public static void setMaxFolderSize(long maxSize) {
        int blockCount = (int) (maxSize / Const.Debug.FileBlockSize);

        if (!Const.Debug.InfiniteTraceFile) {
            if (blockCount < 1) {
                blockCount = Const.Debug.DefFileBlockCount;
            }
        } else {
            blockCount = 1024;
        }

        //Option.putInt(Const.Debug.FileBlockCount, blockCount).commit();
    }

    /**
     * 设置日志文件最长保管周期
     *
     * @param maxPeriod 单位: 毫秒
     */
    public static void setMaxKeepPeriod(long maxPeriod) {
        if (maxPeriod < 24 * 60 * 60 * 1000L) {
            maxPeriod = Const.Debug.DefFileKeepPeriod;
        }

        //Option.putLong(Const.Debug.FileBlockCount, maxPeriod).commit();
    }

    /**
     * 设置文件日志追踪级别
     *
     * @param level 参考级别常量
     */
    public static void setFileTraceLevel(int level) {
        int traceLevel = level;

        if (level > ALL || level < 0) {
            traceLevel = Const.Debug.DefFileTraceLevel;
        }

        //Option.putInt(Const.Debug.FileTraceLevel, traceLevel).commit();
    }

    /**
     * 获得日志文件路径
     *
     * @return 文件对象
     */
    public static File getLogFilePath() {
        boolean useExternal = false;

        String path = Const.Debug.FileRoot + File.separator + Global.getPackageName();

        StorageInfo info = StorageDash.getExternalInfo();

        if (info != null) {
            if (info.getAvailableSize() > Const.Debug.MinSpaceRequired) {
                useExternal = true;
            }
        }

        if (useExternal) {
            return new File(Environment.getExternalStorageDirectory(), path);
        } else {
            return new File(Global.getFilesDir(), path);
        }
    }

    protected static FileTracerConfig CLIENT_CONFIG;
    protected static FileTracerConfig SERVICE_CONFIG;

    protected FileTracer fileTracer;

    private volatile boolean enabled = Const.Debug.Enabled;
    private volatile boolean fileTracerEnabled = Const.Debug.FileTracerEnabled;
    private volatile boolean logcatTracerEnabled = Const.Debug.LogcatTracerEnabled;

    public WnsTracer() {
        int blockCount = Const.Debug.DefFileBlockCount;//Option.getInt(Const.Debug.FileBlockCount, Const.Debug.DefFileBlockCount);
        long keepPeriod = Const.Debug.DefFileKeepPeriod;//Option.getLong(Const.Debug.FileKeepPeriod, Const.Debug.DefFileKeepPeriod);

        File rootPath = getLogFilePath();

        CLIENT_CONFIG = new FileTracerConfig(rootPath, blockCount, Const.Debug.FileBlockSize,
                Const.Debug.DataThreshold, Const.Debug.ClientFileTracerName, Const.Debug.TimeThreshold,
                FileTracerConfig.PRIORITY_BACKGROUND, Const.Debug.ClientFileExt, keepPeriod);

        SERVICE_CONFIG = new FileTracerConfig(rootPath, blockCount, Const.Debug.FileBlockSize,
                Const.Debug.DataThreshold, Const.Debug.FileTracerName, Const.Debug.TimeThreshold,
                FileTracerConfig.PRIORITY_BACKGROUND, Const.Debug.FileExt, keepPeriod);

        // 开始监控配置项变化
        //Option.startListen(this);
    }

    public void stop() {
        if (fileTracer != null) {
            fileTracer.flush();
            fileTracer.quit();
        }
    }

    public void flush() {
        if (fileTracer != null) {
            fileTracer.flush();
        }
    }

    public void trace(int level, String tag, String msg, Throwable tr) {
        // 检测是否允许Debug
        if (isEnabled()) {
            // 检测是否允许文件日志追踪
            if (isFileTracerEnabled()) {
                fileTracer.trace(level, Thread.currentThread(), System.currentTimeMillis(), tag, msg, tr);
            }

            // 检测是否允许Logcat追踪
            if (isLogcatTracerEnabled()) {
                LogcatTracer.Instance.trace(level, Thread.currentThread(), System.currentTimeMillis(), tag, msg, tr);
            }
        }
    }

    public final void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public final boolean isEnabled() {
        return enabled;
    }

    public final void setFileTracerLevel(int traceLevel) {
        fileTracer.setTraceLevel(traceLevel);
    }

    public final void setFileTracerEnabled(boolean fileTracerEnabled) {
        this.fileTracer.flush();

        this.fileTracerEnabled = fileTracerEnabled;
    }

    public final boolean isFileTracerEnabled() {
        return fileTracerEnabled;
    }

    public final void setLogcatTracerEnabled(boolean logcatTracerEnabled) {
        this.logcatTracerEnabled = logcatTracerEnabled;
    }

    public final boolean isLogcatTracerEnabled() {
        return logcatTracerEnabled;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (Const.Debug.FileTraceLevel.equals(key)) {
            int traceLevel = prefs.getInt(Const.Debug.FileTraceLevel, Const.Debug.DefFileTraceLevel);

            trace(WARN, "WnsTracer", "File Trace Level Changed = " + traceLevel, null);

            fileTracer.setTraceLevel(traceLevel);
        }
    }

    public static void deleteFile(File file) {
        if (file == null || !file.exists()) {
            return;
        }
        if (file.isFile()) {
            file.delete();
        } else {
            for (File f : file.listFiles()) {
                deleteFile(f);
            }
        }
    }
}
