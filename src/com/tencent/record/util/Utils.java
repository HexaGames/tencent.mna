/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : Utils.java<br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : Utilities Set of Other Smaller Ones<br>
 */
package com.tencent.record.util;

/**
 * 其他工具集，包含时间、位运算等等
 *
 * @author 
 */
public class Utils {
    /**
     * 位运算
     *
     * @author 
     */
    public static final class Bit {
        public static final int add(int source, int sub) {
            return source | sub;
        }

        public static final boolean has(int source, int sub) {
            return sub == (source & sub);
        }

        public static final int remove(int source, int sub) {
            return source ^ (source & sub);
        }

        public static final int log2(int source) {
            return (int) (Math.log(source) / Math.log(2));
        }
    }
}
