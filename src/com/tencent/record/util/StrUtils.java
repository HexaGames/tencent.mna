/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : StrUtils.java <br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : String Utilities <br>
 */
package com.tencent.record.util;

import java.text.SimpleDateFormat;

/**
 * 字符串工具集
 *
 * @author 
 */
public class StrUtils {
    /**
     * 空字符串常量 <br>
     * <br>
     * <i>佛曰：四大皆空</i>
     */
    public static final String EMPTY = "";
    /**
     * "不可用"字符串常量
     */
    public static final String NOT_AVALIBLE = "N/A";

    /**
     * 创建指定格式的时间格式化对象
     *
     * @param pattern 时间格式，形如"yyyy-MM-dd HH-mm-ss.SSS"
     * @return Format 时间格式化对象
     */
    public static SimpleDateFormat createDataFormat(String pattern) {
        return new SimpleDateFormat(pattern);
    }

    /**
     * 判断字符串是否为空内容/空指针
     *
     * @param str 字符串
     * @return 是空内容/空指针，返回true，否则返回false
     */
    public static boolean isTextEmpty(String str) {
        return (str == null) || (str.length() < 1);
    }
}
