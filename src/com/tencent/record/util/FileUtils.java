/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


/**
 * Name : FileUtils.java<br>
 * Copyright : Copyright (c) Tencent Inc. All rights reserved.<br>
 * Description : File and Storage Utilities<br>
 */
package com.tencent.record.util;

import java.io.File;

/**
 * 文件工具集
 *
 * @author 
 */
public class FileUtils {
    public static final int ZIP_BUFFER_SIZE = 4 * 1024;
    public static final int CPY_BUFFER_SIZE = 4 * 1024;

    public static final String ZIP_FILE_EXT = ".zip";

//    /**
//     * 复制文件
//     * 
//     * @param srcFile
//     *            源文件路径
//     * @param dstFile
//     *            目标文件路径
//     * @return
//     */
//    public static boolean copyFile(File srcFile, File dstFile)
//    {
//        boolean resu = false;
//
//        FileInputStream fis = null;
//        BufferedOutputStream fos = null;
//
//        try
//        {
//            fis = new FileInputStream(srcFile);
//            fos = new BufferedOutputStream(new FileOutputStream(dstFile));
//
//            byte[] buffer = new byte[CPY_BUFFER_SIZE];
//
//            int readLen = 0;
//
//            while (-1 != (readLen = fis.read(buffer)))
//            {
//                fos.write(buffer, 0, readLen);
//            }
//
//            fos.flush();
//
//            resu = true;
//        }
//        catch (IOException e)
//        {
//            resu = false;
//        }
//        finally
//        {
//            DataUtils.closeDataObject(fos);
//            DataUtils.closeDataObject(fis);
//        }
//
//        return resu;
//    }

    /**
     * 尝试删除文件/文件夹。如果删除失败，尝试在虚拟机退出时删除。
     *
     * @param fileName 文件/文件夹路径
     * @return 删除成功/失败
     */
    public static boolean deleteFile(File file) {
        if (file != null) {
            // 是文件，直接删除
            if (file.isFile()) {
                if (!file.delete()) {
                    file.deleteOnExit();

                    return false;
                } else {
                    return true;
                }
            }
            // 是目录，递归删除
            else if (file.isDirectory()) {
                File[] subFiles = file.listFiles();

                for (File subFile : subFiles) {
                    deleteFile(subFile);
                }

                return file.delete();
            }
            // 那你是啥嘛……
            else {
                return false;
            }
        } else {
            return false;
        }
    }

//    /**
//     * ZIP压缩多个文件/文件夹
//     * 
//     * @param srcFiles
//     *            要压缩的文件/文件夹列表
//     * @param dest
//     *            目标文件
//     * @return 压缩成功/失败
//     */
//    public static boolean zip(File[] srcFiles, File dest)
//    {
//        // 参数检查
//        if (srcFiles == null || srcFiles.length < 1 || dest == null)
//        {
//            return false;
//        }
//
//        boolean resu = false;
//
//        ZipOutputStream zos = null;
//
//        try
//        {
//            byte[] buffer = new byte[ZIP_BUFFER_SIZE];
//
//            zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(dest, false)));
//
//            // 添加文件到ZIP压缩流
//            for (File src : srcFiles)
//            {
//                doZip(zos, src, null, buffer);
//            }
//
//            zos.flush();
//            zos.closeEntry();
//
//            resu = true;
//        }
//        catch (IOException e)
//        {
//        	//e.print*StackTrace();
//
//            resu = false;
//        }
//        finally
//        {
//            DataUtils.closeDataObject(zos);
//        }
//
//        return resu;
//    }
//
//    /**
//     * 方法：ZIP压缩单个文件/文件夹
//     * 
//     * @param source
//     *            源文件/文件夹
//     * @param dest
//     *            目标文件
//     * @return 压缩成功/失败
//     */
//    public static boolean zip(File src, File dest)
//    {
//        return zip(new File[] { src }, dest);
//    }
//
//    /**
//     * 压缩文件/文件夹到ZIP流中 <br>
//     * <br>
//     * <i>本方法是为了向自定义的压缩流添加文件/文件夹，若只是要压缩文件/文件夹到指定位置，请使用 {@code FileUtils.zip()}
//     * 方法</i>
//     * 
//     * @param zos
//     *            ZIP输出流
//     * @param file
//     *            被压缩的文件
//     * @param root
//     *            被压缩的文件在ZIP文件中的入口根节点
//     * @param buffer
//     *            读写缓冲区
//     * @throws IOException
//     *             读写流时可能抛出的I/O异常
//     */
//    public static void doZip(ZipOutputStream zos, File file, String root, byte[] buffer) throws IOException
//    {
//        // 参数检查
//        if (zos == null || file == null)
//        {
//            throw new IOException("I/O Object got NullPointerException");
//        }
//
//        if (!file.exists())
//        {
//            throw new FileNotFoundException("Target File is missing");
//        }
//
//        BufferedInputStream bis = null;
//
//        int readLen = 0;
//
//        String rootName = StrUtils.isTextEmpty(root) ? (file.getName()) : (root + File.separator + file.getName());
//
//        // 文件直接放入压缩流中
//        if (file.isFile())
//        {
//            bis = new BufferedInputStream(new FileInputStream(file));
//
//            zos.putNextEntry(new ZipEntry(rootName));
//
//            while (-1 != (readLen = bis.read(buffer, 0, buffer.length)))
//            {
//                zos.write(buffer, 0, readLen);
//            }
//
//            DataUtils.closeDataObject(bis);
//        }
//        // 文件夹则子文件递归
//        else if (file.isDirectory())
//        {
//            File[] subFiles = file.listFiles();
//
//            for (File subFile : subFiles)
//            {
//                doZip(zos, subFile, rootName, buffer);
//            }
//        }
//    }
}
