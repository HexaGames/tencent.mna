/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.tencent.cloudsdk.data.SingleIpItem;
import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.ClientIdManager;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.IpUtils;
import com.tencent.cloudsdk.utils.NullReturnException;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.record.debug.WnsClientLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Http的测速测试。对比OC和RS
 *
 * @author 
 */
public class HttpSpeedTest extends AbsTest<MainActivity.RSDomainItem> {
    private static final String TAG = "HttpSpeedTest";

    private MainActivity.RSDomainItem mItem;
    private int mRound = 1;

    @Override
    protected void beforeDoTest() {
        PrintHandler.clearLog();
        PrintHandler.addLog(TAG, "=======自动化测试HTTP测速 （第 " + (mRound++) + " 轮）=======");
    }

    @Override
    protected void postDoTest() {

    }

    @Override
    protected void postTestFinished() {
        PrintHandler.addLog(TAG, "=======自动化测试HTTP测速 结束（共 " + (--mRound) + " 轮）=======");
    }

    @Override
    public void setData(MainActivity.RSDomainItem data) {
        mItem = data;
    }

    @Override
    public boolean canRun() {
        return mItem.type == MainActivity.RSDomainItem.TYPE_FOR_HTTP_SPEED_TEST;
    }

    @Override
    public void doTest() {
        new SpeedDirector(mItem, mHandlerThread.getLooper()).execute();
    }

    private static class SpeedDirector implements MonitorHandler.OnAllRoundFinishListener {
        private List<SpeedProcessor> mProcessors = new ArrayList<SpeedProcessor>(10);
        private MainActivity.RSDomainItem mDomainItem;
        private Looper mLooper;

        private SingleIpItem mOcItem;
        private SingleIpItem mRsItem;

        public SpeedDirector(MainActivity.RSDomainItem item, Looper looper) {
            mDomainItem = item;
            mLooper = looper;

            try {
                getOCAndRS();
                createProcessors();
            } catch (MalformedURLException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            }
        }

        public void execute() {
            if (mProcessors.size() < 2) {
                return;
            }

            final SpeedProcessor ocProcessor = mProcessors.get(0);
            final SpeedProcessor rsProcessor = mProcessors.get(1);
            mProcessors.remove(0);
            mProcessors.remove(0);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    ocProcessor.start();
                }
            }).start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    rsProcessor.start();
                }
            }).start();
        }

        private void createProcessors() throws MalformedURLException {
            createOneProcessor("/a.jpg", null);
            createOneProcessor("/b.jpg", null);
            createOneProcessor("/c.jpg", null);
            createOneProcessor("/d.jpg", null);

            byte[] postData1 = new byte[10 * 1024];
            createOneProcessor("/post.php", postData1);

            byte[] postData2 = new byte[50 * 1024];
            createOneProcessor("/post.php", postData2);

            byte[] postData3 = new byte[80 * 1024];
            createOneProcessor("/post.php", postData3);

            byte[] postData4 = new byte[100 * 1024];
            createOneProcessor("/post.php", postData4);
        }

        private void createOneProcessor(String file, byte[] postData) throws MalformedURLException {
            if (mOcItem == null || mRsItem == null) {
                return;
            }

            URL ocUrl = new URL("http", mOcItem.ip, mOcItem.port, file);
            URL rsUrl = new URL("http", mRsItem.ip, mDomainItem.port, file);

            List<SpeedResult> results = new ArrayList<SpeedResult>(2);
            results.add(new SpeedResult(rsUrl.getHost(), IpUtils.TYPE_RS, rsUrl));
            results.add(new SpeedResult(ocUrl.getHost(), IpUtils.TYPE_OC, ocUrl));
            final MonitorHandler handler = new MonitorHandler(mLooper, results);
            handler.setOnAllRoundFinishListener(this);

            SpeedProcessor ocProcessor = new SpeedProcessor(ocUrl, mDomainItem, handler, 3, postData);
            SpeedProcessor rsProcessor = new SpeedProcessor(rsUrl, mDomainItem, handler, 3, postData);

            mProcessors.add(ocProcessor);
            mProcessors.add(rsProcessor);
        }

        private void getOCAndRS() {
            try {
                List<SingleIpItem> ipItems = IpUtils.getIpInfosFromDB(GlobalContext.getContext(), mDomainItem.domain, true);
                for (SingleIpItem item : ipItems) {
                    if (item.type == IpUtils.TYPE_OC) {
                        mOcItem = item;
                    } else if (item.type == IpUtils.TYPE_RS) {
                        mRsItem = item;
                    }
                }
            } catch (NullReturnException e) {
                PrintHandler.addLog(TAG, "无法获取OC和RS的IP");
            }
        }

        @Override
        public void onAllRoundFinish() {
            execute();
        }
    }


    /**
     * 监控测速的每个步骤,并计算时间，记录结果，当测速完成后调用上报
     */
    private static class MonitorHandler extends Handler {
        private Map<String, SpeedResult> mUrlResultMap = new HashMap<String, SpeedResult>(2);
        private OnAllRoundFinishListener mListener;

        public MonitorHandler(Looper looper, List<SpeedResult> results) {
            super(looper);
            for (SpeedResult result : results) {
                mUrlResultMap.put(result.url.toString(), result);
            }
        }

        public void setOnAllRoundFinishListener(OnAllRoundFinishListener listener) {
            mListener = listener;
        }

        public static final int MSG_START_CONNECT = 1;
        public static final int MSG_END_CONNECT = 2;
        public static final int MSG_START_DOWNLOAD = 3;
        public static final int MSG_END_DOWNLOAD = 4;
        public static final int MSG_ONE_ROUND_END = 5;
        public static final int MSG_ALL_ROUND_FINISH = 6;

        @Override
        public void handleMessage(Message msg) {
            AbsSpeedState state = (AbsSpeedState) msg.obj;
            URL url = state.getUrl();
            SpeedResult result = mUrlResultMap.get(url.toString());
            int maxCount = state.getSpeedProcessor().getMaxRound();

            switch (msg.what) {
                case MSG_START_CONNECT:
                    break;
                case MSG_END_CONNECT:
                    ConnectState connectState = (ConnectState) msg.obj;
                    long connectTime = connectState.getConnectTime();
                    result.totalConnTime += connectTime;
                    result.packetRecvLength = connectState.getPacketRecvLength();
                    result.packetSendLength = connectState.getPacketSentLength();
                    result.method = connectState.getMethod();
                    break;
                case MSG_START_DOWNLOAD:
                    break;
                case MSG_END_DOWNLOAD:
                    DownloadState downloadState = (DownloadState) msg.obj;
                    long downloadTime = downloadState.getDownloadTime();
                    result.totalDownloadTime += downloadTime;
                    break;
                case MSG_ONE_ROUND_END:
                    EndState endState = (EndState) msg.obj;
                    result.totalTime = result.totalConnTime + result.totalDownloadTime;
                    result.round = state.getSpeedProcessor().getRound();
                    result.ret = endState.getRet();

                    if (endState.getSpeedProcessor().getRound() == maxCount) {
                        result.avgConnTime = result.totalConnTime / maxCount;
                        result.avgDownloadTime = result.totalDownloadTime / maxCount;
                        result.avgTotalTime = result.totalTime / maxCount;
                    }

                    break;
                case MSG_ALL_ROUND_FINISH:
                    for (SpeedResult tmpResult : mUrlResultMap.values()) {
                        if (tmpResult.round != maxCount) {
                            return;
                        }
                    }

                    SpeedReportManager reportManager = new SpeedReportManager(mUrlResultMap.values());
                    reportManager.report();

                    if (mListener != null) {
                        mListener.onAllRoundFinish();
                    }
                    break;
            }
        }

        public static interface OnAllRoundFinishListener {
            void onAllRoundFinish();
        }
    }

    /**
     * 测速的结果
     */
    private static class SpeedResult {
        private SpeedResult(String rsIp, int ipType, URL url) {
            this.rsIp = rsIp;
            this.ipType = ipType;
            this.url = url;
        }

        public long packetRecvLength;
        public long packetSendLength;
        public long avgConnTime;
        public long avgDownloadTime;
        public long avgTotalTime;
        public int method;
        public int round;
        public int ret;

        public URL url;
        public int ipType;
        public String rsIp;

        public long totalConnTime;
        public long totalDownloadTime;
        public long totalTime;
    }

    /**
     * 测速的执行器，管理了测速的每个步骤（SpeedState），和切换
     */
    private static class SpeedProcessor {
        private SpeedState mCurState;
        private URL mUrl;
        private MonitorHandler mMonitorHandler;
        private int mRound;
        private int mMaxRound;
        private String mHostProp;
        private MainActivity.RSDomainItem mRSItem;
        private byte[] mPostData;

        public SpeedProcessor(URL url, MainActivity.RSDomainItem domainItem, MonitorHandler handler, int maxRound, byte[] postData) {
            mUrl = url;
            mMonitorHandler = handler;
            mMaxRound = maxRound;
            mRSItem = domainItem;
            mHostProp = domainItem.display + ":" + domainItem.port;
            mPostData = postData;
        }

        public void start() {
            mCurState = new ConnectState(this, mUrl);
            mCurState.enter();
        }

        public void changeState(SpeedState state) {
            mCurState.exit();
            mCurState = state;
            mCurState.enter();
        }

        public void notifyMonitor(int msgWhat, SpeedState state) {
            Message.obtain(mMonitorHandler, msgWhat, state).sendToTarget();
        }

        public URL getUrl() {
            return mUrl;
        }

        public int getRound() {
            return mRound;
        }

        public void setRound(int round) {
            this.mRound = round;
        }

        public int getMaxRound() {
            return mMaxRound;
        }

        public String getHostProp() {
            return mHostProp;
        }

        public byte[] getPostData() {
            return mPostData;
        }
    }

    /**
     * 代表测试单个步骤的接口
     */
    private interface SpeedState {
        void enter();

        void exit();
    }

    private static abstract class AbsSpeedState implements SpeedState {
        protected SpeedProcessor mSpeedProcessor;

        public AbsSpeedState(SpeedProcessor speedProcessor) {
            mSpeedProcessor = speedProcessor;
        }

        public SpeedProcessor getSpeedProcessor() {
            return mSpeedProcessor;
        }

        public URL getUrl() {
            return mSpeedProcessor.getUrl();
        }
    }

    /**
     * 连接步骤
     */
    private static class ConnectState extends AbsSpeedState {
        private URL mUrl;
        private long mConnectTime;
        private long mRecvLength;
        private long mSendLength;
        private int mMethod;

        public ConnectState(SpeedProcessor processor, URL url) {
            super(processor);
            mUrl = url;
        }

        @Override
        public void enter() {
            mSpeedProcessor.notifyMonitor(MonitorHandler.MSG_START_CONNECT, this);
            HttpParams params = new BasicHttpParams();
            DefaultHttpClient client = new DefaultHttpClient(params);

            byte[] postData = mSpeedProcessor.getPostData();
            HttpUriRequest request;
            if (postData == null) {
                request = new HttpGet(mUrl.toString());
                mMethod = ReportConstanst.METHOD_GET;
            } else {
                request = new HttpPost(mUrl.toString());
                ((HttpPost) request).setEntity(new ByteArrayEntity(postData));
                mSendLength = postData.length;
                mMethod = ReportConstanst.METHOD_POST;
            }

            request.setHeader("Host", mSpeedProcessor.getHostProp());
            InputStream is;
            long startTime = System.currentTimeMillis();
            try {
                HttpResponse response = client.execute(request);
                HttpEntity entity = response.getEntity();
                mRecvLength = entity.getContentLength();
                mRecvLength = mRecvLength < 0 ? 0 : mRecvLength;
                is = entity.getContent();
            } catch (IOException e) {
                PrintHandler.addLog(TAG, String.format("连接发生异常"));
                WnsClientLog.e(TAG, e.getMessage(), e);
                int ret = 0;
                if (e instanceof SocketTimeoutException) {
                    ret += ReportConstanst.REPORT_SOCKET_TIMEOUT;
                } else {
                    ret += ReportConstanst.REPORT_CONNECT_ERR;
                }

                mConnectTime = System.currentTimeMillis() - startTime;
                mSpeedProcessor.changeState(new EndState(mSpeedProcessor, ret));
                return;
            }

            mConnectTime = System.currentTimeMillis() - startTime;
            mSpeedProcessor.changeState(new DownloadState(mSpeedProcessor, is, mRecvLength));
        }

        @Override
        public void exit() {
            mSpeedProcessor.notifyMonitor(MonitorHandler.MSG_END_CONNECT, this);
        }

        private long getConnectTime() {
            return mConnectTime;
        }

        private long getPacketRecvLength() {
            return mRecvLength;
        }

        private long getPacketSentLength() {
            return mSendLength;
        }

        private int getMethod() {
            return mMethod;
        }
    }

    /**
     * 下载步骤
     */
    private static class DownloadState extends AbsSpeedState {
        private InputStream mInputStream;
        private long mDownloadTime;
        private long mLength;

        public DownloadState(SpeedProcessor speedProcessor, InputStream is, long length) {
            super(speedProcessor);
            mInputStream = is;
            mLength = length;
        }

        @Override
        public void enter() {
            long downloadStartTime = System.currentTimeMillis();
            mSpeedProcessor.notifyMonitor(MonitorHandler.MSG_START_DOWNLOAD, this);
            try {
                byte[] buf = new byte[1024];
                int ch = -1;
                int count = 0;
                while ((ch = mInputStream.read(buf)) != -1) {
                    count += ch;
                }
            } catch (IOException e) {
                PrintHandler.addLog(TAG, "下载数据发生异常");
                WnsClientLog.e(TAG, e.getMessage(), e);
                mDownloadTime = System.currentTimeMillis() - downloadStartTime;
                mSpeedProcessor.changeState(new EndState(mSpeedProcessor, ReportConstanst.REPORT_RECV_ERR));
                return;
            }

            mDownloadTime = System.currentTimeMillis() - downloadStartTime;
            mSpeedProcessor.changeState(new EndState(mSpeedProcessor, 0));
        }

        @Override
        public void exit() {
            mSpeedProcessor.notifyMonitor(MonitorHandler.MSG_END_DOWNLOAD, this);
        }

        private long getDownloadTime() {
            return mDownloadTime;
        }
    }

    /**
     * 结束的步骤
     */
    private static class EndState extends AbsSpeedState {
        private int mRet;

        public EndState(SpeedProcessor speedProcessor, int ret) {
            super(speedProcessor);
            mRet = ret;
        }

        @Override
        public void enter() {
            int maxRound = mSpeedProcessor.getMaxRound();
            int round = mRet != 0 ? maxRound : mSpeedProcessor.getRound() + 1;
            mSpeedProcessor.setRound(round);
            mSpeedProcessor.notifyMonitor(MonitorHandler.MSG_ONE_ROUND_END, this);

            if (round >= maxRound) {
                mSpeedProcessor.notifyMonitor(MonitorHandler.MSG_ALL_ROUND_FINISH, this);
            } else {
                mSpeedProcessor.changeState(new ConnectState(mSpeedProcessor, getUrl()));
            }
        }

        @Override
        public void exit() {

        }

        public int getRet() {
            return mRet;
        }
    }


    /**
     * 关于测速上报的管理类
     */
    private static class SpeedReportManager {
        private Map<Integer, SpeedResult> mResultMap = new HashMap<Integer, SpeedResult>(2);

        public SpeedReportManager(Collection<SpeedResult> results) {
            for (SpeedResult result : results) {
                mResultMap.put(result.ipType, result);
            }
        }

        public void report() {
            SpeedResult ocResult = mResultMap.get(IpUtils.TYPE_OC);
            SpeedResult rsResult = mResultMap.get(IpUtils.TYPE_RS);
            if (ocResult == null || rsResult == null) {
                return;
            }

            showResult(ocResult, rsResult);

            Bundle bundle = new Bundle();
            bundle.putInt(ReportConstanst.SPEED_TEST_TYPEID, ReportConstanst.TYPE_ID_HTTP_SPEED_TEST);
            bundle.putLong(ReportConstanst.SPEED_TEST_RESOURCE_IP, IpUtils.parseIpAddress(rsResult.rsIp));
            int port = rsResult.url.getPort();
            bundle.putInt(ReportConstanst.SPEED_TEST_RESOURCE_PORT, port <= 0 ? 80 : port);
            bundle.putLong(ReportConstanst.SPEED_TEST_OC_IP, IpUtils.parseIpAddress(ocResult.url.getHost()));
            bundle.putLong(ReportConstanst.SPEED_TEST_TC, rsResult.avgTotalTime);
            bundle.putInt(ReportConstanst.SPEED_TEST_RET, rsResult.ret);
            bundle.putLong(ReportConstanst.SPEED_TEST_OCTC, ocResult.avgTotalTime);
            bundle.putInt(ReportConstanst.SPEED_TEST_OCRET, ocResult.ret);
            bundle.putInt(ReportConstanst.SPEED_TEST_NT, Common.getNetworkType());
            bundle.putInt(ReportConstanst.SPEED_TEST_SP, Common.getSP());
            bundle.putInt(ReportConstanst.SPEED_TEST_OS, ReportConstanst.OS);
            bundle.putString(ReportConstanst.SPEED_TEST_OSV, android.os.Build.VERSION.RELEASE);
            bundle.putString(ReportConstanst.SPEED_TEST_SDK, ReportConstanst.SDK_VERSION);
            bundle.putLong(ReportConstanst.SPEED_TEST_CTC, rsResult.avgConnTime);
            bundle.putLong(ReportConstanst.SPEED_TEST_OCCTC, ocResult.avgConnTime);
            bundle.putLong(ReportConstanst.SPEED_TEST_STC, 0);
            bundle.putLong(ReportConstanst.SPEED_TEST_OCSTC, 0);
            bundle.putLong(ReportConstanst.SPEED_TEST_RTC, rsResult.avgDownloadTime);
            bundle.putLong(ReportConstanst.SPEED_TEST_OCRTC, ocResult.avgDownloadTime);
            bundle.putLong(ReportConstanst.SPEED_UPLOAD_LENGTH, rsResult.packetSendLength);
            bundle.putLong(ReportConstanst.SPEED_DOWNLOAD_LENGTH, rsResult.packetRecvLength);
            bundle.putInt(ReportConstanst.SPEED_METHOD, rsResult.method);
            bundle.putInt(ReportConstanst.SPEED_CLIENT_SEQ, ClientIdManager.getInstance().getClientId());
            bundle.putInt(ReportConstanst.SPEED_PROTO, ReportConstanst.PROTO_HTTP);

            SDKGlobal.getReportTool().report(bundle);
        }

        private void showResult(SpeedResult ocResult, SpeedResult rsResult) {
            PrintHandler.addLog(TAG, ">>>>>测速结果");
            PrintHandler.addLog(TAG, "RSURL:" + rsResult.url);
            PrintHandler.addLog(TAG, "OCURL:" + ocResult.url);
            PrintHandler.addLog(TAG, "测试HTTP方法：" + (rsResult.method == ReportConstanst.METHOD_GET ? "GET" : "POST"));
            PrintHandler.addLog(TAG, "上传包大小：" + rsResult.packetSendLength);
            PrintHandler.addLog(TAG, "下载包大小：" + rsResult.packetRecvLength);
            PrintHandler.addLog(TAG, "直连的返回结果：" + rsResult.ret);
            PrintHandler.addLog(TAG, "OC加速的返回结果：" + ocResult.ret);
            PrintHandler.addLog(TAG, "直连耗时：" + rsResult.avgTotalTime);
            PrintHandler.addLog(TAG, "OC加速耗时：" + ocResult.avgTotalTime);
            PrintHandler.addLog(TAG, "直连建立连接时间：" + rsResult.avgConnTime);
            PrintHandler.addLog(TAG, "OC加速建立连接时间：" + ocResult.avgConnTime);
            PrintHandler.addLog(TAG, "直连下载时间：" + rsResult.avgDownloadTime);
            PrintHandler.addLog(TAG, "OC加速下载时间：" + ocResult.avgDownloadTime);
            PrintHandler.addLog(TAG, "网络类型：" + Common.getNetworkType());
            PrintHandler.addLog(TAG, "运营商类型：" + Common.getSP());
            PrintHandler.addLog(TAG, "系统版本：" + android.os.Build.VERSION.RELEASE);
        }
    }
}
