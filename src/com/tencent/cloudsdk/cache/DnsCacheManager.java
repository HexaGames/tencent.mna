/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.cache;

import com.tencent.cloudsdk.dns.ResponsePacket;
import com.tencent.cloudsdk.utils.Common;

import java.net.InetAddress;

/**
 * DNS记录缓存，记录的过期时间以TTL为准，考虑到TTL的有效期一般为分钟级
 * ，故这里只做内存的缓存，不做物理缓存
 *
 * @author 
 */
public class DnsCacheManager {

    /**
     * 缓存初始容量值
     */
    private final int INIT_SIZE = 8;

    /**
     * 缓存最大容量值
     */
    private final int MAX_CACHE_SIZE = 32;

    /**
     * 缓存链表
     */
    private MemoryCache<String, HostEntity> data = new MemoryCache<String, HostEntity>(INIT_SIZE, MAX_CACHE_SIZE);

    /**
     * 缓存实例
     */
    private static DnsCacheManager manager = null;

    public synchronized static DnsCacheManager getInstance() {
        if (manager == null) {
            manager = new DnsCacheManager();
        }

        return manager;
    }

    /**
     * 获取指定Host的响应包信息
     *
     * @param host
     * @return {@link InetAddress[]}，如果缓存未找到或缓存已经过期将返回null
     */
    public InetAddress[] getCacheItemByHost(String host) {
        //WnsClientLog.w("HostCacheManager", ">>查找缓存：[" + Thread.currentThread().getId() + "]" + System.currentTimeMillis());
        HostEntity he = data.get(host);
        //WnsClientLog.w("HostCacheManager", ">>缓存查找Done：[" + Thread.currentThread().getId() + "]" + System.currentTimeMillis());
        if (he != null) {

            if (!he.isValid()) {
                //WnsClientLog.w("dnstest", "缓存无效[" + host + "]");
                data.remove(host);
                he = null;
            } else {

                return he.address;
            }
        }

        return null;
    }

    /**
     * 将响应包添加至缓存
     *
     * @param host
     * @param packet
     * @return {@link ResponsePacket}
     */
    public void addCache(String host, InetAddress[] address, long expireTime) {

        HostEntity he = new HostEntity();
        he.expireTime = expireTime;
        he.address = address;
        he.networkFlag = Common.getIpOrMac();

        if (data.containsKey(host)) {
            data.remove(host);
        }

        data.put(host, he);
    }

    ////////////////////////////////////

    /**
     * host的缓存实体
     *
     * @author 
     */
    private class HostEntity {

        public long expireTime = 0;
        public InetAddress[] address = null;
        public String networkFlag = "";

        public boolean isValid() {
            boolean isValid = System.currentTimeMillis() < expireTime;

            if (isValid) {

                isValid = this.networkFlag.equals(Common.getIpOrMac());
            }
            return isValid;
        }
    }

}

