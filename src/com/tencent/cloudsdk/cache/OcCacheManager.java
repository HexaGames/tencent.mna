/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.CommonAsynThread;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.record.debug.WnsClientLog;

//import android.util.Log;

//import java.util.Calendar;

/**
 * OC查询数据缓存管理器
 *
 * @author 
 */
public class OcCacheManager extends OcCacheDatabaseHelper {

    private static final String TAG = OcCacheManager.class.getName();

    /**
     * 缓存器实例
     */
    private static OcCacheManager cache = null;

    /**
     * 最大缓存容量值
     */
    private final static int MAX_CACHE_SIZE = 32;

    /**用来计算缓存过期时间*/
    //private Calendar cal = null;

    /**
     * 缓存链表
     */
    private static MemoryCache<String, OcAddress> mcache = null;

    /**
     * 是否做过清理检查
     */
    private boolean isClearExpired = false;

    /**
     * 清理过期缓存时间间隔
     */
    public static final int CLEAR_EXPIRED_DATE_SPAN = 7 * 24 * 60 * 60 * 1000;

    /**
     * 清理过期缓存SharedPreferences名
     */
    public static final String CLEAR_EXPIRED_SP_NAME = "clr_exp";

    /**
     * 清理过期缓存SharedPreferences数据KEY
     */
    public static final String CLEAR_EXPIRED_SP_DATE_KEY = "last_clr_date";

    //////////////////////////////////////////////

    static {
        /**实例化内存Cahce*/
        mcache = new MemoryCache<String, OcAddress>(MAX_CACHE_SIZE);
    }

    /**
     * 创建一个{@link OcCacheManager}实例，为私有方法，只能在内部创建
     *
     * @param ctx {@link Context}
     */
    private OcCacheManager(Context ctx) {
        super(ctx);
    }

    /**
     * 获取缓存器对象。为单例模式
     *
     * @param ctx {@link Context}
     * @return {@link OcCacheManager}
     */
    public synchronized static OcCacheManager getInstance(Context ctx) {
        if (cache == null) {
            cache = new OcCacheManager(ctx);
        }

        return cache;
    }

    /////////////////////////////////////////////////////

    @Override
    public void add(OcAddress oa, boolean isHttp) {
        String key = getKey(oa.domain, Integer.valueOf(oa.sp), isHttp);
        //WnsClientLog.i(TAG, "添加数据 [key:" + key + "][" + "[domain:" + oa.domain + "][sp:" + oa.sp + "]" + "[size:" + oa.addressList.size() + "]" + "[tid:" + Thread.currentThread().getId() + "]");
        /**当内存缓存中不存在时才需要增加*/
        if (!mcache.containsKey(key)) {
            mcache.put(key, oa);

            super.add(oa, isHttp);
        }
    }

    /**
     * 异步增加数据
     *
     * @param oa
     */
    public void asynAdd(OcAddress oa, boolean isHttp) {
        //WnsClientLog.i(TAG, "异步添加数据 [domain:" + oa.domain + "][sp:" + oa.sp + "]" + "[size:" + oa.addressList.size() + "]");
        CommonAsynThread.handler.post(new AsynRunnable(oa, AsynRunnable.ACTION_INSERT_DATA, isHttp));
    }


    /**
     * 获取缓存信息。按照 内存>DB 的优先级查找
     *
     * @param domain
     * @param sp
     * @return
     */
    public OcAddress get(String domain, int sp, boolean isHttp) {
        String key = getKey(domain, sp, isHttp);
        String _sp = getFakeSp(sp);

        PrintHandler.addLog(TAG, ">>>从缓存中查找IP信息: key=" + key);

        OcAddress oa = null;

        if (mcache.containsKey(key)) {
            oa = mcache.get(key);
            if (oa != null && !oa.isExpired()) {
                PrintHandler.addLog(TAG, ">>>从缓存中查找IP信息: 从内存缓存中返回");
                return oa;
            }

            mcache.remove(key);
        }

        PrintHandler.addLog(TAG, ">>>从缓存中查找IP信息: 内存缓存不存在 key=" + key + "，从DB查找");

        oa = super._get(domain, _sp, isHttp);

        /**如果数据库中存在缓存，则放入内存缓存中*/
        if (oa != null) {
            mcache.put(key, oa);
        }

        return oa;
    }

    @Override
    public void clear() {
        mcache.clear();
        super.clear();
    }

    public void clearDomain(String domain, int sp, boolean isHttp) {
        String key = getKey(domain, sp, isHttp);
        String _sp = getFakeSp(sp);

        WnsClientLog.i(TAG, "clearDomain, key = " + key);

        mcache.remove(key);
        deleteDomain(domain, _sp, isHttp);
    }

    public void clearDomain(String domain, boolean isHttp) {
        WnsClientLog.i(TAG, "clearDomain, domain = " + domain);

        int[] sps = new int[]{
                AnsQueryConstants.E_MOBILE_UNKNOWN,
                AnsQueryConstants.E_MOBILE_CHINAMOBILE,
                AnsQueryConstants.E_MOBILE_TELCOM,
                AnsQueryConstants.E_MOBILE_UNICOM
        };

        for (int sp : sps) {
            clearDomain(domain, sp, isHttp);
        }
    }

    public void clearDomain(String domain) {
        clearDomain(domain, true);
        clearDomain(domain, false);
    }

    /**
     * 清除过期数据。定7天清理一次
     */
    @Override
    public void clearExpiredItem() {
        if (!isClearExpired) {

            CommonAsynThread.handler.post(new AsynRunnable(null, AsynRunnable.ACTION_CLEAR_EXPIRED, false));
        }
    }

    @Override
    public void updateIp(String domain, String sp, boolean isHttp, String curIP, String newIP) {
        mcache.remove(getKey(domain, sp, isHttp));
        super.updateIp(domain, sp, isHttp, curIP, newIP);
    }

    private String getKey(String domain, int sp, boolean isHttp) {
        return getKey(domain, getFakeSp(sp), isHttp);
    }

    private String getKey(String domain, String fakeSp, boolean isHttp) {
        return domain + fakeSp + isHttp;
    }


    private String getFakeSp(int sp) {
        String fakeSp;
        if (sp == AnsQueryConstants.E_MOBILE_UNKNOWN) {
            fakeSp = Common.getIpOrMac();
        } else {
            fakeSp = "" + sp;
        }

        return fakeSp;
    }

    /**
     * 获取过期时间。到第二天凌晨四点过期
     * @return
     */
    /*@Deprecated
    private long getExpireTime(){        
        
        cal = Calendar.getInstance();        
        cal.set(Calendar.HOUR_OF_DAY, 4);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);        
        
        cal.setTimeInMillis(cal.getTimeInMillis() + (24 * 60 * 60 * 1000));
        
        return cal.getTimeInMillis();
    }*/

    /////////////////////////////////////////

    /**
     * 异步写数据库的run able
     *
     * @author 
     */
    private class AsynRunnable implements Runnable {
        private Object data = null;
        private boolean addDb = false;
        private String key = null;

        private boolean isHttp;

        /**
         * action值
         */
        private short action = 0;

        /**
         * 插入缓存ACTION
         */
        public static final short ACTION_INSERT_DATA = 1;
        /**
         * 清理过期缓存ACTION
         */
        public static final short ACTION_CLEAR_EXPIRED = 2;


        public AsynRunnable(Object data, short action, boolean isHttp) {
            this.data = data;
            this.action = action;
            this.isHttp = isHttp;

            if (this.action == ACTION_INSERT_DATA) {
                key = getKey(((OcAddress) data).domain, Integer.valueOf(((OcAddress) data).sp), isHttp);// ((OcAddress)data).domain + ((OcAddress)data).sp;
                PrintHandler.addLog(TAG, ">>>将结果缓存至内存 key=" + key);
                if (!mcache.containsKey(key)) {
                    mcache.put(key, (OcAddress) data);
                    addDb = true;
                }
            }
        }


        @Override
        public void run() {

            switch (action) {

                /**插入缓存操作*/
                case ACTION_INSERT_DATA:
                    if (addDb) {
                        //WnsClientLog.i(TAG, "添加数据 [key:" + key + "][" + "[domain:" + ((OcAddress)data).domain + "][sp:" + ((OcAddress)data).sp + "]"
                        //+ "[size:" + ((OcAddress)data).addressList.size() + "]" + "[tid:" + Thread.currentThread().getId() + "]");

                        OcCacheManager.super.add(((OcAddress) data), isHttp);
                    }
                    break;

                /**清除过期缓存操作*/
                case ACTION_CLEAR_EXPIRED:

                    if (GlobalContext.getContext() != null) {
                        SharedPreferences sp = GlobalContext.getContext().getSharedPreferences(CLEAR_EXPIRED_SP_NAME, Context.MODE_PRIVATE);
                        long lastClearDate = sp.getLong(CLEAR_EXPIRED_SP_DATE_KEY, 0L);
                        //WnsClientLog.i(TAG, ">>>清除过期缓存 [上次清理时间: " + lastClearDate + "]");
                        if (System.currentTimeMillis() > lastClearDate) {
                            //WnsClientLog.i(TAG, ">>>清除过期缓存 [开始清理]");
                            OcCacheManager.super.clearExpiredItem();

                            lastClearDate = System.currentTimeMillis() + CLEAR_EXPIRED_DATE_SPAN;
                            //WnsClientLog.i(TAG, ">>>清除过期缓存 [清理完毕][更新清理时间:" + lastClearDate +"]");
                            Editor editor = sp.edit();
                            editor.putLong(CLEAR_EXPIRED_SP_DATE_KEY, lastClearDate);
                            editor.commit();
                        }
                    }

                    isClearExpired = true;
                    break;
                default:
                    break;
            }
        }
    }

}
