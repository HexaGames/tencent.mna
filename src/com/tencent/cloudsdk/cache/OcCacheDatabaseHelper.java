/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.data.AddressEntity;
import com.tencent.cloudsdk.data.DomainInfo;
import com.tencent.cloudsdk.data.FailCountItem;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.Constanst;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.record.debug.WnsClientLog;

import java.util.ArrayList;
import java.util.List;

/**
 * 为缓存提供数据库操作类
 *
 * @author 
 */
public class OcCacheDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = OcCacheDatabaseHelper.class.getName();

    /**
     * db的名称、版本号等常用设置
     */
    private static final String DATABASE_NAME = "mac_ip_info"; // database名字
    private static final int DATABASE_VERSION = 5; // database版本

    /**
     * domain表名称、列名定义
     */
    private static final String TABLE_NAME_DOMAIN = "domain"; // 存储域名信息的表
    private static final String DOMAIN_COLUMN_ID = "id"; // id。自增整数，用来标识一条记录
    private static final String DOMAIN_COLUMN_DOMAIN = "domain"; // 域名
    private static final String DOMAIN_COLUMN_SP = "sp"; // 网络运营商
    private static final String DOMAIN_COLUMN_EXPIRE_TIME = "expire_time"; // 过期时间
    private static final String DOMAIN_COLUMN_RETEST_TIMESPAN = "retest_ts"; // 重新测速时间间隔
    private static final String DOMAIN_COLUMN_PROTO = "proto"; // 0-tcp, 1-http

    /**
     * ip表名称、列名定义
     */
    private static final String TABLE_NAME_IP = "ip"; // 存储ip信息的表
    private static final String IP_COLUMN_ID = "id"; // id。自增整数，用来标识一条记录
    private static final String IP_COLUMN_DOMAIN_ID = "d_id"; // domain表的id，用来建立domian表跟ip表的映射关系
    private static final String IP_COLUMN_IP = "ip"; // ip地址
    private static final String IP_COLUMN_PORT = "port"; // 端口号
    private static final String IP_COLUMN_SP = "sp"; // 运营商
    private static final String IP_COLUMN_TYPE = "type"; // ip的类型，分为OC接入点IP、源站IP、测速IP三种
    private static final String IP_COLUMN_PRIORITY = "pri"; // 优先级

    /**
     * ANS通用设置表。acs = ans common setting
     */
    private static final String TABLE_NAME_ANS_COMMON_SETTING = "acs";

    /**
     * ANS IP表
     */
    private static final String TABLE_NAME_ANS_IP = "aip";
    /**
     * ANS IP表ID
     */
    private static final String AIP_COLUMN_ID = "id";
    /**
     * IP地址
     */
    private static final String AIP_COLUMN_IP = "ip";
    /**
     * 运营商
     */
    private static final String AIP_COLUMN_SP = "sp";

    /**
     * 资源锁
     */
    private final static byte synLock[] = new byte[1];
    /**
     * ANS配置资源锁
     */
    private final static byte ansSettingSynLock[] = new byte[1];

    /**
     * 连接失败次数表,表名
     */
    private static final String TABLE_NAME_CONNECT_FAIL = "connect_fail_table";

    /**
     * 连接失败次数表ID
     */
    private static final String CONNECT_FAIL_COLUMN_ID = "connect_fail_column_id";

    /**
     * 连接失败次数表域名
     */
    private static final String CONNECT_FAIL_COLUMN_IP = "connect_fail_column_ip";

    /**
     * 连接失败次数表 端口
     */
    private static final String CONNECT_FAIL_COLUMN_PORT = "connect_fail_column_port";

    /**
     * 连接失败次数表错误码
     */
    private static final String CONNECT_FAIL_COLUMN_ERRCODE = "connect_fail_column_errcode";

    /**
     * 连接失败时的网络类型
     */
    private static final String CONNECT_FAIL_COLUMN_NETWORK = "connect_fail_column_network";

    /**
     * 连接失败的ip类型
     */
    private static final String CONNECT_FAIL_COLUMN_IPTYPE = "connect_fail_column_iptype";

    /**
     * 连接失败的运营商, 代码
     */
    private static final String CONNECT_FAIL_COLUMN_SP = "connect_fail_column_sp";

    /**
     * 连接失败次数表失败次数
     */
    private static final String CONNECT_FAIL_COLUMN_COUNT = "connect_fail_column_count";

    /**
     * 连接失败次数表失败次数
     */
    private static final String CONNECT_FAIL_COLUMN_PROTO = "connect_fail_column_proto";

    /**
     * 连接失败次数，上次的时间
     */
    private static final String CONNECT_FAIL_COLUMN_TIME = "connect_fail_column_time";

    private static final String CREATE_CONNECT_FAIL_TABLE_SQL =
            "CREATE TABLE " + TABLE_NAME_CONNECT_FAIL + " ("
                    + CONNECT_FAIL_COLUMN_ID + " INTEGER PRIMARY KEY,"
                    + CONNECT_FAIL_COLUMN_IP + " TEXT,"
                    + CONNECT_FAIL_COLUMN_PORT + " INTEGER,"
                    + CONNECT_FAIL_COLUMN_ERRCODE + " INTEGER,"
                    + CONNECT_FAIL_COLUMN_NETWORK + " INTEGER,"
                    + CONNECT_FAIL_COLUMN_IPTYPE + " INTEGER,"
                    + CONNECT_FAIL_COLUMN_SP + " INTEGER,"
                    + CONNECT_FAIL_COLUMN_PROTO + " INTEGER,"
                    + CONNECT_FAIL_COLUMN_TIME + " INTEGER,"
                    + CONNECT_FAIL_COLUMN_COUNT + " INTEGER);";

    // ///////////////////////////////////////////////////
    protected OcCacheDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // 残酷的现实告诉我们，创建多个表时，要分开多次执行db.execSQL方法！！
        db.execSQL("CREATE TABLE " + TABLE_NAME_DOMAIN + " (" + DOMAIN_COLUMN_ID
                + " INTEGER PRIMARY KEY," + DOMAIN_COLUMN_DOMAIN + " TEXT,"
                + DOMAIN_COLUMN_SP + " TEXT," + DOMAIN_COLUMN_PROTO + " INTEGER," + DOMAIN_COLUMN_EXPIRE_TIME + " TEXT,"
                + DOMAIN_COLUMN_RETEST_TIMESPAN + " INTEGER);"); // 创建domain表

        db.execSQL("CREATE TABLE " + TABLE_NAME_IP + " (" + IP_COLUMN_ID + " INTEGER PRIMARY KEY,"
                + IP_COLUMN_DOMAIN_ID + " INTEGER," + IP_COLUMN_IP + " TEXT,"
                + IP_COLUMN_PORT + " INTEGER," + IP_COLUMN_SP + " INTEGER,"
                + IP_COLUMN_TYPE + " INTEGER," + IP_COLUMN_PRIORITY + " INTEGER);");// 创建ip表

        db.execSQL("CREATE TABLE " + TABLE_NAME_ANS_IP + " (" + AIP_COLUMN_ID
                + " INTEGER PRIMARY KEY,"
                + AIP_COLUMN_IP + " TEXT," + AIP_COLUMN_SP + " INTEGER);");// 创建aip表

        db.execSQL(CREATE_CONNECT_FAIL_TABLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            // 其它情况，直接放弃旧表.
            db.beginTransaction();
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_DOMAIN + ";");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_IP + ";");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ANS_COMMON_SETTING + ";");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ANS_IP + ";");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_CONNECT_FAIL + ";");
            db.setTransactionSuccessful();
            db.endTransaction();
            onCreate(db);
        }
    }

    // //////////////////////////////////

    /**
     * 指定ip端口的失败计数加delta,delta可以为负
     */
    public synchronized void increaseFailCount(FailCountItem increaseItem, int delta) {
        FailCountItem oldItem = getFailCountItem(increaseItem.ip, increaseItem.port, increaseItem.errCode);
        SQLiteDatabase db = getWritableDatabase();
        if (db == null) {
            return;
        }

        if (oldItem == null) {
            if (delta > 0) {
                insertConnectFailItem(db, increaseItem, delta);
            }
        } else {
            int count = 0;
            long delay = AnsSetting.g().getMaxReportConnFailInterval();
            long now = System.currentTimeMillis();
            // 如果上一次连接失败很久远了，就不考虑了。
            if (oldItem.time >= now - 10 * delay) {
                count = oldItem.count + delta;
            }

            if (count <= 0) {
                deleteFailConn(increaseItem.ip, increaseItem.port, increaseItem.errCode);
            } else {
                setConnFailCount(db, oldItem, increaseItem, delta);
            }
        }

        db.close();
    }

    /**
     * 插入某条连接错误计数，delta > 0
     */
    private void insertConnectFailItem(SQLiteDatabase db, FailCountItem item, int delta) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONNECT_FAIL_COLUMN_COUNT, delta);
        contentValues.put(CONNECT_FAIL_COLUMN_ERRCODE, item.errCode);
        contentValues.put(CONNECT_FAIL_COLUMN_IP, item.ip);
        contentValues.put(CONNECT_FAIL_COLUMN_PORT, item.port);
        contentValues.put(CONNECT_FAIL_COLUMN_IPTYPE, item.ipType);
        contentValues.put(CONNECT_FAIL_COLUMN_NETWORK, item.network);
        contentValues.put(CONNECT_FAIL_COLUMN_SP, item.sp);
        contentValues.put(CONNECT_FAIL_COLUMN_PROTO, item.proto);
        contentValues.put(CONNECT_FAIL_COLUMN_TIME, item.time);
        db.insert(TABLE_NAME_CONNECT_FAIL, null, contentValues);
    }

    /**
     * 更新某条连接错误计数，加delta
     */
    private void setConnFailCount(SQLiteDatabase db, FailCountItem oldItem, FailCountItem item, int delta) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONNECT_FAIL_COLUMN_COUNT, oldItem.count + delta);
        contentValues.put(CONNECT_FAIL_COLUMN_TIME, item.time);

        StringBuilder where = new StringBuilder();
        where.append(CONNECT_FAIL_COLUMN_IP);
        where.append(" = ? AND ");
        where.append(CONNECT_FAIL_COLUMN_PORT);
        where.append(" = ? AND ");
        where.append(CONNECT_FAIL_COLUMN_ERRCODE);
        where.append(" = ? ");

        String[] args = new String[]{oldItem.ip, String.valueOf(oldItem.port), String.valueOf(oldItem.errCode)};
        db.update(TABLE_NAME_CONNECT_FAIL, contentValues, where.toString(), args);
    }

    /**
     * 获取指定ip端口的连接失败次数
     */
    public FailCountItem getFailCountItem(String ip, int port, int errCode) {
        SQLiteDatabase db = getReadableDatabase();
        if (db == null) {
            return null;
        }

        FailCountItem item = null;
        StringBuilder where = new StringBuilder();
        where.append(CONNECT_FAIL_COLUMN_IP);
        where.append(" = ? AND ");
        where.append(CONNECT_FAIL_COLUMN_PORT);
        where.append(" = ? AND ");
        where.append(CONNECT_FAIL_COLUMN_ERRCODE);
        where.append(" = ? ");

        String[] args = new String[]{ip, String.valueOf(port), String.valueOf(errCode)};
        Cursor cursor = db.query(TABLE_NAME_CONNECT_FAIL, null, where.toString(), args, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            item = new FailCountItem();
            item.count = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_COUNT));
            item.errCode = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_ERRCODE));
            item.ip = ip;
            item.port = port;
            item.ipType = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_IPTYPE));
            item.network = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_NETWORK));
            item.sp = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_SP));
            item.proto = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_PROTO));
            item.time = cursor.getLong(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_TIME));
        }
        cursor.close();
        db.close();
        return item;
    }

    public List<FailCountItem> getFailCountItemsNeedReportAndClear() {
        List<FailCountItem> items = new ArrayList<FailCountItem>();
        SQLiteDatabase db = getWritableDatabase();
        if (db == null) {
            return items;
        }

        long now = System.currentTimeMillis();
        long delay = AnsSetting.g().getMaxReportConnFailInterval();
        int maxFailNum = AnsSetting.g().getMaxReportConnFailCount();
        StringBuilder where = new StringBuilder();
        where.append(CONNECT_FAIL_COLUMN_TIME).append(" > ").append(now - 10 * delay);
        Cursor cursor = db.query(TABLE_NAME_CONNECT_FAIL, null, where.toString(), null, null, null, null);

        while (cursor.moveToNext()) {
            FailCountItem item = new FailCountItem();
            item.count = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_COUNT));
            item.errCode = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_ERRCODE));
            item.ip = cursor.getString(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_IP));
            item.port = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_PORT));
            item.ipType = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_IPTYPE));
            item.network = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_NETWORK));
            item.sp = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_SP));
            item.proto = cursor.getInt(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_PROTO));
            item.time = cursor.getLong(cursor.getColumnIndex(CONNECT_FAIL_COLUMN_TIME));
            items.add(item);
        }

        deleteReportedFailConn(db);

        cursor.close();
        db.close();

        return items;
    }

    public synchronized void deleteFailConn(String ip, int port, int errCode) {
        SQLiteDatabase db = getWritableDatabase();
        if (db == null) {
            return;
        }

        StringBuilder where = new StringBuilder();
        where.append(CONNECT_FAIL_COLUMN_IP);
        where.append(" = ? AND ");
        where.append(CONNECT_FAIL_COLUMN_PORT);
        where.append(" = ? AND ");
        where.append(CONNECT_FAIL_COLUMN_ERRCODE);
        where.append(" = ? ");

        try {
            db.delete(TABLE_NAME_CONNECT_FAIL, where.toString(), new String[]{ip, String.valueOf(port), String.valueOf(errCode)});
        } catch (Exception e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    /**
     * 清除失败次数。
     */
    private synchronized void deleteReportedFailConn(SQLiteDatabase db) {
        if (db == null) {
            return;
        }

        try {
            db.delete(TABLE_NAME_CONNECT_FAIL, null, null);
        } catch (Exception e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    /**
     * 将ip信息插入到缓存数据库
     *
     * @param ocInfo 域名信息
     */
    public void add(OcAddress ocInfo, boolean isHttp) {
        synchronized (synLock) {
            String sp = ocInfo.sp;
            if (("" + AnsQueryConstants.E_MOBILE_UNKNOWN).equals(sp)) {
                sp = Common.getIpOrMac();
            }

            PrintHandler.addLog(TAG, ">>>将结果缓存至数据库 key=" + ocInfo.domain + sp + ",tId:"
                    + Thread.currentThread().getId());
            /** 如果数据库中已存在，则先删除 */
            List<DomainInfo> domainInfos = getDomainInfo(ocInfo.domain, sp, isHttp);
            for (DomainInfo di : domainInfos) {
                delete(di.id);
            }

            SQLiteDatabase db = getWritableDatabase();
            try {
                db.beginTransaction();

                /** 插入域名信息 */
                ContentValues cv = new ContentValues();
                cv.put(DOMAIN_COLUMN_DOMAIN, ocInfo.domain);
                cv.put(DOMAIN_COLUMN_SP, sp);
                cv.put(DOMAIN_COLUMN_EXPIRE_TIME, ocInfo.expireTime);
                cv.put(DOMAIN_COLUMN_RETEST_TIMESPAN, ocInfo.retestTimeSpan);
                cv.put(DOMAIN_COLUMN_PROTO, isHttp ? ReportConstanst.PROTO_HTTP : ReportConstanst.PROTO_TCP);
                long domainId = db.insert(TABLE_NAME_DOMAIN, null, cv);

                /** 插入IP信息 */
                for (AddressEntity ae : ocInfo.addressList) {
                    cv = new ContentValues();
                    cv.put(IP_COLUMN_DOMAIN_ID, domainId);
                    cv.put(IP_COLUMN_IP, ae.getIp());
                    cv.put(IP_COLUMN_PORT, ae.getPort());
                    cv.put(IP_COLUMN_SP, ae.getSp());
                    cv.put(IP_COLUMN_TYPE, ae.getType());
                    cv.put(IP_COLUMN_PRIORITY, ae.getPriority());

                    db.insert(TABLE_NAME_IP, null, cv);
                }

                db.setTransactionSuccessful();
            } catch (Exception e) {
                WnsClientLog.e(TAG, ">>>添加域名信息出错[" + ocInfo.domain + "][" + sp
                        + "]" + e.getMessage(), e);
                PrintHandler.addLog(TAG, ">>>将结果缓存至数据库异常 [key:" + ocInfo.domain + sp + "][ERR:"
                        + e.getMessage() + "]");
            } finally {
                db.endTransaction();
                db.close();
            }
        }
    }

    /**
     * 更新IP的优先级
     *
     * @param domain 域名
     * @param sp     运营商(无运营商时，为MAC地址)
     * @param curIP  当前最优IP
     * @param newIP  新的更优IP
     */
    public void updateIp(String domain, String sp, boolean isHttp, String curIP, String newIP) {
        synchronized (synLock) {
            PrintHandler.addLog(TAG, ">>>更新IP优先级.[原IP:" + curIP + "][新IP:" + newIP + "]");
            List<DomainInfo> domainInfos = getDomainInfo(domain, sp, isHttp);
            if (domainInfos == null || domainInfos.size() == 0) {
                // WnsClientLog.e(TAG, ">>>更新IP出错。没有查询到域名信息，直接返回[" + domain + "][" + sp +
                // "]");
                PrintHandler.addLog(TAG, ">>>更新IP优先级出错。没有查询到域名信息，直接返回[" + domain + "][" + sp + "]");
                return;
            }

            SQLiteDatabase db = getWritableDatabase();
            try {

                String whereClause = IP_COLUMN_IP + " =? AND " + IP_COLUMN_DOMAIN_ID + " =?";
                /** 理论上domainInfos只有一条数据 */
                int dId = domainInfos.get(0).id;

                db.beginTransaction();

                ContentValues cv = new ContentValues();

                cv.put(IP_COLUMN_PRIORITY, Constanst.IP_PRI_2);
                /** 将当前的最优IP降一个优先级 */
                db.update(TABLE_NAME_IP, cv, whereClause, new String[]{
                        curIP, "" + dId
                });

                cv.put(IP_COLUMN_PRIORITY, Constanst.IP_PRI_1);
                /** 更新测速发现的更优IP优先级 */
                db.update(TABLE_NAME_IP, cv, whereClause, new String[]{
                        newIP, "" + dId
                });

                db.setTransactionSuccessful();
                PrintHandler.addLog(TAG, ">>>更新IP优先级成功.[原IP:" + curIP + "][新IP:" + newIP + "]");
            } catch (Exception e) {
                WnsClientLog.e(TAG, ">>>更新IP出错[" + domain + "][" + sp + "]" +
                        e.getMessage(), e);
                PrintHandler.addLog(TAG, ">>>更新IP优先级出错:[domain:" + domain + "][sp:" + sp + "][ERR:"
                        + e.getMessage() + "]");
            } finally {
                db.endTransaction();
                if (db != null) {
                    db.close();
                }
            }
        }
    }

    /**
     * 根据id删除缓存
     *
     * @param id 域名表id
     */
    public void delete(int id) {
        synchronized (synLock) {
            SQLiteDatabase db = getWritableDatabase();
            try {

                //WnsClientLog.v(TAG, "###删除数据：" + id);
                db.delete(TABLE_NAME_DOMAIN, DOMAIN_COLUMN_ID + " = ?", new String[]{String.valueOf(id)});
                db.delete(TABLE_NAME_IP, IP_COLUMN_DOMAIN_ID + " = ?", new String[]{String.valueOf(id)});
                //WnsClientLog.v(TAG, "###删除数据完成：" + id);

            } catch (Exception e) {
                WnsClientLog.e(TAG, "###删除数据异常：" + id + ". " + e.getMessage(), e);
            } finally {
                db.close();
            }
        }
    }

    /**
     * 删除某个域名的所有信息。
     */
    public void deleteDomain(String domain, String sp, boolean isHttp) {
        List<DomainInfo> domains = getDomainInfo(domain, sp, isHttp);
        if (domains != null && domains.size() > 0) {
            WnsClientLog.i(TAG, String.format("domain %s exist, delete it from db.", domain));
            delete(domains.get(0).id);
        } else {
            WnsClientLog.i(TAG, String.format("domain %s didn't exist in db.", domain));
        }
    }

    /**
     * 清除缓存数据
     */
    public void clear() {
        synchronized (synLock) {
            SQLiteDatabase db = getWritableDatabase();
            try {

                //WnsClientLog.v(TAG, "###清除数据：");
                db.delete(TABLE_NAME_DOMAIN, null, null);
                db.delete(TABLE_NAME_IP, null, null);
                db.delete(TABLE_NAME_CONNECT_FAIL, null, null);
                //WnsClientLog.v(TAG, "###清除数据完成：");

            } catch (Exception e) {
                WnsClientLog.e(TAG, "###清除数据异常：" + e.getMessage(), e);
            } finally {
                db.close();
            }
        }
    }

    /**
     * 清理过期数据
     */
    protected void clearExpiredItem() {
        synchronized (synLock) {

            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = null;

            long expireTime = 0;
            int domainId = 0;
            try {
                cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_DOMAIN, null);
                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();

                    do {

                        expireTime = cursor.getLong(cursor.getColumnIndex(DOMAIN_COLUMN_EXPIRE_TIME));

                        //WnsClientLog.w(TAG, "###清理过程###[domain:" + cursor.getString(cursor.getColumnIndex(DOMAIN_COLUMN_DOMAIN))
                        //+ "][sp:" + cursor.getString(cursor.getColumnIndex(DOMAIN_COLUMN_SP)) + "][expireTime:" + expireTime + "]");

                        if (System.currentTimeMillis() > expireTime) {
                            //WnsClientLog.w(TAG, "###清理过程###[已过期，清理]");
                            domainId = cursor.getInt(cursor.getColumnIndex(DOMAIN_COLUMN_ID));
                            db.delete(TABLE_NAME_DOMAIN, DOMAIN_COLUMN_ID + " = ?", new String[]{String.valueOf(domainId)});
                            db.delete(TABLE_NAME_IP, IP_COLUMN_DOMAIN_ID + " = ?", new String[]{String.valueOf(domainId)});
                        }

                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {

                WnsClientLog.e(TAG, "###清理过期数据异常", e);
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        }
    }

    /**
     * 获取缓存的IP信息
     *
     * @param domain 域名
     * @param sp     网络运营商
     * @return List<AddressEntity>
     */
    public OcAddress _get(String domain, String sp, boolean isHttp) {
        synchronized (synLock) {
            // WnsClientLog.v(TAG, "###获取数据：[" + domain + "][" + sp + "]");
            PrintHandler.addLog(TAG, ">>>从DB缓存中查找IP信息: [domain:" + domain + "][sp:" + sp + "]");
            String sql = "SELECT d.expire_time,d.retest_ts,i.* FROM domain d JOIN ip i ON d.id = i.d_id WHERE d.domain= ? AND d.sp= ? AND d.proto = ? order by i.pri";

            OcAddress cache = null;

            /** 地址列表 */
            List<AddressEntity> list = null;

            /** 过期时间 */
            Long expireTime = 0L;

            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = null;
            try {
                cursor = db.rawQuery(sql, new String[]{
                        domain, sp, String.valueOf(isHttp ? ReportConstanst.PROTO_HTTP : ReportConstanst.PROTO_TCP)
                });
                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();

                    // WnsClientLog.v(TAG, "###获取数据：[" + domain + "][" + sp + "]" +
                    // "获取到 " + cursor.getCount()+" 条数据.");
                    PrintHandler.addLog(TAG, ">>>从DB缓存中查找IP信息: [domain:" + domain + "][sp:" + sp
                            + "][共有 " + cursor.getCount() + " 条数据]");

                    expireTime = cursor.getLong(cursor.getColumnIndex(DOMAIN_COLUMN_EXPIRE_TIME));
                    if (System.currentTimeMillis() > expireTime) {

                        // WnsClientLog.w(TAG, "###获取数据：[" + domain + "][" + sp + "]" +
                        // "数据已经过期[" + expireTime + "]");
                        PrintHandler.addLog(TAG, ">>>从DB缓存中查找IP信息: [domain:" + domain + "][sp:" + sp
                                + "][数据已经过期: " + expireTime + " ]");
                        /** 当数据已经过期，从DB删除，并直接返回NULL */
                        delete(cursor.getInt(cursor.getColumnIndex(IP_COLUMN_DOMAIN_ID)));
                        return null;
                    }

                    cache = new OcAddress();
                    cache.domain = domain;
                    cache.sp = sp;
                    cache.expireTime = expireTime;
                    cache.retestTimeSpan = cursor.getInt(cursor
                            .getColumnIndex(DOMAIN_COLUMN_RETEST_TIMESPAN));

                    list = new ArrayList<AddressEntity>();

                    do {

                        AddressEntity ae = new AddressEntity(
                                cursor.getString(cursor.getColumnIndex(IP_COLUMN_IP)) // ip
                                , cursor.getInt(cursor.getColumnIndex(IP_COLUMN_PORT)) // 端口
                                , cursor.getShort(cursor.getColumnIndex(IP_COLUMN_TYPE)) // ip类型
                                , cursor.getShort(cursor.getColumnIndex(IP_COLUMN_SP)) // 运营商
                                , cursor.getShort(cursor.getColumnIndex(IP_COLUMN_PRIORITY)) // 优先级
                        );
                        list.add(ae);

                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                WnsClientLog.e(TAG, "###获取数据异常：[" + domain + "][" + sp + "]" +
                        e.getMessage(), e);
                PrintHandler.addLog(TAG, ">>>从DB缓存中查找IP信息: [domain:" + domain + "][sp:" + sp
                        + "][查询异常: " + e.getMessage() + " ]");
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }

            if (cache != null) {
                cache.addressList = list;
                /*
                 * WnsClientLog.w(TAG, "###db缓存数据：[domain:" + cache.domain +
                 * "][networkType:" + cache.sp + "][expireTime:" +
                 * cache.expireTime + "][retestTimeSpan:" + cache.retestTimeSpan
                 * + "]"); for(AddressEntity add : cache.addressList){
                 * WnsClientLog.v(TAG, "###db缓存数据IP详细信息:[ip:" + add.getIp() + "][port:" +
                 * add.getPort() + "][type:" + add.getType() + "][sp:" +
                 * add.getSp() + "][pri:" + add.getPriority() + "]"); }
                 */
            }

            return cache;
        }
    }

    /**
     * 域名+网络类型组合为key查询域名信息是否存在。该方法只在类中调用，不加锁
     *
     * @param domain 域名
     * @param sp     运营商
     * @return List<DomainInfo>
     */
    private List<DomainInfo> getDomainInfo(String domain, String sp, boolean isHttp) {

        //WnsClientLog.v(TAG, "###查询域名信息：[" + domain + "][" + sp + "]");

        List<DomainInfo> list = new ArrayList<DomainInfo>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM ");
        sql.append(TABLE_NAME_DOMAIN);
        sql.append(" WHERE ");
        sql.append(DOMAIN_COLUMN_DOMAIN);
        sql.append(" =? ");
        sql.append(" AND ");
        sql.append(DOMAIN_COLUMN_SP);
        sql.append(" =? ");
        sql.append(" AND ");
        sql.append(DOMAIN_COLUMN_PROTO);
        sql.append(" =? ;");

        try {
            cursor = db.rawQuery(sql.toString(), new String[]{
                    domain, "" + sp, String.valueOf(isHttp ? ReportConstanst.PROTO_HTTP : ReportConstanst.PROTO_TCP)
            });
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();

                //WnsClientLog.v(TAG, "###查询域名信息：[" + domain + "][" + sp + "]" + "查询到" + cursor.getCount()
                //+ "条数据");

                do {
                    DomainInfo di = new DomainInfo();
                    di.id = cursor.getInt(cursor.getColumnIndex(DOMAIN_COLUMN_ID));
                    di.domain = cursor.getString(cursor.getColumnIndex(DOMAIN_COLUMN_DOMAIN));
                    di.sp = cursor.getString(cursor.getColumnIndex(DOMAIN_COLUMN_SP));

                    list.add(di);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            WnsClientLog.e(TAG, "###查询域名信息异常：[" + domain + "][" + sp + "]" + e.getMessage(), e);
        } finally {

        }

        return list;
    }
}
