/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 缓存数据的存储链表，这里通过{@link LinkedHashMap}实现轻量级的LRU缓存
 * <br/>替换策略(Least Rencetly Used 最近最少使用 )
 *
 * @param <K> Key
 * @param <V> Value
 * @author 
 */
public class MemoryCache<K, V> extends LinkedHashMap<K, V> {

    /**
     * 序列化版本ID
     */
    private static final long serialVersionUID = -6940751117906094384L;

    /**
     * 缓存初始容量大小
     */
    private static int initSize = 8;

    /**
     * 缓存容量，默认为initSize
     */
    private int capacity = initSize;


    /**扩容因子，当容量达最大值时扩容的百分比。经官方测试，0.75为时间和空间的最优值*/
    /**
     * 注：本缓存器实现了固定容量缓存，当容量超出指定容量的最大值时，缓存器会根据LRU策略将超出的缓存删掉，不会再进行扩容
     */
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * 资源锁
     */
    private Object lock = new Object();

    /**
     * 创建一个Cache对象
     *
     * @param capacity cache的容量，capacity必须大于0，否则将使用默认值
     */
    public MemoryCache(int capacity) {
        super(initSize, DEFAULT_LOAD_FACTOR, true);

        //如果传入的capacity为大于初始容量值，则更新容量值
        if (capacity > initSize) {
            this.capacity = capacity;
        }

    }

    /**
     * 创建一个Cache对象
     *
     * @param initSize 初始容量大小
     * @param capacity cache的容量，capacity必须大于0，否则将使用默认值
     */
    public MemoryCache(int initSize, int capacity) {
        super(initSize, DEFAULT_LOAD_FACTOR, true);

        //如果传入的capacity为大于初始容量值，则更新容量值
        if (capacity > initSize) {
            this.capacity = capacity;
        }

    }

    ///////////////////////////////////////////////

    @Override
    public V get(Object key) {

        synchronized (lock) {
            return super.get(key);
        }
    }

    @Override
    public V put(K key, V value) {
        synchronized (lock) {
            return super.put(key, value);
        }
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
        synchronized (lock) {

            super.putAll(map);
        }
    }

    @Override
    public V remove(Object key) {
        synchronized (lock) {
            return super.remove(key);
        }
    }

    @Override
    public void clear() {
        synchronized (lock) {
            super.clear();
        }
    }

    @Override
    public boolean containsKey(Object key) {
        synchronized (lock) {
            return super.containsKey(key);
        }
    }

    @Override
    public int size() {

        synchronized (lock) {
            return super.size();
        }
    }

    @Override
    protected boolean removeEldestEntry(java.util.Map.Entry<K, V> eldest) {
        return size() > capacity;
    }

    //////////////////////////////////////////////////

    public int getCapcity() {

        return capacity;
    }

}
