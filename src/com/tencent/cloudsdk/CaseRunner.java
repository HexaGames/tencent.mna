/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.tencent.cloudsdk.ansquery.QueryMain;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.AddressEntity;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.data.SingleIpItem;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.cloudsdk.utils.SpeedConfig;
import com.tencent.record.debug.WnsClientLog;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class CaseRunner {
    public static final String TAG = "CaseRunner";
    private Element mRootElement;
    private Context m_ctx;
    private static CaseRunner runner = null;
    private static HashMap<String, ArrayList<SingleIpItem>> mDomainInfo;
    private static ArrayList<ArrayList<CaseStep>> mCaseInfo;
    private static MobileAdmin mMobileAdmin = null;
    private static WifiAdmin mWifiAdmin = null;

    public CaseRunner(Context context) {
        m_ctx = context;
    }

    static public CaseRunner GetInstance(Context context) {
        if (runner == null) {
            runner = new CaseRunner(context);
            mDomainInfo = new HashMap<String, ArrayList<SingleIpItem>>();
            mCaseInfo = new ArrayList<ArrayList<CaseStep>>();
            mMobileAdmin = new MobileAdmin(context);
            mWifiAdmin = new WifiAdmin(context);
        }
        return runner;
    }

    private class CaseStep {
        String stepName;
        String stepPara;
        String stepWait = "0";
        String stepExpect = "";
    }

    public void LoadCaseFile() {
        /***********************************************
         * 解析用例文件到变量mRootElement
         * @param xmlFile  要解析的文件路径
         */
        try {
            File targetFile = new File(Environment.getExternalStorageDirectory().getPath() + "/SpeedSDK.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document document = db.parse(targetFile);
            mRootElement = document.getDocumentElement();

            //载入域名信息
            NodeList domainList = mRootElement.getElementsByTagName("TestDomain");
            if (domainList.getLength() == 0) {
                throw new Exception("沒有配置域名!!");
            }
            for (int i = 0; i < domainList.getLength(); ++i) {
                ArrayList<SingleIpItem> ips = new ArrayList<SingleIpItem>();
                Element domain = (Element) domainList.item(i);
                NodeList ipList = domain.getElementsByTagName("machine");
                for (int j = 0; j < ipList.getLength(); ++j) {
                    Element machine = (Element) ipList.item(j);
                    SingleIpItem item = new SingleIpItem();
                    item.ip = machine.getAttribute("ip");
                    item.port = Integer.parseInt(machine.getAttribute("port"));
                    item.type = Integer.parseInt(machine.getAttribute("type"));
                    item.sp_type = Integer.parseInt(machine.getAttribute("sp_type"));
                    ips.add(item);
                }
                mDomainInfo.put(domain.getAttribute("name"), ips);
            }

            //载入用例信息
            NodeList caseList = mRootElement.getElementsByTagName("Case");
            for (int i = 0; i < caseList.getLength(); ++i) {
                Element onecase = (Element) caseList.item(i);

                ArrayList<CaseStep> steps = new ArrayList<CaseStep>();
                NodeList stepList = onecase.getElementsByTagName("step");
                for (int j = 0; j < stepList.getLength(); ++j) {
                    Element stepElement = (Element) stepList.item(j);
                    CaseStep step = new CaseStep();
                    step.stepName = stepElement.getAttribute("name");
                    if (stepElement.hasAttribute("para")) {
                        step.stepPara = stepElement.getAttribute("para");
                    }
                    if (stepElement.hasAttribute("sleep")) {
                        step.stepWait = stepElement.getAttribute("sleep");
                    }
                    if (stepElement.hasAttribute("expect")) {
                        step.stepExpect = stepElement.getAttribute("expect");
                    }
                    steps.add(step);
                }
                mCaseInfo.add(steps);
            }
            Toast.makeText(m_ctx, "用例文件加载成功", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            WnsClientLog.v("error", e.getMessage());
        } catch (ParserConfigurationException e) {
            WnsClientLog.v("error", e.getMessage());
        } catch (SAXException e) {
            WnsClientLog.v("error", e.getMessage());
        } catch (IOException e) {
            WnsClientLog.v("error", e.getMessage());
        } catch (Exception e) {

        }
    }

    public void RunCase() {
        for (String domainport : mDomainInfo.keySet()) {
            String domain = domainport.split(":")[0];
            String port = domainport.split(":")[1];
            ArrayList<SingleIpItem> ips = mDomainInfo.get(domainport);

            for (int i = 0; i < mCaseInfo.size(); i++) {
                ArrayList<CaseStep> onecase = mCaseInfo.get(i);
                for (int j = 0; j < onecase.size(); j++) {
                    CaseStep step = onecase.get(j);
                    doStep(domain, port, step);

                }
            }
        }
    }

    private void doStep(final String domain, final String port, CaseStep step) {
        if (step.stepName == "openWifi") {
            //打开wifi
            mWifiAdmin.openWifi();
        } else if (step.stepName == "closeWifi") {
            //关闭wifi
            mWifiAdmin.closeWifi();
        } else if ("openMobile".equals(step.stepName)) {
            //打开流量
            mMobileAdmin.OpenMobileData();
        } else if ("closeMobile".equals(step.stepName)) {
            //关闭流量
            mMobileAdmin.CloseMobileData();
        } else if ("clearCache".equals(step.stepName)) {
            //清除缓存
            OcCacheManager.getInstance(m_ctx).clear();
        } else if ("resetTestTime".equals(step.stepName)) {
            //重置测速时间
            SpeedConfig.reset(m_ctx, domain);
        } else if ("sleep".equals(step.stepName)) {
            //休眠一阵
            try {
                Thread.sleep(Integer.parseInt(step.stepPara));
            } catch (NumberFormatException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            } catch (InterruptedException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            }
        } else if ("OC_QUERY".equals(step.stepName)) {
            //OC 加速查询
            GlobalContext.setContext(m_ctx);
            new queryThread(domain).start();
        } else if ("OC_SPEEDTEST".equals(step.stepName)) {
            //OC 加速查询，关于网络的操作必须新起线程，不能在主线程里做，要不然会被StrictMode拦下来
            new Thread() {
                public void run() {
                    // httpDownload.execute("a.php");
                    MainActivity.RSDomainItem item = new MainActivity.RSDomainItem();
                    item.domain = domain;
                    item.port = Integer.parseInt(port);
                    item.type = MainActivity.RSDomainItem.TYPE_FOR_TCP_TEST;
                    item.display = "";
                    TcpTest tcpTest = new TcpTest();
                    tcpTest.setData(item);
                    tcpTest.doTest();
                    // SpeedThreadManager.getInstance().execute(domain,
                    // port);
                    // TestApi.test();
                }

                ;
            }.start();
        } else if ("pass".equals(step.stepName)) {
            //do nothing
        } else {
            Toast.makeText(m_ctx, "No Step Named:" + step.stepName, Toast.LENGTH_LONG).show();
        }

        int slp = Integer.parseInt(step.stepWait);
        if (slp != 0) {
            try {
                Thread.sleep(slp);
            } catch (InterruptedException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            }
        }
    }

    private class queryThread extends Thread {
        private String domain;

        public queryThread(String domain) {
            this.domain = domain;
        }

        @Override
        public void run() {
            OcAddress address = QueryMain.queryFromOC(domain, false);
            if (address != null) {
                for (AddressEntity add : address.addressList) {
                    //WnsClientLog.v(TAG, "ip:" + add.getIp() + ",port:" + add.getPort() + ",type:" + add.getType() + ",sp:" + add.getSp() + ",pri:" + add.getPriority());
                    PrintHandler.addLog(TAG, ">>>查询结果: [IP:" + add.getIp() + "]\n[port:" + add.getPort() + "]\n[type:" + add.getType() + "]\n[sp:" + add.getSp() + "]\n[pri:" + add.getPriority() + "]");
                }
            }
            super.run();
        }
    }


}
