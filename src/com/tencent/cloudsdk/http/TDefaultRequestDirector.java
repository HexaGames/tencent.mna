/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.http;

import com.tencent.cloudsdk.data.SingleIpItem;
import com.tencent.cloudsdk.utils.PrintHandler;

import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.AuthenticationHandler;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.UserTokenHandler;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.impl.client.DefaultRequestDirector;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpRequestExecutor;

import java.io.IOException;

/**
 * @author 
 */
class TDefaultRequestDirector extends DefaultRequestDirector {
    private static final String TAG = "TDefaultRequestDirector";

    public TDefaultRequestDirector(HttpRequestExecutor requestExec, ClientConnectionManager conman, ConnectionReuseStrategy reustrat, ConnectionKeepAliveStrategy kastrat, HttpRoutePlanner rouplan, HttpProcessor httpProcessor, HttpRequestRetryHandler retryHandler, RedirectHandler redirectHandler, AuthenticationHandler targetAuthHandler, AuthenticationHandler proxyAuthHandler, UserTokenHandler userTokenHandler, HttpParams params) {
        super(requestExec, conman, reustrat, kastrat, rouplan, httpProcessor, retryHandler, redirectHandler, targetAuthHandler, proxyAuthHandler, userTokenHandler, params);
    }

    @Override
    public HttpResponse execute(HttpHost target, HttpRequest request, HttpContext context) throws HttpException, IOException {
        String targetHost = target.getHostName();
        int targetPort = target.getPort();
        targetPort = targetPort < 0 ? 80 : targetPort;

        SingleIpItem betterItem = HttpOptimizer.findBetterAddress(targetHost, targetPort);
        String betterHost = betterItem.ip;
        int betterPort = betterItem.port;

        PrintHandler.addLog(TAG, String.format("修改Request的属性Host为%s:%d", targetHost, targetPort));
        request.setHeader("Host", targetHost + ":" + targetPort);

        HttpHost betterTarget = new HttpHost(betterHost, betterPort, target.getSchemeName());
        PrintHandler.addLog(TAG, String.format("修改target的Host和端口，从%s到%s", target, betterTarget));

        return super.execute(betterTarget, request, context);
    }
}
