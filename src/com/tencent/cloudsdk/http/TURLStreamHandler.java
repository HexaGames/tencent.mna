/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.http;

import com.tencent.cloudsdk.utils.PrintHandler;

import java.io.IOException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * 和指定的URL进行通信。加入了加速策略。<br/>
 * 在URL的构造函数中指定，可以使HTTP访问加速。
 *
 * @author 
 */
public class TURLStreamHandler extends URLStreamHandler {
    private static final String TAG = "TURLStreamHandler";

    @Override
    protected URLConnection openConnection(URL u) throws IOException {
        return openConnectionInternal(u, null);
    }

    @Override
    protected URLConnection openConnection(URL u, Proxy proxy) throws IOException {
        return openConnectionInternal(u, proxy);
    }

    private URLConnection openConnectionInternal(URL u, Proxy proxy) throws IOException {
        String host = u.getHost();
        int port = u.getPort();
        HttpOptimizer mHttpOptimizer = new HttpOptimizer(u.toString());
        String betterUrlStr = mHttpOptimizer.findBetterUrl();
        PrintHandler.addLog(TAG, String.format("转换URL从 %s 到 %s", u.toString(), betterUrlStr));

        URL betterUrl = new URL(betterUrlStr);
        URLConnection connection = (proxy == null ? betterUrl.openConnection() : betterUrl.openConnection(proxy));
        connection.setRequestProperty("Host", port < 0 ? host : host + ":" + port);
        PrintHandler.addLog(TAG, String.format("替换Host属性为%s", port < 0 ? host : host + ":" + port));

        return connection;
    }
}
