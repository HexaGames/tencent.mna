/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.http;

import com.tencent.cloudsdk.data.SingleIpItem;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.tsocket.SpeedThreadManager;
import com.tencent.cloudsdk.utils.DebugCfg;
import com.tencent.cloudsdk.utils.IpUtils;
import com.tencent.cloudsdk.utils.NullReturnException;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.record.debug.WnsClientLog;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * 主要用于查找访问速度较快的URL，来代理原来URL的HTTP请求。<br/><br/>
 * <p/>
 * 如果代码使用了DefaultHttpClient或者AndroidHttpClient，可以不使用该类，而直接使用TDefaultHttpClient或TAndroidHttpClient来达到加速效果。<br/>
 * 如果代码使用了HttpURLConnection来发送HTTP请求，可以不使用该类，直接使用在URL的构造函数中加入TURLStreamHandler的方式.
 *
 * @author 
 */
public class HttpOptimizer {
    public static final String TAG = "HttpOptimizer";
    private URL mUrl;

    public HttpOptimizer(String url) throws MalformedURLException {
        mUrl = new URL(url);
    }

    /**
     * 得到访问速度较快的代理Url <br/>
     * 可以使用该Url来代理http请求，但是需要将请求的Http包头的Host字段改为原来URL的Host.<br/>
     * 如果指定了端口，需要将包头的Host字段改为Host:Port<br/>
     * 该方法可能会比较耗时，请在非主线程中调用。
     *
     * @return 返回代理url
     */
    public String findBetterUrl() {
        URL originUrl = mUrl;
        if (!originUrl.getProtocol().equals("http")) {
            return originUrl.toString();
        }

        String host = originUrl.getHost();
        int port = originUrl.getPort();
        SingleIpItem betterHost = findBetterAddress(host, port);

        URL betterUrl;
        try {
            betterUrl = new URL("http", betterHost.ip, betterHost.port, originUrl.getFile());
        } catch (MalformedURLException e) {
            WnsClientLog.e(TAG, "findBetterUrl, " + originUrl, e);
            betterUrl = originUrl;
        }

        return betterUrl.toString();
    }

    /**
     * 得到访问速度较快的Host
     */
    static SingleIpItem findBetterAddress(String host, int port) {
        SingleIpItem item = new SingleIpItem();
        item.ip = host;
        item.port = port < 0 ? 80 : port;

        if (GlobalContext.getContext() == null) {
            return item;
        }

        if (IpUtils.isIpFormat(host)) {
            return item;
        }

        List<SingleIpItem> ipItems;
        try {
            ipItems = IpUtils.getIpInfosFromDB(GlobalContext.getContext(), host, true);
            SpeedThreadManager.getInstance().execute(host, item.port, true);
        } catch (NullReturnException e) {
            WnsClientLog.w(TAG, e.getMessage(), e);
            return item;
        } catch (NoClassDefFoundError error) {
            WnsClientLog.e(TAG, "NoClassDefFoundError", error);
            return item;
        }

        if (ipItems == null || ipItems.size() <= 0) {
            return item;
        }

        SingleIpItem resultIpItem = ipItems.get(0);
        if (resultIpItem.type != IpUtils.TYPE_OC) {
            resultIpItem.port = item.port;
        }

        resultIpItem = selectOcForDebug(ipItems, resultIpItem);
        printIpType(resultIpItem);

        return resultIpItem;
    }

    private static void printIpType(SingleIpItem resultIpItem) {
        String typeStr = "[OCIP]";
        if (resultIpItem.type == IpUtils.TYPE_RS) {
            typeStr = "[RSIP]";
        } else if (resultIpItem.type == IpUtils.TYPE_RS_SPEED) {
            typeStr = "[RS_SPEED_IP]";
        }

        PrintHandler.addLog(TAG, String.format("查找更快的IP，最终选择%s%s", resultIpItem.ip, typeStr));
    }

    private static SingleIpItem selectOcForDebug(List<SingleIpItem> ipItems, SingleIpItem result) {
        SingleIpItem item = result;
        if (DebugCfg.FORCE_HTTP_USE_OC) {
            for (SingleIpItem singleIpItem : ipItems) {
                if (singleIpItem.type == IpUtils.TYPE_OC) {
                    item = singleIpItem;
                }
            }
        }

        return item;
    }
}
