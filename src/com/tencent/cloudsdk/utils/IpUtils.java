/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.utils;

/**
 * 各类型ip数组处理类
 *
 * @author 
 * @date 2013-04-22
 */

import android.content.Context;
import android.text.TextUtils;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.ansquery.QueryMain;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.AddressEntity;
import com.tencent.cloudsdk.data.ListIpItem;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.data.SingleIpItem;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.record.debug.WnsClientLog;

import java.util.ArrayList;
import java.util.List;

//import android.util.Log;

public class IpUtils {
    private final static String TAG = IpUtils.class.getSimpleName();
    public final static int TYPE_OC = AnsQueryConstants.E_IP_TYPE_OC;
    public final static int TYPE_RS = AnsQueryConstants.E_IP_TYPE_RS;
    public final static int TYPE_RS_SPEED = AnsQueryConstants.E_IP_TYPE_TEST; // 测速站点，即rs'

    /**
     * 从db中查询到所有ip，包含oc列表，rsip，rs'ip
     *
     * @throws NullReturnException
     */
    public static List<SingleIpItem> getIpInfosFromDB(Context ctx, String domain, boolean isHttp)
            throws NullReturnException {
        //long startTime = System.currentTimeMillis();
        OcAddress ocAddress = QueryMain.queryFromOC(domain, isHttp);
        //long endTime = System.currentTimeMillis();
        //WnsClientLog.i(Tag, ">>>从oc查询模块查询耗时:" + (endTime - startTime));
        if (ocAddress == null || ocAddress.addressList == null || ocAddress.addressList.size() == 0) {
            throw new NullReturnException("queryFromOc() return null");
        }
        long time = ocAddress.retestTimeSpan;
        //WnsClientLog.i(Tag, ">>>测速间隔为:" + (time * 1000));
        SpeedConfig.saveFreq(time * 1000);
        List<AddressEntity> ipList = ocAddress.addressList;
        if (ipList == null || ipList.size() == 0) {
            throw new NullReturnException("getIpInfosFromDB return null");
        }
        List<SingleIpItem> result = new ArrayList<SingleIpItem>();
        for (AddressEntity addressEntity : ipList) {
            SingleIpItem singleIpItem = new SingleIpItem();
            singleIpItem.ip = addressEntity.getIp();
            singleIpItem.port = addressEntity.getPort();
            singleIpItem.sp_type = addressEntity.getSp();
            singleIpItem.type = addressEntity.getType();
            result.add(singleIpItem);
            //WnsClientLog.i(Tag, "ip=" + singleIpItem.ip + " port=" + singleIpItem.port + " sp_type="
            //+ singleIpItem.sp_type + " type=" + singleIpItem.type);
        }
        return result;
//         List<SingleIpItem> result = new ArrayList<SingleIpItem>();
//         SingleIpItem testOcItem = new SingleIpItem();   // 移动
//         testOcItem.ip = "120.196.211.56";
//         testOcItem.type = TYPE_OC;
//         testOcItem.sp_type = 4;
//         testOcItem.port = 2080;
//         result.add(testOcItem);
//        
////         SingleIpItem testOcItem2 = new SingleIpItem(); //联通
////         testOcItem2.ip = "112.95.139.43";
////         testOcItem2.type = TYPE_OC;
////         testOcItem2.sp_type = 2;
////         testOcItem2.port = 2080;
////         result.add(testOcItem2);
////         
////         SingleIpItem testOcItem3 = new SingleIpItem(); //电信
////         testOcItem3.ip = "113.105.73.148";
////         testOcItem3.type = TYPE_OC;
////         testOcItem3.sp_type = 1;
////         testOcItem3.port = 2080;
////         result.add(testOcItem3);
////          加入测试rsip
//         SingleIpItem testRsItem2 = new SingleIpItem();  // 移动
//         testRsItem2.ip = "1.2.3.4";
//         testRsItem2.type = TYPE_RS;
//         testRsItem2.sp_type = 4;
//         testRsItem2.port = 8080;
//         result.add(testRsItem2);
//         
////         SingleIpItem testRsItem1 = new SingleIpItem();  // 移动
////         testRsItem1.ip = "1.2.3.4";
////         testRsItem1.type = TYPE_RS;
////         testRsItem1.sp_type = 2;
////         testRsItem1.port = 8080;
////         result.add(testRsItem1);
////         
////         SingleIpItem testRsItem3= new SingleIpItem();  // 移动
////         testRsItem3.ip = "1.2.3.4";
////         testRsItem3.type = TYPE_RS;
////         testRsItem3.sp_type = 1;
////         testRsItem3.port = 8080;
////         result.add(testRsItem3);
//         // 加入测试rs'ip
//         SingleIpItem testSpeedItem = new SingleIpItem(); // 移动
//         testSpeedItem.ip = "1.2.3.4";
//         testSpeedItem.type = TYPE_RS_SPEED;
//         testSpeedItem.sp_type = 4;
//         testSpeedItem.port = 2081;
//         result.add(testSpeedItem);
//        
////         SingleIpItem testSpeedItem2 = new SingleIpItem(); // 联通
////         testSpeedItem2.ip="112.90.14.175";
////         testSpeedItem2.type = TYPE_RS_SPEED;
////         testSpeedItem2.sp_type = 2;
////         testSpeedItem2.port = 2081;
////         result.add(testSpeedItem2);
////         
////         SingleIpItem testSpeedItem3 = new SingleIpItem(); // 联通
////         testSpeedItem3.ip="183.60.118.149";
////         testSpeedItem3.type = TYPE_RS_SPEED;
////         testSpeedItem3.sp_type = 1;
////         testSpeedItem3.port = 2081;
////         result.add(testSpeedItem3);
//      
//         return result;
    }

    /**
     * 根据路由mac或终端ip，域名，ip刷新数据库中的最快标识
     *
     * @param sp，当非wifi时使用sp，wifi时使用ip（或mac）
     * @param orgFirstIp,上次最快的ip
     * @param orgPri,                        更新后的优先级
     * @param newFirstIp,本次最快的ip
     * @param newPri,更新后的优先级
     * @params domain 域名
     */
    public static void updateFastFlag(String domain, String orgFirstIp, String newFirstIp, boolean isHttp) {
        //  调用接口
        String spOrIp = Common.getSpOrIp();
        //WnsClientLog.i(Tag, "domain=" + domain + " spOrIp=" + spOrIp + " orgFirstIp=" + orgFirstIp
        //+ " newFirstIp=" + newFirstIp);
        if (orgFirstIp.equals(newFirstIp)) { //如果上次最快ip和本次测速结果一致，则不更新
            return;
        }
        OcCacheManager.getInstance(GlobalContext.getContext()).updateIp(domain, spOrIp, isHttp, orgFirstIp,
                newFirstIp);
    }

    /**
     * 获取所有oc ，只要有返回，则必定不为null
     */
    public static List<SingleIpItem> getAllOcItem(ListIpItem listIpItem) throws NullReturnException {
        if (listIpItem != null && listIpItem.ocItemList != null && listIpItem.ocItemList.size() > 0) {
            return listIpItem.ocItemList;
        }
        throw new NullReturnException("getAllOcItem return null");
    }

    /**
     * 获取首个oc，只要有返回，则必定不为null
     */
    public static SingleIpItem getFirstOcItem(ListIpItem listIpItem) throws NullReturnException {
        if (listIpItem != null && listIpItem.ocItemList != null && listIpItem.ocItemList.size() > 0) {
            return listIpItem.ocItemList.get(0);
        }
        throw new NullReturnException("getFirstIpItem return null");
    }

    /**
     * 获取测速站点ip，只要有返回，则必定不为null
     */
    public static List<SingleIpItem> getRsSpeedItemList(ListIpItem listIpItem)
            throws NullReturnException {
        if (listIpItem != null && listIpItem.rsSpeedItemList != null
                && listIpItem.rsSpeedItemList.size() > 0) {
            return listIpItem.rsSpeedItemList;
        }
        throw new NullReturnException("getRsIpItem return null");
    }

    /**
     * 获取源站ip，只要有返回，则必定不为null
     */
    public static List<SingleIpItem> getRsIpItemList(ListIpItem listIpItem)
            throws NullReturnException {
        if (listIpItem != null && listIpItem.rsItemList != null && listIpItem.rsItemList.size() > 0) {
            return listIpItem.rsItemList;
        }
        throw new NullReturnException("getRsIpItem return null");
    }

    /**
     * 从db中获取所有ip信息，转换为ListIpItem对象，以便后续操作，只要有返回，则必定不为null
     */
    public static ListIpItem getAllIpInfos(List<SingleIpItem> ipItems) throws NullReturnException {
        if (ipItems == null || ipItems.size() == 0) {
            throw new NullReturnException("getAllIpInfos() return null");
        }
        ListIpItem listIpItem = new ListIpItem();
        List<SingleIpItem> ocList = new ArrayList<SingleIpItem>();
        List<SingleIpItem> rsList = new ArrayList<SingleIpItem>();
        List<SingleIpItem> rsSpeedList = new ArrayList<SingleIpItem>();
        for (SingleIpItem singleIpItem : ipItems) {
            if (singleIpItem.type == TYPE_OC) {
                ocList.add(singleIpItem);
            }
            if (singleIpItem.type == TYPE_RS) {
                rsList.add(singleIpItem);
            }
            if (singleIpItem.type == TYPE_RS_SPEED) {
                rsSpeedList.add(singleIpItem);
            }
        }
        listIpItem.ocItemList = ocList;
        listIpItem.rsItemList = rsList;
        listIpItem.rsSpeedItemList = rsSpeedList;
        return listIpItem;
    }

    /**
     * 从db中获取所有ip信息，转换为ListIpItem对象，以便后续操作
     */
    public static ListIpItem getAllIpInfos(Context ctx, String domain, boolean isHttp)
            throws NullReturnException {
        List<SingleIpItem> ipItems = getIpInfosFromDB(ctx, domain, isHttp);
        return getAllIpInfos(ipItems);
    }

    public static String binaryArray2Ipv4Address(byte[] addr) {
        String ip = "";
        for (int i = 0; i < addr.length; i++) {
            ip += (addr[i] & 0xFF) + ".";
        }
        return ip.substring(0, ip.length() - 1);
    }

    public static byte[] ipv4Address2BinaryArray(String ipAdd) {
        byte[] binIP = new byte[4];
        String[] strs = ipAdd.split("\\.");
        for (int i = 0; i < strs.length; i++) {
            try {
                binIP[i] = (byte) Integer.parseInt(strs[i]);
            } catch (NumberFormatException e) {
            }
        }
        return binIP;
    }

    /**
     * 判断是否是ip
     */
    public static boolean isIpFormat(String param) {
        String reg = "((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)";
        return param.matches(reg);
    }

    /**
     * 将ip转换成整形
     */
    public static long parseIpAddress(String ipAddress) {
        if (ipAddress == null || ipAddress.equals("0") || TextUtils.isEmpty(ipAddress)) {
            return 0;
        }
        ipAddress = ipAddress.trim();
        long[] ip = new long[4];
        // 先找到IP地址字符串中.的位置
        int position1 = ipAddress.indexOf(".");
        int position2 = ipAddress.indexOf(".", position1 + 1);
        int position3 = ipAddress.indexOf(".", position2 + 1);
        // 将每个.之间的字符串转换成整型
        try {
            ip[0] = Long.parseLong(ipAddress.substring(0, position1));
            ip[1] = Long.parseLong(ipAddress.substring(position1 + 1, position2));
            ip[2] = Long.parseLong(ipAddress.substring(position2 + 1, position3));
            ip[3] = Long.parseLong(ipAddress.substring(position3 + 1));
        } catch (NumberFormatException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
        return (ip[0] << 24) + (ip[1] << 16) + (ip[2] << 8) + ip[3];
    }

    public static String longToIP(long longIp) {
        StringBuffer sb = new StringBuffer("");
        // 直接右移24位
        sb.append(String.valueOf((longIp >>> 24)));
        sb.append(".");
        // 将高8位置0，然后右移16位
        sb.append(String.valueOf((longIp & 0x00FFFFFF) >>> 16));
        sb.append(".");
        // 将高16位置0，然后右移8位
        sb.append(String.valueOf((longIp & 0x0000FFFF) >>> 8));
        sb.append(".");
        // 将高24位置0
        sb.append(String.valueOf((longIp & 0x000000FF)));
        return sb.toString();
    }
}
