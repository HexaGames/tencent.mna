/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.utils;

/**
 * 空返回异常类
 *
 * @author 
 * @date 2013-04-22
 */
public class NullReturnException extends Exception {
    private int statusCode = -1;
    private static final long serialVersionUID = -2623309261327598087L;

    public NullReturnException(String msg) {
        super(msg);
    }

    public NullReturnException(Exception cause) {
        super(cause);
    }

    public NullReturnException(String msg, int statusCode) {
        super(msg);
        this.statusCode = statusCode;

    }

    public NullReturnException(String msg, Exception cause) {
        super(msg, cause);
    }

    public NullReturnException(String msg, Exception cause, int statusCode) {
        super(msg, cause);
        this.statusCode = statusCode;

    }

    public int getStatusCode() {
        return this.statusCode;
    }
}
