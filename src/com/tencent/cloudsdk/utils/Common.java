/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.record.debug.WnsClientLog;

import org.apache.http.conn.util.InetAddressUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;

public class Common {

    private static final String TAG = Common.class.getName();

    private Common() {
    }

    ;

    /**
     * 获取运营商代码
     *
     * @param ctx
     * @return
     */
    public static short getSP() {

        short code = AnsQueryConstants.E_MOBILE_UNKNOWN;

        if (GlobalContext.getContext() != null) {

            try {
                TelephonyManager telManager = (TelephonyManager) GlobalContext.getContext()
                        .getSystemService(Context.TELEPHONY_SERVICE);
                String operator = telManager.getSimOperator();

                if (operator != null) {
                    if (operator.equals("46000") || operator.equals("46002")
                            || operator.equals("46007") || operator.equals("46020")) {

                        /** 中国移动 */
                        code = AnsQueryConstants.E_MOBILE_CHINAMOBILE;
                    } else if (operator.equals("46001") || operator.equals("46006")) {

                        /** 中国联通 */
                        code = AnsQueryConstants.E_MOBILE_UNICOM;
                    } else if (operator.equals("46003") || operator.equals("46005")) {

                        /** 中国电信 */
                        code = AnsQueryConstants.E_MOBILE_TELCOM;
                    }

                    //WnsClientLog.v(TAG, ">>> getSP():[operator:" + operator + "]" + "[code:" + "]");
                }
            } catch (Exception e) {
                WnsClientLog.e(TAG, ">>> getSP()异常:" + e.getMessage());
            }
        }

        return code;
    }

    /**
     * 获取自定义当前联网类型
     *
     * @return 网络类型。-1为网络不可用；0为未知网络；1为WIFI；2为2G；3为3G；4为4G
     */
    public static int getNetworkType() {

        try {

            if (GlobalContext.getContext() != null) {

                ConnectivityManager connectivity = (ConnectivityManager) GlobalContext.getContext()
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivity == null) {
                    return AnsQueryConstants.NETWORK_TYPE_UNKNOWN;
                }

                NetworkInfo activeNetInfo = connectivity.getActiveNetworkInfo();
                if (activeNetInfo == null) {
                    return AnsQueryConstants.NETWORK_TYPE_UNCONNECTED;
                }

                if (!activeNetInfo.isAvailable() || !activeNetInfo.isConnected()) {
                    return AnsQueryConstants.NETWORK_TYPE_UNCONNECTED;
                }

                if (activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {

                    return AnsQueryConstants.NETWORK_TYPE_WIFI;
                } else if (activeNetInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                    // 后续这里完善一下，区分具体的2G/3G网络
                    // return activeNetInfo.getSubtype();
                    switch (activeNetInfo.getSubtype()) {
                        case TelephonyManager.NETWORK_TYPE_CDMA:// ~ 14-64 kbps
                        case TelephonyManager.NETWORK_TYPE_IDEN:// ~25 kbps
                        case TelephonyManager.NETWORK_TYPE_1xRTT:// ~ 50-100
                            // kbps
                        case TelephonyManager.NETWORK_TYPE_EDGE:// ~ 50-100 kbps
                        case TelephonyManager.NETWORK_TYPE_GPRS:// ~ 100 kbps
                            return AnsQueryConstants.NETWORK_TYPE_2G;

                        case TelephonyManager.NETWORK_TYPE_EVDO_0:// ~ 400-1000
                            // kbps
                        case TelephonyManager.NETWORK_TYPE_UMTS:// ~ 400-7000
                            // kbps
                        case TelephonyManager.NETWORK_TYPE_EVDO_A:// ~ 600-1400
                            // kbps
                        case TelephonyManager.NETWORK_TYPE_HSPA:// ~ 700-1700
                            // kbps
                        case TelephonyManager.NETWORK_TYPE_HSUPA:// ~ 1-23 Mbps
                        case TelephonyManager.NETWORK_TYPE_HSDPA:// ~ 2-14 Mbps
                        case 15: // 对应TelephonyManager.NETWORK_TYPE_HSPAP: //
                            // 在api level 13下没有此值，但存在此网络类型，下面直接用数值代替
                            return AnsQueryConstants.NETWORK_TYPE_3G;
                        case 13: // 对应TelephonyManager.NETWORK_TYPE_LTE
                            return AnsQueryConstants.NETWORK_TYPE_4G;
                        case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                        default:
                            return AnsQueryConstants.NETWORK_TYPE_UNKNOWN;
                    }
                } else {
                    return AnsQueryConstants.NETWORK_TYPE_UNKNOWN;
                }

                /*
                 * NetworkInfo[] info = connectivity.getAllNetworkInfo(); if
                 * (info != null) { for (int i = 0; i < info.length; i++) { if
                 * (info[i].isConnectedOrConnecting()) { code =
                 * getNetworkType(info[i]); WnsClientLog.v(TAG, ">>> getNetworkType():" +
                 * code); return code; } } }
                 */
            }
        } catch (Exception e) {
            WnsClientLog.e(TAG, ">>> getNetworkType()异常:" + e.getMessage());
        }

        return AnsQueryConstants.NETWORK_TYPE_UNCONNECTED;
    }

    /**
     * @return 如果当前是wifi，则返回domain+ip; 否则，返回domain+sp
     */
    public static String getKey(String domain) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) GlobalContext
                    .getContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetInfo != null
                    && activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                return domain + getIpOrMac();
            } else {
                return domain + getSP();
            }
        } catch (Exception e) {
            // 这里try catch是为了防止某些手机需要权限而crash
        }
        return domain;
    }

    public static String getSpOrIp() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) GlobalContext
                    .getContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetInfo != null
                    && activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                return getIpOrMac();
            } else {
                return String.valueOf(getSP());
            }
        } catch (Exception e) {
            // 这里try catch是为了防止某些手机需要权限而crash
        }
        return "0";
    }

    /**
     * 获取本机IP或路由mac地址，如果能获取到mac地址，则返回mac地址；获取不到则返回本机ip地址
     */
    public static String getIpOrMac() {
        String result = getRouteMac();
        if (result == null || result.equals("0")) {
            return getLocalIpAddress();
        }
        return result;
    }

    /**
     * 获取本机IP函数
     */
    public static String getLocalIpAddress() {
        try {
            String ipv4;
            ArrayList<NetworkInterface> mylist = Collections
                    .list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface ni : mylist) {
                ArrayList<InetAddress> iaList = Collections.list(ni
                        .getInetAddresses());
                for (InetAddress address : iaList) {
                    if (!address.isLoopbackAddress()
                            && InetAddressUtils.isIPv4Address(ipv4 = address
                            .getHostAddress())) {
                        return ipv4;
                    }
                }
            }
        } catch (SocketException ex) {
        }
        return "0";
    }

    public static String getRouteMac() {
        try {
            WifiManager wifiManager = (WifiManager) GlobalContext.getContext().getSystemService(
                    Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo == null) {
                return "0";
            }
            return wifiInfo.getBSSID();
        } catch (SecurityException e) {
            // 这里try catch是为了防止某些手机需要权限而crash
        }
        return "0";
    }
}
