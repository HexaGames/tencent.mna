/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.record.debug.WnsClientLog;

import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * @author 
 */
public class ClientIdManager {
    private static final String TAG = "ClientIdManager";
    private static final String PERF_NAME = "perf_name_client_id";
    private static final String PREF_CLIENT_ID = "pref_client_id";
    private int mClientId;

    private static ClientIdManager sInstance;

    public static synchronized ClientIdManager getInstance() {
        if (sInstance == null) {
            sInstance = new ClientIdManager();
        }
        return sInstance;
    }

    public int getClientId() {
        if (mClientId == 0) {
            mClientId = getClientIdFromPref();
        }

        if (mClientId == 0) {
            mClientId = generateRandom();
            saveClientId();
            saveFlagFileToSdcard();
        }

        return mClientId;
    }

    private int getClientIdFromPref() {
        SharedPreferences sp = GlobalContext.getContext().getSharedPreferences(PERF_NAME, Context.MODE_PRIVATE);
        return sp.getInt(PREF_CLIENT_ID, 0);
    }

    private int generateRandom() {
        int id = new Random().nextInt();
        return id > 0 ? id : -id;
    }

    private void saveClientId() {
        SharedPreferences sp = GlobalContext.getContext().getSharedPreferences(PERF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(PREF_CLIENT_ID, mClientId);
        editor.commit();
    }

    private void saveFlagFileToSdcard() {
        File sdcard = Environment.getExternalStorageDirectory();
        if (sdcard != null) {
            String sdcardStr = sdcard.toString();
            if (!sdcardStr.endsWith(File.separator)) {
                sdcardStr += File.separator;
            }

            String clientFileStr = new StringBuilder(sdcardStr).append("Tencent")
                    .append(File.separator).append("CloudSdk")
                    .append(File.separator).append("ClientId")
                    .append(File.separator).append("cloudsdk_client_id_")
                    .append(System.currentTimeMillis()).append("_").append(mClientId).toString();

            File clientIdFile = new File(clientFileStr);
            try {
                clientIdFile.getParentFile().mkdirs();
                clientIdFile.createNewFile();
            } catch (IOException e) {
                WnsClientLog.v(TAG, e.getMessage(), e);
            }
        }
    }
}
