/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.utils;

import android.os.Handler;
import android.os.HandlerThread;

/**
 * 通用的异步线程类
 *
 * @author 
 */
public class CommonAsynThread {
    /**
     * 异步线程
     */
    private static HandlerThread asynThread = null;

    /**
     * 异步线程{@link Handler}
     */
    public static Handler handler = null;

    static {
        /**初始化异步线程*/
        asynThread = new HandlerThread("asynThread");
        asynThread.start();

        handler = new Handler(asynThread.getLooper());
    }
}
