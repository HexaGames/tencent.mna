/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.tencent.cloudsdk.anssetting.AnsSetting;

//import android.util.Log;

/**
 * 测速上报配置管理类
 *
 * @author 
 * @date 2013-04-22
 */
public class SpeedConfig {
    private final static String TAG = SpeedConfig.class.getSimpleName();
    private final static String SHARE_NAME = "expried";
    private final static long DEFAULT_FREQ = 600 * 1000; // 默认10分钟
    private final static int DEFAULT_TIMEOUT = 15000; // 默认连接超时时间
    private static long freq = DEFAULT_FREQ;
    private static int timeout = DEFAULT_TIMEOUT;

    /**
     * 保存测速的时间戳
     */
    public static void saveLastTest(Context ctx, String domain, long time) {
        Editor editor = getPreferences(ctx).edit();
        editor.putLong(Common.getKey(domain), time);
        editor.commit();
        //WnsClientLog.i(Tag, ">>>测速时间已更新>>>");
    }

    public static void saveFreq(long time) {
        freq = time;
    }

    public static long getFreq() {
        return freq;
    }

    // public static void saveTimeOut(int time){
    // timeout = time;
    // }

    public static int getTimeout() {
        //WnsClientLog.i(Tag, ">>ans speedtestTime="+AnsSetting.g().speedTestTimeout);
        return AnsSetting.g().getSpeedTestTimeout() == 0 ? timeout : AnsSetting.g().getSpeedTestTimeout();
    }

    // 需要在提供的接口处把context传递进来
    private static SharedPreferences getPreferences(Context ctx) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(SHARE_NAME,
                Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    /**
     * 获取上次测速的时间戳
     */
    public static long getLastTest(Context ctx, String domain) {
        SharedPreferences sharedPreferences = getPreferences(ctx);
        return sharedPreferences.getLong(Common.getKey(domain), 0);
    }

    /**
     * 判断此（网络类型+域名）下的测速结果是否已过期
     *
     * @param key 网络类型+域名拼接的串
     */
    public static boolean isExpired(Context ctx, String domain) {
        long lastTime = getLastTest(ctx, domain);
        long curTime = System.currentTimeMillis();
        if (lastTime == 0 || lastTime > curTime) {
            return true;
        }
        //WnsClientLog.i(Tag, ">>>freq=" + getFreq());
        if (lastTime + getFreq() < curTime) {
            //WnsClientLog.i(Tag, ">>>测速结果已过期>>>");
            PrintHandler.addLog(TAG, ">>>测速结果已过期<<<");
            return true;
        }
        //WnsClientLog.i(Tag, ">>>测速结果未过期，等待下次触发>>>");
        PrintHandler.addLog(TAG, ">>>测速结果未过期，等待下次触发<<<");
        return false;
    }

    public static void reset(Context ctx, String domain) {
        saveLastTest(ctx, domain, 0);
    }

    public static void resetAll(Context ctx) {
        SharedPreferences sharedPreferences = getPreferences(ctx);
        Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

}
