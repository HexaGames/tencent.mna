/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.utils;

import android.os.Handler;
import android.os.Message;

import com.tencent.record.debug.WnsClientLog;

public class PrintHandler {

    private static Handler mHandler;

    public static final int WHAT_ADD_LOG = 0;
    public static final int WHAT_CLEAR_LOG = 1;

    public static class Log {
        public String tag;
        public String msg;
    }

    public static void setHandler(Handler handler) {
        mHandler = handler;
    }

    public static void addLog(String tag, String msg) {
        if (mHandler != null) {
            Log log = new Log();
            log.tag = tag;
            log.msg = msg;
            Message message = mHandler.obtainMessage(WHAT_ADD_LOG);
            message.obj = log;
            mHandler.sendMessage(message);
        }

        WnsClientLog.i(tag, msg);
    }

    public static void clearLog() {
        if (mHandler != null) {
            Message message = mHandler.obtainMessage(WHAT_CLEAR_LOG);
            mHandler.sendMessage(message);
        }
    }

}
