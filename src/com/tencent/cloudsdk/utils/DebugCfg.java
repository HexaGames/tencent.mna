/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.utils;

/**
 * debug开关设定
 *
 * @author 
 */
public class DebugCfg {

    /**
     * 功能使用测试服务器，发布提交svn前，请改为false
     */
    public static final boolean DEBUG = false;

    /**
     * 上报使用测试服务器，发布提交svn前，改为false
     */
    public static final boolean REPORT_DEBUG = false;

    /**
     * 使用老的命令字（不包含httpflag）
     */
    public static final boolean USE_OLD_CMD = false;

    /**
     * 强制http的host使用oc而不是源站
     */
    public static final boolean FORCE_HTTP_USE_OC = false;
}
