/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.anssetting;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.ansquery.QueryTcpClient;
import com.tencent.cloudsdk.ansquery.QueryTcpClient.QueryListener;
import com.tencent.cloudsdk.ansquery.packet.AnsHeader;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.CommonAsynThread;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.record.debug.WnsClientLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ANS配置类
 *
 * @author 
 *         TODO 拆分一下
 */
public class AnsSetting {
    private static final String TAG = "AnsSetting";

    private static AnsSetting setting = null;

    /**
     * ANS查询超时时间
     */
    int ansQueryTimeout = AnsSettingDefaultValue.QUERY_TIMEOUT;
    /**
     * 测速超时时间
     */
    int speedTestTimeout = AnsSettingDefaultValue.SPEED_TEST_TIMEOUT;
    /**
     * ANS端口
     */
    int port = AnsSettingQueryConfig.PORT;

    /**
     * TSocket连接错误上报的总失败数阀值
     */
    int maxReportConnFailCount = AnsSettingDefaultValue.MAX_REPORT_CONN_FAIL_COUNT;

    /**
     * TSocket连接错误上报时间间隔上限阀值
     */
    long maxReportConnFailInterval = AnsSettingDefaultValue.MAX_REPORT_CONN_FAIL_INTERVAL;

    /**
     * http测速的默认端口
     */
    int httpSpeedTestPort = AnsSettingDefaultValue.DEFAULT_HTTP_SPEED_TEST_PORT;


    int domainRetryInterval = AnsSettingDefaultValue.DEFAUTL_DOMAIN_RETRY_INTERVAL;

    /**
     * 电信IP列表
     */
    List<String> ip_telcom = new ArrayList<String>();
    /**
     * 联通IP列表
     */
    List<String> ip_unicom = new ArrayList<String>();
    /**
     * 移动IP列表
     */
    List<String> ip_chinamobile = new ArrayList<String>();

    int statisticInterval = AnsSettingDefaultValue.STATISTIC_INTERVAL;

    int statisticClearBufInterval = AnsSettingDefaultValue.STATISTIC_CLEAR_BUF_INTERVAL;

    int statisticMaxBuffer = AnsSettingDefaultValue.STATISTIC_MAX_BUFFER;

    /**
     * 配置版本号的 {@link SharedPreferences} 名
     */
    public static final String SPNAMW = "ansetting";
    /**
     * 配置版本号的 {@link SharedPreferences} KEY
     */
    public static final String SP_VER_KEY = "ver";

    /**
     * 当前版本号
     */
    private static int version = AnsSettingDefaultValue.SETTING_VERSION;

    private static final byte[] lock = new byte[1];
    private AnsSettingStorage mAnsSettingPrefStorage;

    public static void init() {
        //WnsClientLog.i(TAG, "@@>>AnsSetting 静态构造函数");
        PrintHandler.addLog(TAG, ">>>AnsSetting 静态构造函数");
        /**将配置版本号保存至本地*/
        SharedPreferences sp = GlobalContext.getContext().getSharedPreferences(SPNAMW, Context.MODE_PRIVATE);
        if (sp.getInt(SP_VER_KEY, 0) == 0) {
            //WnsClientLog.i(TAG, "@@>>AnsSetting 静态构造函数. [将配置版本号写入本地]");
            PrintHandler.addLog(TAG, ">>>AnsSetting 静态构造函数. [将配置版本号写入本地:" + AnsSettingDefaultValue.SETTING_VERSION + "]");
            Editor e = sp.edit();
            e.putInt(SP_VER_KEY, AnsSettingDefaultValue.SETTING_VERSION);
            e.commit();
        } else {
            version = sp.getInt(SP_VER_KEY, 0);
        }
    }

    private AnsSetting(Context context) {
        PrintHandler.addLog(TAG, ">>>new AnsSetting()");
        mAnsSettingPrefStorage = new AnsSettingStorage(context);
    }

    public void initFromPref() {
        mAnsSettingPrefStorage.readAnsSetting(this);
    }

    public synchronized static AnsSetting g() {

        long start = System.currentTimeMillis();
        if (setting == null) {
            setting = new AnsSetting(GlobalContext.getContext());
            PrintHandler.addLog(TAG, ">>>初始化ANS配置实例[" + start + "]");
            setting.initFromPref();

            /**数据库中没有电信IP信息，从默认列表中初始化*/
            if (setting.ip_telcom.size() == 0) {
                for (String ip : AnsSettingDefaultValue.IP_TELCOM_DEFAULT) {
                    PrintHandler.addLog(TAG, ">>存储没有电信IP信息，初始化为默认IP[ip:" + ip + "]");
                    setting.ip_telcom.add(ip);
                }
            }

            /**数据库中没有移动IP信息，从默认列表中初始化*/
            if (setting.ip_chinamobile.size() == 0) {
                for (String ip : AnsSettingDefaultValue.IP_CHINAMOBILE_DEFAULT) {
                    PrintHandler.addLog(TAG, ">>>存储没有移动IP信息，初始化为默认IP[ip:" + ip + "]");
                    setting.ip_chinamobile.add(ip);
                }
            }

            /**数据库中没有联通IP信息，从默认列表中初始化*/
            if (setting.ip_unicom.size() == 0) {
                for (String ip : AnsSettingDefaultValue.IP_UNICOM_DEFAULT) {
                    PrintHandler.addLog(TAG, ">>>存储没有联通IP信息，初始化为默认IP[ip:" + ip + "]");
                    setting.ip_unicom.add(ip);
                }
            }
            //WnsClientLog.i(TAG, "###初始化ANS配置实例结束，耗时：" + (System.currentTimeMillis() - start));
            PrintHandler.addLog(TAG, ">>>初始化ANS配置实例结束,耗时：[" + (System.currentTimeMillis() - start) + "]");
        }

        return setting;
    }

    public int getSpeedTestTimeout() {
        return speedTestTimeout > 0 ? speedTestTimeout : AnsSettingDefaultValue.SPEED_TEST_TIMEOUT;
    }

    public long getMaxReportConnFailInterval() {
        return maxReportConnFailInterval > 0 ? maxReportConnFailInterval : AnsSettingDefaultValue.MAX_REPORT_CONN_FAIL_INTERVAL;
    }

    public int getMaxReportConnFailCount() {
        return maxReportConnFailCount > 0 ? maxReportConnFailCount : AnsSettingDefaultValue.MAX_REPORT_CONN_FAIL_COUNT;
    }

    public int getHttpSpeedTestPort() {
        return httpSpeedTestPort > 0 ? httpSpeedTestPort : AnsSettingDefaultValue.DEFAULT_HTTP_SPEED_TEST_PORT;
    }

    public int getDomainRetryInterval() {
        return domainRetryInterval > 0 ? domainRetryInterval : AnsSettingDefaultValue.DEFAUTL_DOMAIN_RETRY_INTERVAL;
    }

    public int getStatisticInterval() {
        return statisticInterval > 0 ? statisticInterval : AnsSettingDefaultValue.STATISTIC_INTERVAL;
    }

    public int getStatisticClearBufInterval() {
        return statisticClearBufInterval > 0 ? statisticClearBufInterval : AnsSettingDefaultValue.STATISTIC_CLEAR_BUF_INTERVAL;
    }

    public int getStatisticMaxBuffer() {
        return statisticMaxBuffer > 0 ? statisticMaxBuffer : AnsSettingDefaultValue.STATISTIC_MAX_BUFFER;
    }

    public int getAnsQueryTimeout() {
        return ansQueryTimeout > 0 ? ansQueryTimeout : AnsSettingDefaultValue.QUERY_TIMEOUT;
    }

    public static void settingQuery(String ip, int port, int sVer) {
        PrintHandler.addLog(TAG, ">>>拉取ANS配置：[ip：" + ip + "][port:" + port + "][sVer:" + sVer + "]");
        CommonAsynThread.handler.post(new QueryRunnable(ip, port, AnsSetting.g().ansQueryTimeout, sVer));
    }

    public static void asynInit() {
        if (setting == null) {
            CommonAsynThread.handler.post(new Runnable() {

                @Override
                public void run() {
                    g();
                }

            });
        }
    }

    public static int getVersion() {
        return version;
    }

    public static void setVersion(int ver) {
        version = ver;
    }

    public List<String> getIpTelcom() {
        synchronized (lock) {
            return ip_telcom;
        }
    }

    public List<String> getIpUnicom() {
        synchronized (lock) {
            return ip_unicom;
        }
    }

    public List<String> getIpChinamobile() {
        synchronized (lock) {
            return ip_chinamobile;
        }
    }

    public void SetIpTelcom(List<String> ips) {
        synchronized (lock) {
            if (ips != null && ips.size() > 0) {
                ip_telcom.clear();
                ip_telcom.addAll(ips);
            }
        }
    }

    public void SetIpUnicom(List<String> ips) {
        synchronized (lock) {
            if (ips != null && ips.size() > 0) {
                ip_unicom.clear();
                ip_unicom.addAll(ips);
            }
        }
    }

    public void SetIpChinamobile(List<String> ips) {
        synchronized (lock) {
            if (ips != null && ips.size() > 0) {
                ip_chinamobile.clear();
                ip_chinamobile.addAll(ips);
            }
        }
    }

    /**
     * 解析配置JSON
     *
     * @param json 配置JSON
     */
    private void parseValue(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            port = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_ANS_PORT);
            ansQueryTimeout = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_ANS_QUERY_TIMEOUT) * 1000;
            speedTestTimeout = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_ANS_TEST_SPEED_TIMEOUT) * 1000;
            maxReportConnFailCount = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_MAX_FAIL_COUNT);
            maxReportConnFailInterval = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_MAX_FAIL_INTERVAL) * 1000;
            httpSpeedTestPort = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_SPEED_TEST_PORT);
            domainRetryInterval = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_DOMAIN_RETRY_INTERVAL) * 1000;
            version = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_ANS_VERSION);
            statisticInterval = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_INTERVAL) * 1000;
            statisticClearBufInterval = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_CLEAR_BUF_INTERVAL) * 1000;
            statisticMaxBuffer = jsonObject.getInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_MAX_BUFFER);

            JSONArray info = jsonObject.getJSONArray(AnsSettingJsonKey.JSON_KEY_ANS_LIST);

            Map<Short, List<String>> ipMap = new HashMap<Short, List<String>>(3);
            for (int i = 0; i < info.length(); i++) {
                String ipSp = info.getString(i);
                if (ipSp != null && !ipSp.equals("")) {
                    String[] ips = ipSp.split("\\|");
                    if (ips.length == 2) {
                        short sp = Short.valueOf(ips[1]);
                        String ip = ips[0];

                        List<String> ipList = ipMap.get(sp);
                        if (ipList == null) {
                            ipList = new ArrayList<String>(3);
                            ipMap.put(sp, ipList);
                        }
                        ipList.add(ip);
                    }
                }
            }

            setting.SetIpTelcom(ipMap.get(AnsQueryConstants.E_MOBILE_TELCOM));
            setting.SetIpUnicom(ipMap.get(AnsQueryConstants.E_MOBILE_UNICOM));
            setting.SetIpChinamobile(ipMap.get(AnsQueryConstants.E_MOBILE_CHINAMOBILE));

            mAnsSettingPrefStorage.writeAnsSetting(this);
        } catch (JSONException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    ////////////////////////////////////////////////

    private static class QueryRunnable implements Runnable {
        private String ip;
        private int port;
        private int timeout;
        private int sVer;
        private SharedPreferences sp;

        public QueryRunnable(String ip, int port, int timeout, int sVer) {
            this.ip = ip;
            this.port = port;
            this.timeout = timeout;
            this.sVer = sVer;
        }

        @Override
        public void run() {

            if (GlobalContext.getContext() != null && Common.getNetworkType() != AnsQueryConstants.NETWORK_TYPE_UNCONNECTED) {
                sp = GlobalContext.getContext().getSharedPreferences(SPNAMW, Context.MODE_PRIVATE);
                if (sp.getInt(SP_VER_KEY, 0) != sVer) {
                    PrintHandler.addLog(TAG, ">>>版本号有变更，开始拉取：[lastVersion：" + sp.getInt(SP_VER_KEY, 0) + "][currentVersion:" + sVer + "][" + Thread.currentThread().getId() + "]");
                    AnsHeader header = new AnsHeader(AnsHeader.SIZE, AnsQueryConstants.E_CMD_GET_CONF_REQ, (short) 0, AnsSettingQueryConfig.ANS_VERSION);
                    QueryTcpClient client = new QueryTcpClient(ip, port, timeout, header.getData(), new QueryListener() {

                        @Override
                        public void onStart(String ip, int port, long startTime, long tId) {
                        }

                        @Override
                        public void onSend(long time) {
                        }

                        @Override
                        public void onRecv(long time) {
                        }

                        @Override
                        public void onError(String ip, int port, long startTime, String msg, int errorCode, long tId) {

                            PrintHandler.addLog(TAG, ">>>拉取失败：[msg：" + msg + "][耗时:" + (System.currentTimeMillis() - startTime) + "][" + tId + "]");
                        }

                        @Override
                        public void onEnd(String ip, int port, long startTime, byte[] data, long tId) {
                            AnsHeader retHeader = AnsHeader.tryParse(data);

                            if (retHeader.getUchRetCode() == AnsQueryConstants.E_ERR_SUCC) {
                                byte[] b = new byte[data.length - AnsHeader.SIZE];
                                System.arraycopy(data, AnsHeader.SIZE, b, 0, b.length);

                                String json = new String(b);
                                AnsSetting.g().parseValue(json);

                                Editor e = sp.edit();
                                e.putInt(SP_VER_KEY, version);
                                e.commit();

                                PrintHandler.addLog(TAG, ">>>拉取完毕：[data：" + json + "][耗时:" + (System.currentTimeMillis() - startTime) + "][" + tId + "]");
                            }

                        }

                        @Override
                        public void onConnect(long time) {
                        }
                    });

                    /**这里不走start方法新开线程，直接在当前线程RUN*/
                    client.run();
                }
            }
        }

    }
}
