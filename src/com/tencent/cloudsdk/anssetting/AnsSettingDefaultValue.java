/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.anssetting;

import com.tencent.cloudsdk.utils.DebugCfg;

/**
 * Ans配置的默认值
 */
public class AnsSettingDefaultValue {
    /**
     * 默认的TSocket连接错误上报的总失败数阀值
     */
    public static final int MAX_REPORT_CONN_FAIL_COUNT = 2;

    /**
     * 默认的TSocket连接错误上报时间间隔上限阀值
     */
    public static final int MAX_REPORT_CONN_FAIL_INTERVAL = 300000;

    /**
     * HTTP测速的默认端口
     */
    public static final int DEFAULT_HTTP_SPEED_TEST_PORT = 2080;

    /**
     * 域名重新测速的时间间隔
     */
    public static final int DEFAUTL_DOMAIN_RETRY_INTERVAL = 600000;

    /**
     * 测试环境ANS服务器IP，电信IP列表
     */
    private static final String[] TEST_TELCOM_IP = new String[]{"183.60.118.142"};

    /**
     * 测试环境ANS服务器IP，联通IP列表
     */
    private static final String[] TEST_UNICOM_IP = new String[]{"112.90.14.180"};

    /**
     * 测试环境ANS服务器IP，移动IP列表
     */
    private static final String[] TEST_CHINAMOBILE_IP = new String[]{"183.232.17.61"};

    /**
     * 正式环境ANS服务器IP，电信IP列表
     */
    private static final String[] RELEASE_TELCOM_IP = new String[]{"183.60.118.150", "123.151.39.55", "101.226.76.78"};

    /**
     * 正式环境ANS服务器IP，联通IP列表
     */
    private static final String[] RELEASE_UNICOM_IP = new String[]{"112.90.14.176", "111.161.51.36", "112.64.237.239"};

    /**
     * 正式环境ANS服务器IP，移动IP列表
     */
    private static final String[] RELEASE_CHINAMOBILE_IP = new String[]{"183.232.17.65", "111.30.129.156", "120.204.201.205"};

    /**
     * ANS服务器IP，电信IP列表
     */
    public static final String[] IP_TELCOM_DEFAULT = DebugCfg.DEBUG ? TEST_TELCOM_IP : RELEASE_TELCOM_IP;

    /**
     * ANS服务器IP，联通IP列表
     */
    public static final String[] IP_UNICOM_DEFAULT = DebugCfg.DEBUG ? TEST_UNICOM_IP : RELEASE_UNICOM_IP;

    /**
     * ANS服务器IP，移动IP列表
     */
    public static final String[] IP_CHINAMOBILE_DEFAULT = DebugCfg.DEBUG ? TEST_CHINAMOBILE_IP : RELEASE_CHINAMOBILE_IP;

    /**
     * ANS查询超时时间，默认为15秒
     */
    public static final int QUERY_TIMEOUT = 15 * 1000;

    /**
     * 测速超时时间，默认为15秒
     */
    public static final int SPEED_TEST_TIMEOUT = 15 * 1000;

    /**
     * 配置版本号
     */
    public static final int SETTING_VERSION = 1;

    /**
     * 上报统计的时间间隔
     */
    public static final int STATISTIC_INTERVAL = 5 * 60 * 1000;

    /**
     * 上报统计超过多长时间的数据将被忽略
     */
    public static final int STATISTIC_CLEAR_BUF_INTERVAL = 2 * 60 * 60 * 1000;

    /**
     * 上报统计数据包的最大值
     */
    public static final int STATISTIC_MAX_BUFFER = 64 * 1024;
}
