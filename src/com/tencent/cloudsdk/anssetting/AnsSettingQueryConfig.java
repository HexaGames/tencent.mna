/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.anssetting;

import com.tencent.cloudsdk.utils.DebugCfg;

/**
 * Ans查询的配置，包括域名，端口等
 */
public class AnsSettingQueryConfig {
    /**
     * ANS版本号
     */
    public static final short ANS_VERSION = 1;

    /**
     * ANS服务器测试端口
     */
    private static final int TEST_PORT = 8000;

    /**
     * ANS服务器正式端口
     */
    private static final int RELEASE_PORT = 8000;

    /**
     * ANS服务器端口，暂时默认为8000
     */
    public static final int PORT = DebugCfg.DEBUG ? TEST_PORT : RELEASE_PORT;

    /**
     * ANS服务器正式域名
     */
    private static final String RELEASE_DOMAIN = "ans.qcloud.com";

    /**
     * ANS服务器测试域名
     */
    private static final String TEST_DOMAIN = "debug.ans.qcloud.com";

    /**
     * ANS服务器域名
     */
    public static final String OC_DOMAIN = DebugCfg.DEBUG ? TEST_DOMAIN : RELEASE_DOMAIN;
}
