/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.anssetting;

/**
 * Ans查询返回json数据中的key
 */
public class AnsSettingJsonKey {
    /**
     * 配置JSON IP列表KEY
     */
    static final String JSON_KEY_ANS_LIST = "AnsList";

    /**
     * 配置JSON 端口KEY
     */
    static final String JSON_KEY_ANS_PORT = "AnsPort";

    /**
     * 配置JSON 查询超时KEY
     */
    static final String JSON_KEY_ANS_QUERY_TIMEOUT = "AnsQuTo";

    /**
     * 配置JSON 测速超时KEY
     */
    static final String JSON_KEY_ANS_TEST_SPEED_TIMEOUT = "TstSpTo";

    /**
     * 配置JSON 配置版本号KEY
     */
    static final String JSON_KEY_ANS_VERSION = "Ver";

    /**
     * 配置JSON TSocket连接错误上报的总失败数阀值KEY
     */
    static final String JSON_KEY_MAX_FAIL_COUNT = "TsoRepMaxLinkErr";

    /**
     * 配置JSON TSocket连接错误上报时间间隔上限阀值KEY
     */
    static final String JSON_KEY_MAX_FAIL_INTERVAL = "ReportIntval";

    /**
     * 配置JSON HTTP测速端口
     */
    static final String JSON_KEY_SPEED_TEST_PORT = "HttpOcTestPort";

    /**
     * 配置JSON 不在ANS配置的域名，再次查询ans的时间间隔
     */
    static final String JSON_KEY_DOMAIN_RETRY_INTERVAL = "DomainRetryTo";

    /**
     * 上报统计数据的时间间隔
     */
    static final String JSON_KEY_STATISTICS_INTERVAL = "MonitorReportInterval";

    /**
     * 上报统计数据最长多长时间的将被删掉
     */
    static final String JSON_KEY_STATISTICS_CLEAR_BUF_INTERVAL = "ClearBufInterval";

    /**
     * 上报统计数据包的最大长度
     */
    static final String JSON_KEY_STATISTICS_MAX_BUFFER = "MaxBufLength";
}
