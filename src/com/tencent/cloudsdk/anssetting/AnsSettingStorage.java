/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.anssetting;

import android.content.Context;
import android.content.SharedPreferences;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.PrintHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AnsSetting在SharedPreference的存储。
 *
 * @author 
 */
public class AnsSettingStorage {
    private static final String TAG = "AnsSettingStorage";
    public static final String PREF_NAME = "ans_setting_content";

    private SharedPreferences mSharedPref;

    public AnsSettingStorage(Context context) {
        mSharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    /**
     * 从SharedPreference中读取AnsSetting配置
     */
    public void readAnsSetting(AnsSetting setting) {
        PrintHandler.addLog(TAG, ">>>初始化设置到内存");
        SharedPreferences sp = mSharedPref;
        setting.ansQueryTimeout = sp.getInt(AnsSettingJsonKey.JSON_KEY_ANS_QUERY_TIMEOUT, AnsSettingDefaultValue.QUERY_TIMEOUT);
        setting.speedTestTimeout = sp.getInt(AnsSettingJsonKey.JSON_KEY_ANS_TEST_SPEED_TIMEOUT, AnsSettingDefaultValue.SPEED_TEST_TIMEOUT);
        setting.port = sp.getInt(AnsSettingJsonKey.JSON_KEY_ANS_PORT, AnsSettingQueryConfig.PORT);
        setting.maxReportConnFailCount = sp.getInt(AnsSettingJsonKey.JSON_KEY_MAX_FAIL_COUNT, AnsSettingDefaultValue.MAX_REPORT_CONN_FAIL_COUNT);
        setting.maxReportConnFailInterval = sp.getLong(AnsSettingJsonKey.JSON_KEY_MAX_FAIL_INTERVAL, AnsSettingDefaultValue.MAX_REPORT_CONN_FAIL_INTERVAL);
        setting.httpSpeedTestPort = sp.getInt(AnsSettingJsonKey.JSON_KEY_SPEED_TEST_PORT, AnsSettingDefaultValue.DEFAULT_HTTP_SPEED_TEST_PORT);
        setting.domainRetryInterval = sp.getInt(AnsSettingJsonKey.JSON_KEY_DOMAIN_RETRY_INTERVAL, AnsSettingDefaultValue.DEFAUTL_DOMAIN_RETRY_INTERVAL);
        setting.statisticInterval = sp.getInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_INTERVAL, AnsSettingDefaultValue.STATISTIC_INTERVAL);
        setting.statisticClearBufInterval = sp.getInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_CLEAR_BUF_INTERVAL, AnsSettingDefaultValue.STATISTIC_CLEAR_BUF_INTERVAL);
        setting.statisticMaxBuffer = sp.getInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_MAX_BUFFER, AnsSettingDefaultValue.STATISTIC_MAX_BUFFER);

        String ansList = sp.getString(AnsSettingJsonKey.JSON_KEY_ANS_LIST, null);
        if (ansList == null || "".equals(ansList)) {
            setDefaultIps(setting);
        } else {
            boolean ret = parseAnsIps(ansList, setting);
            if (!ret) setDefaultIps(setting);
        }
    }

    /**
     * 将AnsSetting的数据写入SharedPreference
     */
    public void writeAnsSetting(AnsSetting setting) {
        SharedPreferences sp = mSharedPref;
        SharedPreferences.Editor editor = sp.edit();

        editor.putInt(AnsSettingJsonKey.JSON_KEY_ANS_QUERY_TIMEOUT, setting.ansQueryTimeout);
        editor.putInt(AnsSettingJsonKey.JSON_KEY_ANS_TEST_SPEED_TIMEOUT, setting.speedTestTimeout);
        editor.putInt(AnsSettingJsonKey.JSON_KEY_ANS_PORT, setting.port);
        editor.putInt(AnsSettingJsonKey.JSON_KEY_MAX_FAIL_COUNT, setting.maxReportConnFailCount);
        editor.putLong(AnsSettingJsonKey.JSON_KEY_MAX_FAIL_INTERVAL, setting.maxReportConnFailInterval);
        editor.putInt(AnsSettingJsonKey.JSON_KEY_SPEED_TEST_PORT, setting.httpSpeedTestPort);
        editor.putInt(AnsSettingJsonKey.JSON_KEY_DOMAIN_RETRY_INTERVAL, setting.domainRetryInterval);
        editor.putInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_INTERVAL, setting.statisticInterval);
        editor.putInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_CLEAR_BUF_INTERVAL, setting.statisticClearBufInterval);
        editor.putInt(AnsSettingJsonKey.JSON_KEY_STATISTICS_MAX_BUFFER, setting.statisticMaxBuffer);

        String ansIpsString = getAnsIpString(setting);
        editor.putString(AnsSettingJsonKey.JSON_KEY_ANS_LIST, ansIpsString);

        editor.commit();
    }

    /**
     * 清空在SharedPreference中的ANS数据
     */
    public void clear() {
        SharedPreferences sp = mSharedPref;
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * 将ANS的IP列表设置为默认值
     */
    private void setDefaultIps(AnsSetting setting) {
        setting.SetIpTelcom(Arrays.asList(AnsSettingDefaultValue.IP_TELCOM_DEFAULT));
        setting.SetIpChinamobile(Arrays.asList(AnsSettingDefaultValue.IP_CHINAMOBILE_DEFAULT));
        setting.SetIpUnicom(Arrays.asList(AnsSettingDefaultValue.IP_UNICOM_DEFAULT));
    }

    /**
     * 从一列字符串中，解析出ANS的IP和运营商，并写入AnsSetting <br/>
     * 字符串的基本格式为："IP1|SP1,IP2|SP1,IP3|SP2,IP4|SP2..."
     */
    private boolean parseAnsIps(String listString, AnsSetting setting) {
        String[] ipInfos = listString.split(",");
        if (ipInfos == null || ipInfos.length <= 0) {
            return false;
        }

        Map<Short, List<String>> ipMap = new HashMap<Short, List<String>>(3);
        for (String ipInfo : ipInfos) {
            String[] ipSp = ipInfo.split("\\|");
            if (ipSp == null || ipSp.length < 2) {
                return false;
            }

            short sp = Short.parseShort(ipSp[1]);
            String ip = ipSp[0];
            List<String> ipList = ipMap.get(sp);
            if (ipList == null) {
                ipList = new ArrayList<String>(3);
                ipMap.put(sp, ipList);
            }
            ipList.add(ip);
        }

        setting.SetIpTelcom(ipMap.get(AnsQueryConstants.E_MOBILE_TELCOM));
        setting.SetIpUnicom(ipMap.get(AnsQueryConstants.E_MOBILE_UNICOM));
        setting.SetIpChinamobile(ipMap.get(AnsQueryConstants.E_MOBILE_CHINAMOBILE));
        return true;
    }

    /**
     * 写入SharedPreference的ANS列表字符串，基本格式为:"IP1|SP1,IP2|SP1,IP3|SP2,IP4|SP2..."<br/>
     */
    private String getAnsIpString(AnsSetting setting) {
        List<String> chinaMobileList = setting.getIpChinamobile();
        List<String> telcomList = setting.getIpTelcom();
        List<String> unicomList = setting.getIpUnicom();

        String result = null;
        for (String ip : chinaMobileList) {
            if (result == null) {
                result = ip + "|" + AnsQueryConstants.E_MOBILE_CHINAMOBILE;
            } else {
                result = result + "," + ip + "|" + AnsQueryConstants.E_MOBILE_CHINAMOBILE;
            }
        }

        for (String ip : telcomList) {
            if (result == null) {
                result = ip + "|" + AnsQueryConstants.E_MOBILE_TELCOM;
            } else {
                result = result + "," + ip + "|" + AnsQueryConstants.E_MOBILE_TELCOM;
            }
        }

        for (String ip : unicomList) {
            if (result == null) {
                result = ip + "|" + AnsQueryConstants.E_MOBILE_UNICOM;
            } else {
                result = result + "," + ip + "|" + AnsQueryConstants.E_MOBILE_UNICOM;
            }
        }

        return result;
    }
}
