/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery;

import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.PrintHandler;

/**
 * OC查询入口类
 *
 * @author 
 */
public class QueryMain {
    private static final String TAG = QueryMain.class.getName();

    public static OcAddress queryFromOC(String domain, boolean isHttp) {
        return queryFromOC(domain, isHttp, null);
    }

    public static OcAddress queryFromOC(String domain, boolean isHttp, QueryFinishListener listener) {
        //WnsClientLog.v(TAG, ">>>从OC查询域名信息:" + domain);

        /**检查一下是否要异步创建一下配置实例*/
        AnsSetting.asynInit();

        /**检查一下是否要清理过期的数据*/
        OcCacheManager.getInstance(GlobalContext.getContext()).clearExpiredItem();

        PrintHandler.addLog(TAG, ">>>从ANS查询域名信息:" + domain);
        long start = System.currentTimeMillis();
        //WnsClientLog.v(TAG, ">>>start time:" + start);
        PrintHandler.addLog(TAG, ">>>查询开始时间:" + start);
        QueryLogic ql = new QueryLogic(domain);
        ql.setQueryMainListener(listener);

        OcAddress oa = ql.ansQuery(isHttp);
        long end = System.currentTimeMillis();
        //WnsClientLog.v(TAG, ">>>end time:" + end + ", duration:" + (end - start));
        PrintHandler.addLog(TAG, ">>>查询结束时间:" + end + ", 耗时:" + (end - start));
        return oa;
    }

    public interface QueryFinishListener {
        void onFinish();
    }
}
