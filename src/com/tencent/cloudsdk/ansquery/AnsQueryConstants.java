/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery;

/**
 * OC查询的一些常理定义，包括查询命令字、IP类型、SP标识码等
 *
 * @author 
 */
public class AnsQueryConstants {

    ////OC查询的命令字////

    /**
     * 查询请求码
     */
    public final static short E_CMD_GET_OC_LIST_REQ = 1;
    /**
     * 查询响应码
     */
    public final static short E_CMD_GET_OC_LIST_RES = 2;
    /**
     * 拉取配置请求码
     */
    public final static short E_CMD_GET_CONF_REQ = 3;
    /**
     * 拉取配置响应码
     */
    public final static short E_CMD_GET_CONF_RES = 4;
    /**
     * 新的查询请求码
     */
    public final static short E_CMD_GET_OC_LIST_EX_REQ = 5;
    /**
     * 新的查询响应码
     */
    public final static short E_CMD_GET_OC_LIST_EX_RES = 6;
    /**
     * 边界值，暂无具体业务意义
     */
    public final static short E_CMD_BUTT = 10;


    ////网络运营商标识码///

    /**
     * 未知运营商
     */
    public final static short E_MOBILE_UNKNOWN = 0; //未知运营商
    /**
     * 中国电信
     */
    public final static short E_MOBILE_TELCOM = 1; //中国电信
    /**
     * 中国联通
     */
    public final static short E_MOBILE_UNICOM = 2; //中国联通
    /**
     * 中国移动
     */
    public final static short E_MOBILE_CHINAMOBILE = 4; //中国移动    


    ////IP类型////

    /**
     * OC接入点IP
     */
    public final static short E_IP_TYPE_OC = 0;
    /**
     * 源站IPS
     */
    public final static short E_IP_TYPE_RS = 1;
    /**
     * 测试IP
     */
    public final static short E_IP_TYPE_TEST = 2;


    ////网络类型////

    /**
     * 无网络
     */
    public final static short NETWORK_TYPE_UNCONNECTED = -1;
    /**
     * 未知网络
     */
    public final static short NETWORK_TYPE_UNKNOWN = 0;
    /**
     * WIFI网络
     */
    public final static short NETWORK_TYPE_WIFI = 1;
    /**
     * 2G网络
     */
    public final static short NETWORK_TYPE_2G = 2;
    /**
     * 3G网络
     */
    public final static short NETWORK_TYPE_3G = 3;
    /**
     * 4G网络
     */
    public final static short NETWORK_TYPE_4G = 4;


    ////响应包返回码////

    /**
     * 响应包返回码，表示成功
     */
    public final static short E_ERR_SUCC = 0;
    /**
     * 响应包返回码，表示其它未知错误
     */
    public final static short E_ERR_ERROR = 1;
    /**
     * 响应包返回码，表示域名不存在错误
     */
    public final static short E_ERR_DOMAIN_NOT_EXIST = 9;
}
