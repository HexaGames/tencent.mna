/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery;

import android.os.Bundle;

import com.tencent.cloudsdk.ansquery.QueryTcpClient.QueryListener;
import com.tencent.cloudsdk.ansquery.packet.AnsHeader;
import com.tencent.cloudsdk.ansquery.packet.request.RequestPacket;
import com.tencent.cloudsdk.ansquery.packet.request.RequestPacketFactory;
import com.tencent.cloudsdk.ansquery.packet.response.AnsResponsePacket;
import com.tencent.cloudsdk.ansquery.packet.response.ParseClient;
import com.tencent.cloudsdk.ansquery.packet.response.ParseClientFactory;
import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.anssetting.AnsSettingQueryConfig;
import com.tencent.cloudsdk.cache.MemoryCache;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.AddressEntity;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.dns.DnsMain;
import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.ClientIdManager;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.IpUtils;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.record.debug.WnsClientLog;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * ANS查询逻辑类
 *
 * @author 
 */
public class QueryLogic {
    private static final String TAG = QueryLogic.class.getName();

    /**
     * 查询并发线程数，这里默认为3个
     */
    private int queryTheadCount = 3;

    /**
     * IP列表
     */
    private OcAddress oaInfo = null;

    /**
     * 查询是否完成，按最先响应的线程为准
     */
    private boolean isCompleted = false;

    /**
     * 查询域名
     */
    private String domain;

    /**
     * 当前网络类型
     */
    private int networkType;

    /**
     * 运营商
     */
    private short sp;

    /**
     * 资源锁
     */
    private final static byte synLock[] = new byte[1];

    /**
     * 上报数据
     */
    private List<Bundle> reportBundles = new ArrayList<Bundle>();

    /**
     * dns解析耗时
     */
    private long dnsQueryTime;
    /**
     * dns解析结果
     */
    private int dnsQueryStatus;
    /**
     * 建立连接耗时
     */
    private long connectTime;
    /**
     * 发送耗时
     */
    private long sendTime;
    /**
     * 接收耗时
     */
    private long recvTime;

    private final static int DNS_QUERY_OK = 0;
    private final static int DNS_QUERY_ERR = 1;

    private QueryMain.QueryFinishListener mQueryFinishListener;

    private static final Map<String, DomainBlockItem> sDomainBlockItems = Collections.synchronizedMap(new MemoryCache<String, DomainBlockItem>(512));


    /**
     * 创建一个{@link QueryLogic}对象
     *
     * @param domain
     */
    public QueryLogic(String domain) {
        this.domain = domain;
    }

    ////////////////////////////////////////////////

    public void setQueryMainListener(QueryMain.QueryFinishListener listener) {
        mQueryFinishListener = listener;
    }

    /**
     * 查询域名IP。查询时先从本地缓存查找，本地无缓存或者缓存已经过期时再从OC查询服务器查找
     *
     * @return {@link OcAddress}
     */
    public OcAddress ansQuery(boolean isHttp) {
        // 对于上次查询ANS没有结果且没有过期的项，不再重新查询
        DomainBlockItem blockDomain = sDomainBlockItems.get(domain);
        int domainRetryInterval = AnsSetting.g().getDomainRetryInterval();
        long now = System.currentTimeMillis();
        if (blockDomain != null && now - blockDomain.addTime > domainRetryInterval) {
            sDomainBlockItems.remove(domain);
        } else if (blockDomain != null && now - blockDomain.addTime <= domainRetryInterval) {
            return null;
        }

        networkType = Common.getNetworkType();

        /**无网络时直接返回NULL*/
        if (networkType == AnsQueryConstants.NETWORK_TYPE_UNCONNECTED) {
            //WnsClientLog.w(TAG, "###网络未连接，直接返回NULL，查询结束");
            PrintHandler.addLog(TAG, ">>>网络未连接，直接返回NULL，查询结束");
            return null;
        }

        /**WIFI网络，直接将SP置为E_MOBILE_UNKNOWN*/
        if (networkType == AnsQueryConstants.NETWORK_TYPE_WIFI) {

            sp = AnsQueryConstants.E_MOBILE_UNKNOWN;
        } else {

            sp = Common.getSP();
        }

        PrintHandler.addLog(TAG, ">>>当前网络类型:" + networkType + ", 当前运营商:" + sp);

        oaInfo = OcCacheManager.getInstance(GlobalContext
                .getContext()).get(domain, sp, isHttp);

        if (oaInfo == null || oaInfo.addressList == null || oaInfo.addressList.size() == 0) {

            PrintHandler.addLog(TAG, ">>>缓存数据不存在或已经过期，开始网络查询");

            /**wifi网络或者获取sp失败，走114DNS解析OC查询服务器IP*/
            if (sp == AnsQueryConstants.E_MOBILE_UNKNOWN) {
                PrintHandler.addLog(TAG, ">>>WIFI网络或者获取SP失败，开始114DNS解析[" + AnsSettingQueryConfig.OC_DOMAIN + "]");

                long startTime = System.currentTimeMillis();
                InetAddress[] ocSvrAddress = DnsMain.getBetterHostByName(AnsSettingQueryConfig.OC_DOMAIN);
                if (ocSvrAddress != null && ocSvrAddress.length > 0) {
                    PrintHandler.addLog(TAG, ">>>114DNS解析成功，耗时：" + (System.currentTimeMillis() - startTime));
                    dnsQueryTime = System.currentTimeMillis() - startTime;
                    dnsQueryStatus = DNS_QUERY_OK;
                    /*if(ocSvrAddress.length < queryTheadCount){
                        queryTheadCount = ocSvrAddress.length;
                    }*/
                    ArrayList<String> l = new ArrayList<String>();
                    for (int i = 0; i < ocSvrAddress.length; i++) {
                        l.add(ocSvrAddress[i].getHostAddress());
                        PrintHandler.addLog(TAG, ">>>114DNS返回IP:" + l.get(i));
                    }
                    doQuery(l, domain, AnsQueryConstants.E_MOBILE_UNKNOWN, isHttp);
                    //wifi网络，先拿中国电信IP做测试
                    //doQuery(OcQueryConstants.TELCOM_IP, domain, OcQueryConstants.E_MOBILE_UNKNOWN);
                } else {
                    PrintHandler.addLog(TAG, ">>>114DNS解析失败，查询结束。耗时：" + (System.currentTimeMillis() - startTime));
                    dnsQueryStatus = DNS_QUERY_ERR;

                    //DNS解析失败，ANS查询无法继续，这里做一次DNS解析上报错误后直接返回NULL。
                    //TSocket发现OC IP为NULL后，跟源站发起直连请求
                    addBundle(ReportConstanst.REPORT_DNS_ERR, "", System.currentTimeMillis());
                    oaInfo = null;
                }
            } else {
                /**非wifi网络，且能获取到sp信息时*/
                switch (sp) {
                    case AnsQueryConstants.E_MOBILE_CHINAMOBILE:
                        doQuery(AnsSetting.g().getIpChinamobile(), domain, AnsQueryConstants.E_MOBILE_CHINAMOBILE, isHttp);
                        break;
                    case AnsQueryConstants.E_MOBILE_TELCOM:
                        doQuery(AnsSetting.g().getIpTelcom(), domain, AnsQueryConstants.E_MOBILE_TELCOM, isHttp);
                        break;
                    case AnsQueryConstants.E_MOBILE_UNICOM:
                        doQuery(AnsSetting.g().getIpUnicom(), domain, AnsQueryConstants.E_MOBILE_UNICOM, isHttp);
                        break;
                }
            }

            /**查询上报*/
            if (reportBundles.size() > 0) {
                WnsClientLog.v(TAG, "开始上报：" + domain + ",数量:" + reportBundles.size());
                SDKGlobal.getReportTool().asynReport(reportBundles);
            }
        }

        return oaInfo;
    }


    ///////////////////////////////////////////// PRIVATE METHOD ///////////////////////

    /**
     * 执行网络查询，根据IP数组进行并发查询
     *
     * @param ips    IP数组
     * @param domain 域名
     * @param sp     运营商
     */
    private void doQuery(List<String> ips, String domain, short sp, final boolean isHttp) {

        if (ips.size() < queryTheadCount) {
            queryTheadCount = ips.size();
        }

        RequestPacket reqPacket = RequestPacketFactory.createRequestPacket(domain, sp, isHttp);
        int timeout = AnsSetting.g().getAnsQueryTimeout();
        for (int i = 0; i < queryTheadCount; i++) {
            QueryTcpClient client = new QueryTcpClient(ips.get(i), AnsSettingQueryConfig.PORT, timeout, reqPacket.getReqData(), new QueryListener() {

                public void onStart(String ip, int port, long startTime, long tId) {
                    //WnsClientLog.v(TAG, "###Thread start:[ip:" + ip + "][" + "port:" + port + "][" + tId + "]");
                    PrintHandler.addLog(TAG, ">>>开始网络查询:[SERVER IP:" + ip + "][port:" + port + "][Thread id:" + tId + "][startTime:" + startTime + "]");
                }

                public void onError(String ip, int port, long startTime, String msg, int errorCode, long tId) {
                    addBundle(errorCode, ip, startTime);
                    descThreadCount();
                    //WnsClientLog.v(TAG, "###Thread onError:" + tId + "." + msg + ",errorCode:" + errorCode);
                    PrintHandler.addLog(TAG, ">>>网络查询错误:[SERVER IP:" + ip + "][port:" + port + "][Thread id:" + tId + "][ERR:" + msg + "]");
                }

                @Override
                public void onEnd(String ip, int port, long startTime, byte[] data, long tId) {
                    //WnsClientLog.v(TAG, "###Thread onEnd:" + tId + "." + new String(data) + "[CurrentThreadCount:" + getCurrentThreadCount() + "]");
                    PrintHandler.addLog(TAG, ">>>网络查询完成:[SERVER IP:" + ip + "][port:" + port + "][Thread id:" + tId + "][耗时:" + (System.currentTimeMillis() - startTime) + "]");
                    threadDone(data, ip, port, startTime, isHttp);
                }

                @Override
                public void onConnect(long time) {
                    connectTime = time;
                }

                @Override
                public void onSend(long time) {
                    sendTime = time;
                }

                @Override
                public void onRecv(long time) {
                    recvTime = time;
                }

            });

            client.start();
        }

        while (!isCompleted()) {
            /**等待查询
             *
             * 1. 当有一个线程最先从服务器上查到数据时，等待结束
             * 2. 当全部线程都查询失败时,等待结束
             * */

            //sleep一下，免得这里无聊空转
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {

            }
        }

        if (oaInfo == null || !domain.equals(oaInfo.domain)) {
            DomainBlockItem blockItem = new DomainBlockItem();
            blockItem.addTime = System.currentTimeMillis();
            sDomainBlockItems.put(domain, blockItem);
        }
    }

    /**
     * 当有线程完成或出错时，递减一个线程数，当线程数递减到0，则认为i线程已经跑完
     */
    private void descThreadCount() {
        synchronized (synLock) {
            queryTheadCount--;
            if (queryTheadCount <= 0) {
                isCompleted = true;
            }
        }
    }

    /**
     * 线程拉取到OC IP数据，解析响应包，如果响应包没有错误，则设置完成标识属性为true
     *
     * @param data
     */
    private void threadDone(byte[] data, String ip, int port, long startTime, boolean isHttp) {
        synchronized (synLock) {
            if (!isCompleted) {
                ParseClient<AnsResponsePacket> client = ParseClientFactory.createParseClient(data);
                AnsHeader header = client.parseHeader();
                AnsResponsePacket resPacket = null;
                if (header.getUchRetCode() == AnsQueryConstants.E_ERR_SUCC) {
                    try {
                        resPacket = client.parse();
                    } catch (Exception e) {
                        WnsClientLog.e(TAG, "parse data exception on threadDone.", e);
                    }
                }

                List<AddressEntity> list = null;
                if (resPacket != null) {
                    list = resPacket.getAddressInfos();
                }

                if (header.getUchRetCode() == AnsQueryConstants.E_ERR_SUCC && (list != null && list.size() > 0)) {
                    oaInfo = new OcAddress();
                    oaInfo.domain = domain;
                    oaInfo.sp = "" + sp;
                    oaInfo.expireTime = resPacket.getExpireTimeSpan() * 1000 + System.currentTimeMillis();
                    oaInfo.retestTimeSpan = resPacket.getRetestTimeSpan();
                    oaInfo.addressList = list;

                    /**将结果添加至本地缓存*/
                    OcCacheManager.getInstance(GlobalContext.getContext()).asynAdd(oaInfo, isHttp);
                    if (AnsSetting.getVersion() != resPacket.getSettingVersion()) {
                        AnsSetting.settingQuery(ip, port, resPacket.getSettingVersion());
                    }
                    addBundle(resPacket.getHeader().getUchRetCode(), ip, startTime);
                    isCompleted = true;
                    //WnsClientLog.v(TAG, "@@@threadDone:" + Thread.currentThread().getId() + ".[isCompleted:" + isCompleted + "][CurrentThreadCount:" + getCurrentThreadCount() + "]");
                } else {
                    descThreadCount();
                }

                if (mQueryFinishListener != null) {
                    mQueryFinishListener.onFinish();
                }

                //WnsClientLog.v(TAG, "###threadDone:" + Thread.currentThread().getId() + ".[data:" + new String(data) + "][CurrentThreadCount:" + getCurrentThreadCount() + "]");
            }
        }
    }

    /**
     * 获取当前线程数
     * @return 当前线程数
     */
    /*@Deprecated
    private int getCurrentThreadCount(){
        synchronized (synLock) {
            return queryTheadCount;
        }
    }*/

    /**
     * 查询是否完成
     */
    private boolean isCompleted() {
        synchronized (synLock) {
            return isCompleted;
        }
    }

    private void addBundle(int retCode, String ip, long startTime) {
        synchronized (synLock) {
            //WnsClientLog.v(TAG, "添加数据到上报列表：" + ip + ",retCode:" + retCode);
            Bundle bundle = new Bundle();
            bundle.putInt(ReportConstanst.OC_QUERY_TYPEID, ReportConstanst.TYPE_ID_OC_QUERY);
            bundle.putInt(ReportConstanst.OC_QUERY_RET, retCode);
            bundle.putLong(ReportConstanst.OC_QUERY_TC, (System.currentTimeMillis() - startTime));
            bundle.putString(ReportConstanst.OC_QUERY_DM, domain);
            bundle.putLong(ReportConstanst.OC_QUERY_SERVER_IP, IpUtils.parseIpAddress(ip));
            bundle.putInt(ReportConstanst.OC_QUERY_SP, sp);
            bundle.putInt(ReportConstanst.OC_QUERY_NETWORK_TYPE, networkType);
            bundle.putInt(ReportConstanst.OC_QUERY_OS, ReportConstanst.OS);//0-IOS,1-Android,2-other
            bundle.putString(ReportConstanst.OC_QUERY_OS_VERSION, android.os.Build.VERSION.RELEASE);
            bundle.putString(ReportConstanst.OC_QUERY_SDK_VERSION, ReportConstanst.SDK_VERSION);
            bundle.putInt(ReportConstanst.OC_QUERY_ANS_VERSION, AnsSettingQueryConfig.ANS_VERSION);
            bundle.putLong(ReportConstanst.DNS_QUERY_TIME, dnsQueryTime);
            bundle.putInt(ReportConstanst.DNS_QUERY_STATUS, dnsQueryStatus);
            bundle.putLong(ReportConstanst.OC_QUERY_CONNECT_TIME, connectTime);
            bundle.putLong(ReportConstanst.OC_QUERY_SEND_TIME, sendTime);
            bundle.putLong(ReportConstanst.OC_QUERY_RECV_TIME, recvTime);
            bundle.putInt(ReportConstanst.OC_QUERY_CLIENT_SEQ, ClientIdManager.getInstance().getClientId());
            reportBundles.add(bundle);
        }
    }

    public static class DomainBlockItem {
        public long addTime;
    }
}
