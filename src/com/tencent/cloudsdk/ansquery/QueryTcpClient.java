/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.ansquery;

import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.utils.FormatTransfer;
import com.tencent.record.debug.WnsClientLog;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * OC查询tcp client类
 *
 * @author 
 */
public class QueryTcpClient extends Thread {
    private static final String TAG = "QueryTcpClient";

    /**
     * oc查询socket
     */
    private Socket socket = null;

    /**
     * 查询IP
     */
    private String ip;

    /**
     * 端口
     */
    private int port;

    /**
     * 请求包数据流
     */
    private byte[] req;

    /**
     * 状态回调
     */
    private QueryListener callback;

    /**
     * 超时时间
     */
    private int timeout;

    /**
     * 读取响应包数据流的最大缓冲值。响应包5+1+N*8 N代表返回节点数，目前最大是3，
     * 因此，相应的最大长度，应该是29字节。为了兼容可能增加的额外IP数量，这里将最大值定为100
     */
    private static final int maxBufferSize = 1000;

    // /////////////////////////////////

    /**
     * 创建一个{@link QueryTcpClient}对象
     *
     * @param ip       OC查询服务器IP
     * @param port     OC查询端口
     * @param req      OC查询请求包数据流
     * @param callback 监听回调
     */
    public QueryTcpClient(String ip, int port, int timeout, byte[] req, QueryListener callback) {
        this.ip = ip;
        this.port = port;
        this.timeout = timeout;
        this.req = req;
        this.callback = callback;
    }

    @Override
    public void run() {
        long tId = Thread.currentThread().getId();
        long startTime = System.currentTimeMillis();
        long tempTime;
        try {
            if (callback != null) {
                callback.onStart(ip, port, startTime, tId);
            }
            socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), timeout);
            socket.setSoTimeout(timeout * 2);

            if (callback != null) {
                callback.onConnect(System.currentTimeMillis() - startTime);
            }
            tempTime = System.currentTimeMillis();

            /** 发送请求包 */
            OutputStream out = socket.getOutputStream();
            DataOutputStream output = new DataOutputStream(out);
            output.write(req);
            output.flush();
            if (callback != null) {
                callback.onSend(System.currentTimeMillis() - tempTime);
            }
            tempTime = System.currentTimeMillis();

            /** 读取响应包 */
            InputStream inputStream = socket.getInputStream();
            ByteArrayOutputStream packetStream = readPacket(inputStream);

            if (callback != null) {
                callback.onRecv(System.currentTimeMillis() - tempTime);
            }
            /*socket.close();
            socket = null;*/

            if (packetStream.size() <= 0) {
                if (callback != null) {
                    callback.onError(ip, port, startTime, "Not data response.",
                            ReportConstanst.REPORT_RESPONSE_ERR, tId);
                }
            } else {
                if (callback != null) {

                    callback.onEnd(ip, port, startTime, packetStream.toByteArray(), tId);
                }
            }

        } catch (UnknownHostException e) {

            if (callback != null) {
                callback.onError(ip, port, startTime, e.getMessage(),
                        ReportConstanst.REPORT_UNKNOWHOST, tId);
            }
            WnsClientLog.e(TAG, e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            if (callback != null) {
                callback.onError(ip, port, startTime, e.getMessage(),
                        ReportConstanst.REPORT_SOCKET_TIMEOUT, tId);
            }
            WnsClientLog.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            if (callback != null) {
                callback.onError(ip, port, startTime, e.getMessage(),
                        ReportConstanst.REPORT_DEFAULT_IOEXCEPTION, tId);
            }
            WnsClientLog.e(TAG, e.getMessage(), e);
        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {
                    WnsClientLog.e(TAG, e.getMessage(), e);
                }
            }
            socket = null;
        }

    }

    /**
     * 读取响应包
     *
     * @param in 输入流
     * @return {@link ByteArrayOutputStream}
     * @throws IOException
     */
    private ByteArrayOutputStream readPacket(InputStream in) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        byte[] buffer = new byte[maxBufferSize];
        int len = in.read(buffer);

        if (len > 0) {
            int packetLen = FormatTransfer.bytesToU16(new byte[]{buffer[0], buffer[1]});
            output.write(buffer, 0, len);

            int readedLen = len;
            while (readedLen < packetLen) {
                len = in.read(buffer);
                if (len > 0) {
                    output.write(buffer, readedLen, len);
                    readedLen += len;
                } else {
                    break;
                }
            }
        }

        if (in != null) {
            in.close();
            in = null;
        }
        buffer = null;

        return output;
    }

    // /////////////////////////////////////////

    /**
     * 查询监听接口
     *
     * @author 
     */
    public static interface QueryListener {

        /**
         * 查询开始的事件回调，该方法会在线程启动的时候触发
         *
         * @param ip   查询IP
         * @param port 端口
         * @param tId  查询线程ID
         */
        public void onStart(String ip, int port, long startTime, long tId);

        /**
         * 查询错误的事件回调，该方法会在查询出现异常时触发
         *
         * @param ip        查询IP
         * @param port      端口
         * @param msg       错误信息
         * @param errorCode 错误码
         * @param tId       查询线程ID
         */
        public void onError(String ip, int port, long startTime, String msg, int errorCode, long tId);

        /**
         * 查询结束的事件回调，该方法会在查询结束的时候触发
         *
         * @param ip   查询IP
         * @param port 端口
         * @param data 查询响应包数据流
         * @param tId  查询线程ID
         */
        public void onEnd(String ip, int port, long startTime, byte[] data, long tId);

        /**
         * 建立连接耗时回调
         */
        public void onConnect(long time);

        /**
         * 发送耗时回调
         */
        public void onSend(long time);

        /**
         * 接收耗时回调
         */
        public void onRecv(long time);

    }

}
