/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery.packet.request;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.ansquery.packet.AnsHeader;

/**
 * OC节点查询请求包
 *
 * @author 
 */
public class AnsRequestPacket implements RequestPacket {

    /**
     * 运营商标识码
     */
    private short uchISP;

    /**
     * 域名
     */
    private String domain;

    /**
     * 报文头
     */
    private AnsHeader header;

    /**
     * 数据流
     */
    private byte[] data;

    ////////////////////////////////////////////////////

    /**
     * 创建一个{@link AnsRequestPacket}
     *
     * @param uchISP 运营商标识码
     * @param domain 域名
     */
    public AnsRequestPacket(short uchISP, String domain) {
        this(uchISP, domain, null);
        this.header = new AnsHeader((short) getSize(), AnsQueryConstants.E_CMD_GET_OC_LIST_REQ);
    }

    public AnsRequestPacket(short uchISP, String domain, AnsHeader header) {
        this.uchISP = uchISP;
        this.domain = domain;

        data = new byte[getSize()];
        this.header = header;
    }

    ///////////////////////////////////////////////////

    /**
     * 获取请求包数据流
     *
     * @return 字节数组
     */
    @Override
    public byte[] getReqData() {

        /**装载运营商标识码*/
        data[AnsHeader.SIZE] = (byte) uchISP;

        /**装载域名的长度*/
        data[AnsHeader.SIZE + 1] = (byte) domain.length();

        /**装载头数据*/
        System.arraycopy(header.getData(), 0, data, 0, AnsHeader.SIZE);

        /**装载域名数据*/
        System.arraycopy(domain.getBytes(), 0, data, AnsHeader.SIZE + 2, domain.length());

        return data;
    }

    @Override
    public int getSize() {
        return AnsHeader.SIZE + 2 + domain.length();
    }

    /**
     * 获取营运商标识码
     *
     * @return 营运商标识码，用{@link short}表示
     */
    public short getUchISP() {
        return uchISP;
    }

    /**
     * 获取查询域名
     *
     * @return 域名
     */
    public String getDomain() {
        return domain;
    }

    /**
     * 获取包头信息
     *
     * @return {@link AnsHeader}
     */
    public AnsHeader getHeader() {
        return header;
    }
}
