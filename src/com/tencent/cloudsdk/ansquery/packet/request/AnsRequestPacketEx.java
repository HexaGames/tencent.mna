/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery.packet.request;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.ansquery.packet.AnsHeader;
import com.tencent.cloudsdk.utils.ClientIdManager;
import com.tencent.cloudsdk.utils.FormatTransfer;

/**
 * OC节点查询请求包，新的协议
 * 和原包体相比，只是在结尾加入了三个字段,且只需填httpflag
 *
 * @author 
 */
public class AnsRequestPacketEx implements RequestPacket {
    private AnsRequestPacket mAnsRequestPacket;
    private boolean mIsHttp;
    private byte[] data;
    private String domain;

    public AnsRequestPacketEx(short uchISP, String domain, boolean isHttp) {
        this.domain = domain;
        AnsHeader header = new AnsHeader((short) getSize(), AnsQueryConstants.E_CMD_GET_OC_LIST_EX_REQ);
        mAnsRequestPacket = new AnsRequestPacket(uchISP, domain, header);
        mIsHttp = isHttp;
        data = new byte[getSize()];
    }

    @Override
    public byte[] getReqData() {
        byte[] originData = mAnsRequestPacket.getReqData();
        System.arraycopy(originData, 0, data, 0, originData.length);
        data[originData.length + 4] = (byte) (mIsHttp ? 1 : 0);
        byte[] clientIdArray = FormatTransfer.toHH(ClientIdManager.getInstance().getClientId());
        System.arraycopy(clientIdArray, 0, data, originData.length + 5, 4);
        return data;
    }

    @Override
    public int getSize() {
        // 新的命令字比原来的命令字多了9个字节
        return AnsHeader.SIZE + 2 + domain.length() + 9;
    }
}
