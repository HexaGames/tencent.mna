/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery.packet.response;

import com.tencent.cloudsdk.ansquery.packet.AnsHeader;
import com.tencent.cloudsdk.utils.FormatTransfer;

/**
 * @author 
 */
public class AnsHeaderParser implements Parser {
    AnsHeader mAnsHeader;

    public AnsHeaderParser() {
        mAnsHeader = new AnsHeader((short) 0, (short) 0, (short) -1, (short) 0);
    }

    @Override
    public void parse(ParseContext context) {
        byte[] data = context.getData();
        if (data == null || data.length < AnsHeader.SIZE) {
            return;
        }

        int pos = context.getPos();

        byte[] lenBytes = new byte[2];
        lenBytes[0] = data[pos++];
        lenBytes[1] = data[pos++];
        mAnsHeader = new AnsHeader(FormatTransfer.hBytesToShort(lenBytes) //包长
                , FormatTransfer.byteToShort(data[pos++]) //命令字
                , FormatTransfer.byteToShort(data[pos++]) //返回码
                , FormatTransfer.byteToShort(data[pos++])); //版本号

        context.setPos(pos);
    }

    public AnsHeader getAnsHeader() {
        return mAnsHeader;
    }
}
