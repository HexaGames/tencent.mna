/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery.packet.response;

import com.tencent.cloudsdk.ansquery.packet.AnsHeader;
import com.tencent.cloudsdk.data.AddressEntity;

import java.util.List;

/**
 * OC查询响应包的解析结果
 *
 * @author 
 */
public class AnsResponsePacket {
    private AnsHeader header;
    private int expireTimeSpan;
    private int retestTimeSpan;
    private int settingVersion;
    private List<AddressEntity> addressInfos;

    public List<AddressEntity> getAddressInfos() {
        return addressInfos;
    }

    public void setHeader(AnsHeader header) {
        this.header = header;
    }

    public void setExpireTimeSpan(int expireTimeSpan) {
        this.expireTimeSpan = expireTimeSpan;
    }

    public void setRetestTimeSpan(int retestTimeSpan) {
        this.retestTimeSpan = retestTimeSpan;
    }

    public void setSettingVersion(int settingVersion) {
        this.settingVersion = settingVersion;
    }

    public void setAddressInfos(List<AddressEntity> addressInfos) {
        this.addressInfos = addressInfos;
    }

    public AnsHeader getHeader() {
        return header;
    }

    /**
     * 获取过期时长，单位为秒
     *
     * @return int, 过期时长
     */
    public int getExpireTimeSpan() {
        return expireTimeSpan;
    }

    /**
     * 获取重新测速时间
     *
     * @return int
     */
    public int getRetestTimeSpan() {
        return retestTimeSpan;
    }

    /**
     * 获取配置版本号
     *
     * @return int
     */
    public int getSettingVersion() {
        return settingVersion;
    }
}
