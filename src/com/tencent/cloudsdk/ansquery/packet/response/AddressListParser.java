/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery.packet.response;

import com.tencent.cloudsdk.data.AddressEntity;
import com.tencent.cloudsdk.utils.FormatTransfer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author 
 */
public class AddressListParser implements Parser {
    /**
     * 响应包信息列表
     */
    private List<AddressEntity> address;

    /**
     * IP数据段的长度 + 端口长度
     */
    public static final short segmentLen = 10;

    /**
     * 查询到的IP个数
     */
    private short count;

    /**
     * IP优先级排序器
     */
    private static PriorityComparator pComparator = new PriorityComparator();

    @Override
    public void parse(ParseContext context) {
        byte[] packet = context.getData();
        int pos = context.getPos();

        count = FormatTransfer.byteToShort(packet[pos++]);
        address = new ArrayList<AddressEntity>();

        /**解析响应包IP数据 */
        for (short i = 0; i < count; i++) {
            if ((pos + segmentLen) <= packet.length) {
                AddressEntity ae = new AddressEntity(readBytes(packet, pos, segmentLen));
                address.add(ae);
                pos += segmentLen;
            }
        }
        context.setPos(pos);

        /**根据优先级排序*/
        Collections.sort(address, pComparator);
    }

    public List<AddressEntity> getAddress() {
        return address;
    }

    private byte[] readBytes(byte[] packet, int startPos, int len) {
        byte[] buffer = new byte[segmentLen];
        System.arraycopy(packet, startPos, buffer, 0, len);
        return buffer;
    }

    /**
     * IP列表优先级排序类
     *
     * @author 
     */
    private static class PriorityComparator implements Comparator<AddressEntity> {
        /**
         * 根据优先级排序
         */
        public int compare(AddressEntity a1, AddressEntity a2) {

            if (a1.getPriority() > a2.getPriority()) {
                return 1;
            }

            return -1;
        }
    }
}
