/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery.packet.response;

import com.tencent.cloudsdk.utils.FormatTransfer;
import com.tencent.cloudsdk.utils.PrintHandler;

/**
 * @author 
 */
public class SettingParser implements Parser {
    private static final String TAG = "SettingParser";
    /**
     * 超时时间，单位为秒
     */
    private int expireTimeSpan;

    /**
     * 重新测速时间间隔，单位为秒
     */
    private int retestTimeSpan;

    /**
     * 配置版本
     */

    private int settingVersion;

    @Override
    public void parse(ParseContext context) {
        int pos = context.getPos();
        byte[] packet = context.getData();

        /**解析配置版本号*/
        settingVersion = FormatTransfer.hBytesToShort(new byte[]{packet[pos++], packet[pos++]});
        PrintHandler.addLog(TAG, ">>>解析到配置版本号:" + settingVersion + ",tid:" + Thread.currentThread().getId());

        /**解析过期时间，占4个字节*/
        expireTimeSpan = FormatTransfer.hBytesToInt(new byte[]{packet[pos++], packet[pos++], packet[pos++], packet[pos++]});
        //WnsClientLog.w(TAG, "###expireTimeSpan:"+expireTimeSpan);
        PrintHandler.addLog(TAG, ">>>解析到过期时长:" + expireTimeSpan + ",tid:" + Thread.currentThread().getId());

        /**解析测速时间，占2个字节*/
        retestTimeSpan = FormatTransfer.bytesToU16(new byte[]{packet[pos++], packet[pos++]});
        //WnsClientLog.w(TAG, "###retestTimeSpan:"+retestTimeSpan);
        PrintHandler.addLog(TAG, ">>>解析到重新测速时长:" + retestTimeSpan + ",tid:" + Thread.currentThread().getId());

        context.setPos(pos);
    }

    public int getSettingVersion() {
        return settingVersion;
    }

    public int getRetestTimeSpan() {
        return retestTimeSpan;
    }

    public int getExpireTimeSpan() {
        return expireTimeSpan;
    }
}
