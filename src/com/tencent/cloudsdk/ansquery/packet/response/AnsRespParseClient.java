/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery.packet.response;

import com.tencent.cloudsdk.ansquery.packet.AnsHeader;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 
 */
public class AnsRespParseClient implements ParseClient<AnsResponsePacket> {
    private ParseContext mParseContext;
    private AnsHeaderParser mAnsHeaderParser;
    private SettingParser mSettingParser;
    private AddressListParser mAddressListParser;

    protected List<Parser> mParsers = new ArrayList<Parser>();

    public AnsRespParseClient(byte[] data) {
        mParseContext = new ParseContext();
        mParseContext.setPos(0);
        mParseContext.setData(data);

        populateParsers();
    }

    @Override
    public AnsHeader parseHeader() {
        mAnsHeaderParser.parse(mParseContext);
        return mAnsHeaderParser.getAnsHeader();
    }

    @Override
    public AnsResponsePacket parse() {
        for (Parser parser : mParsers) {
            parser.parse(mParseContext);
        }

        AnsResponsePacket packet = new AnsResponsePacket();
        packet.setAddressInfos(mAddressListParser.getAddress());
        packet.setExpireTimeSpan(mSettingParser.getExpireTimeSpan());
        packet.setHeader(mAnsHeaderParser.getAnsHeader());
        packet.setRetestTimeSpan(mSettingParser.getRetestTimeSpan());
        packet.setSettingVersion(mSettingParser.getSettingVersion());

        return packet;
    }

    protected void populateParsers() {
        mAnsHeaderParser = new AnsHeaderParser();
        mSettingParser = new SettingParser();
        mAddressListParser = new AddressListParser();

        mParsers.add(mSettingParser);
        mParsers.add(mAddressListParser);
    }
}
