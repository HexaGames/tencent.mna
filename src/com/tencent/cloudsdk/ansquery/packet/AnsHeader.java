/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.ansquery.packet;

import com.tencent.cloudsdk.anssetting.AnsSettingQueryConfig;
import com.tencent.cloudsdk.ansquery.packet.response.AnsHeaderParser;
import com.tencent.cloudsdk.ansquery.packet.response.ParseContext;
import com.tencent.cloudsdk.utils.FormatTransfer;

/**
 * ANS协议头，主要定义包长、命令字、返回码等信息
 *
 * @author 
 */
public class AnsHeader {

    /**
     * 默认头大小，现在协议约定是5字节
     */
    public static final short SIZE = 5;

    /**
     * 包长
     */
    private short usLen;

    /**
     * 命令字
     */
    private short uchCmd;

    /**
     * 响应码，默认为0
     */
    private short uchRetCode = 0;

    /**
     * 数据流
     */
    private byte[] data = new byte[SIZE]; //|包长2字节|命令字1字节|返回码1字节|版本号1字节|

    /**
     * 版本号
     */
    private short version = AnsSettingQueryConfig.ANS_VERSION;

    ///////////////////////////////////////////

    /**
     * 创建一个{@link AnsHeader}
     *
     * @param usLen      包长
     * @param uchCmd     命令字
     * @param uchRetCode 返回码
     * @param version    版本号
     */
    public AnsHeader(short usLen, short uchCmd, short uchRetCode, short version) {
        this.usLen = usLen;
        this.uchCmd = uchCmd;
        this.uchRetCode = uchRetCode;
        this.version = version;

        wrap();
    }

    /**
     * 创建一个{@link AnsHeader}，一般在请求时使用该构造方法
     *
     * @param usLen  包长
     * @param uchCmd 命令字
     */
    public AnsHeader(short usLen, short uchCmd) {
        this.usLen = usLen;
        this.uchCmd = uchCmd;

        wrap();
    }

    /**
     * 创建一个空的{@link AnsHeader}对象
     */
    private AnsHeader() {
        this.usLen = 0;
        this.uchCmd = 0;
        this.uchRetCode = -1;
        this.version = 0;
    }


    ////////////////////////////////////

    /**
     * byte流转解析为{@link AnsHeader}
     *
     * @param data byte流
     * @return {@link AnsHeader}. byte流无效时将返回null
     */
    public static AnsHeader tryParse(byte[] data) {
        AnsHeaderParser headerParser = new AnsHeaderParser();
        ParseContext parseContext = new ParseContext();
        parseContext.setData(data);
        headerParser.parse(parseContext);
        return headerParser.getAnsHeader();
    }

    /**
     * 获取头数据流
     *
     * @return 字节数组
     */
    public byte[] getData() {
        return data;
    }

    public short getUsLen() {
        return usLen;
    }

    public short getUchCmd() {
        return uchCmd;
    }

    public short getUchRetCode() {
        return uchRetCode;
    }

    public short getVersion() {
        return version;
    }

    //////////////////////////// private method //////////////

    /**
     * 封装header
     */
    private void wrap() {

        FormatTransfer.toHH(usLen, data);//按网络序封装包长，占第一、第二两个字节

        data[2] = (byte) uchCmd; //第三位是命令字
        data[3] = (byte) uchRetCode; //第四位是返回码
        data[4] = (byte) version; //第五位是版本号
    }
}
