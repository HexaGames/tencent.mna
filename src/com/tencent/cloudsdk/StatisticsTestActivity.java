/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.tencent.cloudsdk.report.HttpStatistics;
import com.tencent.cloudsdk.report.StatisticsSetting;
import com.tencent.cloudsdk.report.TSocketRecvStatistics;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.tsocket.TSocket;
import com.tencent.cloudsdk.utils.IpUtils;
import com.tencent.cloudsdk.utils.NullReturnException;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.record.debug.WnsClientLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 测试统计上报的界面
 */
public class StatisticsTestActivity extends Activity implements AdapterView.OnItemSelectedListener {
    private List<RSDomainItem> mRsItems = new ArrayList<RSDomainItem>();
    private RSDomainItem mCurRsDomainItem;
    private HandlerThread mHandlerThread;
    private Handler mThreadHandler;
    private Handler mUIHandler;
    private boolean mNeedSucess = true;
    private TextView mResultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);

        populateRSDomainItems();
        Spinner rsDomainSpinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<RSDomainItem> adapter = new ArrayAdapter<RSDomainItem>(this,
                android.R.layout.simple_spinner_item, mRsItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rsDomainSpinner.setAdapter(adapter);
        rsDomainSpinner.setOnItemSelectedListener(this);

        ((ToggleButton) findViewById(R.id.toggleButtonWifi)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                StatisticsSetting.enableOnlyWifi(isChecked);
            }
        });

        ((ToggleButton) findViewById(R.id.toggleButtonSucc)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mNeedSucess = !isChecked;
            }
        });

        mHandlerThread = new HandlerThread("Test Thread");
        mHandlerThread.start();
        mThreadHandler = new Handler(mHandlerThread.getLooper());
        mResultText = (TextView) findViewById(R.id.tv_result);
        mUIHandler = new UIHandler(mResultText);

        initDatas();

        PrintHandler.setHandler(mUIHandler);

    }

    @Override
    protected void onDestroy() {
        mHandlerThread.quit();
        super.onDestroy();
    }

    public void onClick1K(View view) {
        mResultText.append("\n\n");
        if (mCurRsDomainItem.type == RSDomainItem.TYPE_FOR_TCP_TEST) {
            TcpTest tcpTest = new TcpTest(mThreadHandler, mCurRsDomainItem, 1023);
            tcpTest.test();
        } else {
            HttpTest httpTest = new HttpTest(mThreadHandler, mCurRsDomainItem, mNeedSucess, 1023);
            httpTest.test();
        }
    }

    public void onClick4K(View view) {
        mResultText.append("\n\n");
        if (mCurRsDomainItem.type == RSDomainItem.TYPE_FOR_TCP_TEST) {
            TcpTest tcpTest = new TcpTest(mThreadHandler, mCurRsDomainItem, 3 * 1024);
            tcpTest.test();
        } else {
            HttpTest httpTest = new HttpTest(mThreadHandler, mCurRsDomainItem, mNeedSucess, 3 * 1024);
            httpTest.test();
        }
    }

    public void onClick10K(View view) {
        mResultText.append("\n\n");
        if (mCurRsDomainItem.type == RSDomainItem.TYPE_FOR_TCP_TEST) {
            TcpTest tcpTest = new TcpTest(mThreadHandler, mCurRsDomainItem, 9 * 1024);
            tcpTest.test();
        } else {
            HttpTest httpTest = new HttpTest(mThreadHandler, mCurRsDomainItem, mNeedSucess, 9 * 1024);
            httpTest.test();
        }
    }

    public void onClick1M(View view) {
        mResultText.append("\n\n");
        if (mCurRsDomainItem.type == RSDomainItem.TYPE_FOR_TCP_TEST) {
            TcpTest tcpTest = new TcpTest(mThreadHandler, mCurRsDomainItem, 20 * 1024);
            tcpTest.test();
        } else {
            HttpTest httpTest = new HttpTest(mThreadHandler, mCurRsDomainItem, mNeedSucess, 20 * 1024);
            httpTest.test();
        }
    }

    public void onClickOther(View view) {
        mResultText.append("\n\n");
        if (mCurRsDomainItem.type == RSDomainItem.TYPE_FOR_TCP_TEST) {
            TSocketRecvStatisticsTest tSocketRecvStatisticsTest = new TSocketRecvStatisticsTest(mThreadHandler, mCurRsDomainItem, mNeedSucess);
            tSocketRecvStatisticsTest.test();
        } else {
            HttpTest httpTest = new HttpTest(mThreadHandler, mCurRsDomainItem, mNeedSucess, 2 * 1024 * 1024);
            httpTest.test();
        }
    }

    public void onClickClear(View view) {
        mResultText.setText("");
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position >= 0 && position < mRsItems.size()) {
            mCurRsDomainItem = mRsItems.get(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    /**
     * 填充源站
     */
    private void populateRSDomainItems() {
        RSDomainItem item = new RSDomainItem();
        item.display = "ocspeed2.tc.qq.com:80 (TCP)";
        item.port = 80;
        item.type = RSDomainItem.TYPE_FOR_TCP_TEST;
        item.domain = "ocspeed2.tc.qq.com";
        mRsItems.add(item);

        item = new RSDomainItem();
        item.display = "mnatest.qcloud.com:8080 (HTTP)";
        item.port = 8080;
        item.type = RSDomainItem.TYPE_FOR_HTTP_TEST;
        item.domain = "mnatest.qcloud.com";
        mRsItems.add(item);

        mCurRsDomainItem = mRsItems.get(0);
    }

    private void initDatas() {
        mThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                // 这里查询一次
                try {
                    IpUtils.getIpInfosFromDB(GlobalContext.getContext(), "ocspeed2.tc.qq.com", false);
                    IpUtils.getIpInfosFromDB(GlobalContext.getContext(), "mnatest.qcloud.com", true);
                } catch (NullReturnException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    /**
     * 源站的某一项
     */
    public static class RSDomainItem {
        public static final int TYPE_FOR_HTTP_TEST = 1;
        public static final int TYPE_FOR_HTTP_SPEED_TEST = 2;
        public static final int TYPE_FOR_TCP_TEST = 3;
        public static final int TYPE_FOR_CPLUS_TEST = 4;

        public String domain;
        public int port;
        public int type;
        public String display;

        @Override
        public String toString() {
            return display;
        }
    }

    public interface Test {
        void test();

        boolean canRun();
    }

    public static abstract class AbsTest implements Test {
        private Handler handler;

        public AbsTest(Handler handler) {
            this.handler = handler;
        }

        @Override
        public void test() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    doTest();
                }
            });
        }

        protected abstract void doTest();
    }

    private static class TcpTest extends AbsTest {
        private static final String TAG = "TcpTest";
        private RSDomainItem mRsDomainItem;
        private long mSize;

        public TcpTest(Handler handler, RSDomainItem rsDomainItem, long size) {
            super(handler);
            mRsDomainItem = rsDomainItem;
            mSize = size;
        }

        @Override
        protected void doTest() {
            try {
                TSocket tSocket = new TSocket();
                tSocket.connect(new InetSocketAddress(mRsDomainItem.domain, mRsDomainItem.port));
                OutputStream os = tSocket.getOutputStream();
                InputStream in = tSocket.getInputStream();

                writeCertainSizeValue(mSize, os);
                os.flush();

                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String line = br.readLine();
                WnsClientLog.i("Jie", ">>line=" + line);
                br.close();
                tSocket.close();

            } catch (UnknownHostException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            } catch (IOException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            }
        }

        private void writeCertainSizeValue(long size, OutputStream os) {
            long curSize;
            String head = "GET / HTTP/1.1\r\n"
                    + "HOST: " + mRsDomainItem.domain
                    + "\r\n"
                    + "User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31\r\n"
                    + "\r\n\r\n";
            curSize = head.getBytes().length;
            long remainSize = size - curSize;
            head += new String(new byte[(int) remainSize]);

            try {
                os.write(head.getBytes());
            } catch (IOException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            }
        }

        @Override
        public boolean canRun() {
            return mRsDomainItem.type == RSDomainItem.TYPE_FOR_TCP_TEST;
        }
    }

    public static class TSocketRecvStatisticsTest extends AbsTest {
        private static final String TAG = "TSocketRecvStatisticsTest";
        private RSDomainItem mItem;
        private boolean mSuccess;

        public TSocketRecvStatisticsTest(Handler handler, RSDomainItem rsDomainItem, boolean success) {
            super(handler);
            mItem = rsDomainItem;
            mSuccess = success;
        }

        @Override
        protected void doTest() {
            TSocketRecvStatistics tSocketRecvStatistics = new TSocketRecvStatistics();
            tSocketRecvStatistics.timeStart();

            try {
                TSocket tSocket = new TSocket();
                tSocket.connect(new InetSocketAddress(mItem.domain, mItem.port));
                OutputStream os = tSocket.getOutputStream();

                writeCertainSizeValue(2 * 1024 * 1024, os);
                os.flush();
                tSocket.close();

            } catch (UnknownHostException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            } catch (IOException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            }
            tSocketRecvStatistics.timeEnd();
            int errCode = mSuccess ? TSocketRecvStatistics.ERR_CODE_SUCC : TSocketRecvStatistics.ERR_CODE_FAIL;
            tSocketRecvStatistics.report(mItem.domain, errCode, 2 * 1024 * 1024);
        }

        @Override
        public boolean canRun() {
            return false;
        }

        private void writeCertainSizeValue(long size, OutputStream os) {
            long curSize;
            String head = "GET / HTTP/1.1\r\n"
                    + "HOST: " + mItem.domain
                    + "\r\n"
                    + "User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31\r\n"
                    + "\r\n\r\n";
            curSize = head.getBytes().length;
            long remainSize = size - curSize;
            head += new String(new byte[(int) remainSize]);

            try {
                os.write(head.getBytes());
            } catch (IOException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            }
        }
    }

    public static class HttpTest extends AbsTest {
        private static final String TAG = "HttpTest";
        private RSDomainItem mCurItem;
        private boolean mSucc;
        private long mSize;

        public HttpTest(Handler handler, RSDomainItem item, boolean success, long size) {
            super(handler);
            mCurItem = item;
            mSucc = success;
            mSize = size;
        }

        @Override
        protected void doTest() {
            HttpStatistics httpStatistics = new HttpStatistics();
            httpStatistics.timeStart();
            try {
                Thread.sleep(1304);
            } catch (InterruptedException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
            }
            httpStatistics.timeEnd();
            int ret = mSucc ? HttpStatistics.ERR_CODE_SUCC : HttpStatistics.ERR_CODE_FAIL;
            httpStatistics.report("http://" + mCurItem.domain, ret, mSize);
        }

        @Override
        public boolean canRun() {
            return mCurItem.type == RSDomainItem.TYPE_FOR_HTTP_TEST;
        }
    }

    private static class UIHandler extends Handler {
        private HashSet<String> mTagSet = new HashSet<String>();
        private WeakReference<TextView> mTextViewRef;

        public UIHandler(TextView textView) {
            mTagSet.add("AbsCircleReport");
            mTagSet.add("HttpStatisticsData");
            mTagSet.add("HttpStatisticsReportImp");
            mTagSet.add("TcpStatisticsData");
            mTagSet.add("TcpStatisticsReportImp");
            mTextViewRef = new WeakReference<TextView>(textView);
        }

        @Override
        public void handleMessage(Message message) {
            TextView textView = mTextViewRef.get();
            if (textView == null) {
                return;
            }

            int what = message.what;
            String tag = ((PrintHandler.Log) message.obj).tag;
            String msg = ((PrintHandler.Log) message.obj).msg;
            SpannableString msgNew = new SpannableString(msg);
            switch (what) {
                case PrintHandler.WHAT_ADD_LOG:
                    if (mTagSet.contains(tag)) {
                        msgNew.setSpan(new ForegroundColorSpan(Color.BLUE), 0, msgNew.length(), 0);
                        msgNew.setSpan(new BackgroundColorSpan(Color.LTGRAY), 0, msgNew.length(), 0);
                    }
                    textView.append(msgNew);
                    textView.append("\n");
                    break;
                case PrintHandler.WHAT_CLEAR_LOG:
                    textView.setText("");
                    break;
            }
        }
    }
}
