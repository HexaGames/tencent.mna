/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.dns;

//import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 为生成唯一请求ID提供类。<br/>
 * 采用递增的方式保证ID的唯一性，范围从100到65535。当递增至达到最大值后，将从新从100开始递增
 *
 * @author 
 */
public class AtomicRequestId {

    private static AtomicRequestId reqId = null;

    private final static int initValue = 100;  //初始值
    private final static int maxValue = 0xffff; //最大值
    private static AtomicInteger reqIdentity = new AtomicInteger(initValue);

    /**
     * 生成AtomicRequestId 实例
     *
     * @return
     */
    public synchronized static AtomicRequestId getInstance() {
        if (reqId == null) {
            reqId = new AtomicRequestId();
        }

        return reqId;
    }

    /**
     * 获取唯一ID
     *
     * @return
     */
    public synchronized int getId() {
        int id = reqIdentity.getAndIncrement();
        //WnsClientLog.w("dnstest", "ReqId:" + id);

        /**当id超过最大值时，从新初始化AtomicInteger*/
        if (id >= maxValue) {
            reqIdentity = new AtomicInteger(initValue);
            id = reqIdentity.getAndIncrement();
        }

        return id;
    }

}
