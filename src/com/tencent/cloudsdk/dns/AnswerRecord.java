/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.dns;

/**
 * 应答记录实体类
 *
 * @author 
 */
public class AnswerRecord {

    /**
     * SERVER 应答的域名
     */
    public String domain;

    /**
     * SERVER 应答的IP，以byte数组形式存在
     */
    public byte[] ip;

    /**
     * SERVER 应答的ANSWER类型，一般跟QTYPE一致
     */
    public int type;

    /**
     * SERVER 应答的TTL，用来做缓存的有效时长
     */
    public long ttl;

    /**
     * SERVER 应答的CLASS，一般跟QCLASS保持一致
     */
    public int qclass;


    /////////////////////////////////////////////////

    public AnswerRecord() {

    }

    public AnswerRecord(String domain, byte[] ip, int type, long ttl, int qclass) {
        this.domain = domain;
        this.ip = ip;
        this.type = type;
        this.ttl = ttl;
        this.qclass = qclass;
    }

}
