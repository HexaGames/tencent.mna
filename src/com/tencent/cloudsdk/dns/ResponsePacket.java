/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.dns;

import com.tencent.record.debug.WnsClientLog;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * DNS响应包解析类
 *
 * @author 
 */
public class ResponsePacket {
    private static final String TAG = "ResponsePacket";
    private DNSInput in;
    private int[] counts = new int[4];
    private int reqId;
    private int flags;
    @SuppressWarnings("rawtypes")
    private List[] sections;
    private long expireTime = 0;
    private String host = "";

    private static final int MAXLABEL = 64;
    private static final int LABEL_NORMAL = 0;
    private static final int LABEL_COMPRESSION = 0xC0;
    private static final int LABEL_MASK = 0xC0;

    private static final int SECTION_QUESTION = 0;
    private static final int SECTION_ADDRESS = 1;

    private byte[] label = new byte[MAXLABEL];
    private StringBuilder nameBuilder = new StringBuilder();


    public ResponsePacket(DNSInput in, String host) throws WireParseException, UnknownHostException, Exception {
        this.in = in;
        this.host = host;

        sections = new List[4];

        initHeader();

        /**检查响应包有无错误*/
        check(flags);
        parseAnswer();
    }


    @SuppressWarnings("unchecked")
    public ArrayList<AnswerRecord> getAnswers() {

        return (ArrayList<AnswerRecord>) sections[SECTION_ADDRESS];
    }

    /**
     * 获取host的InetAddress信息
     *
     * @return
     */
    public InetAddress[] getByAddress() {

        if (sections[SECTION_ADDRESS] != null && sections[SECTION_ADDRESS].size() > 0) {

            ArrayList<InetAddress> list = new ArrayList<InetAddress>();
            for (int i = 0; i < sections[SECTION_ADDRESS].size(); i++) {
                AnswerRecord ar = (AnswerRecord) sections[SECTION_ADDRESS].get(i);
                try {
                    InetAddress ia = InetAddress.getByAddress(ar.domain, ar.ip);

                    //这里做一下容错处理，防止有些极品机型的InetAddress.getByAddress方法不能正确返回一个
                    //InetAddress 对象。因为只需要ANS服务器的IP，故这里不做hostName的验证
                    if (ia != null && ia.getHostAddress() != null && ia.getHostAddress().length() > 0) {
                        list.add(ia);
                    }
                } catch (UnknownHostException e) {
                    WnsClientLog.e(TAG, e.getMessage(), e);
                }
            }

            return list.toArray(new InetAddress[list.size()]);
        }

        return null;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public int getReqId() {

        return reqId;
    }

    ////////////////////////////////////////////////

    /**
     * 初始化头部数据
     */
    private void initHeader() throws WireParseException {
        reqId = in.readU16();
        flags = in.readU16();

        for (int i = 0; i < counts.length; i++)
            counts[i] = in.readU16();
    }

    @SuppressWarnings("unchecked")
    private void parseAnswer() throws WireParseException {
        try {
            for (int i = 0; i < 2; i++) {//只读QUESTIONG、ANSWER部分
                int count = counts[i];
                if (count > 0)
                    sections[i] = new ArrayList<AnswerRecord>(count);
                for (int j = 0; j < count; j++) {

                    AnswerRecord ar = new AnswerRecord();

                    if (i == SECTION_QUESTION) {

                        ar.domain = retrieveName();
                        ar.type = in.readU16();
                        ar.qclass = in.readU16();

                        sections[i].add(ar);
                    } else {

                        /**走这个方法目的不在于获取域名，而在于将in的索引值移到正确的位置 ，*/
                        /**否则将影响后续数值的读取*/
                        retrieveName();

                        /**从QUESTION中获取域名*/
                        ar.domain = host; //((AnswerRecord)sections[SECTION_QUESTION].get(0)).domain;
                        ar.type = in.readU16();
                        ar.qclass = in.readU16();
                        ar.ttl = in.readU32();

                        //WnsClientLog.e("dnstest", "TTL:" + ar.ttl);

                        in.setActive(in.readU16());
                        ar.ip = in.readByteArray();

                        //直接丢弃非ADDRESS类型的记录
                        if (ar.type == DnsConstants.QTYPE_A) {
                            setExpireTime(ar.ttl);
                            sections[i].add(ar);
                        }
                    }
                }
            }
        } catch (WireParseException e) {
            throw e;
        }
    }

    private String retrieveName() throws WireParseException {
        int len, pos;
        boolean done = false;
        boolean savedState = false;

        /**清空namebuilder*/
        if (nameBuilder.length() > 0) {
            nameBuilder.delete(0, nameBuilder.length());
        }

        while (!done) {
            len = in.readU8();
            switch (len & LABEL_MASK) {
                case LABEL_NORMAL:
                    if (len == 0) {

                        done = true;
                    } else {

                        in.readByteArray(label, 0, len);
                        nameBuilder.append(ByteBase.byteString(label, len));
                        nameBuilder.append(".");
                    }
                    break;
                case LABEL_COMPRESSION:
                    pos = in.readU8();
                    pos += ((len & ~LABEL_MASK) << 8);


                    if (pos >= in.current() - 2)
                        throw new WireParseException("bad compression");
                    if (!savedState) {
                        in.save();
                        savedState = true;
                    }
                    in.jump(pos);

                    break;
                default:
                    throw new WireParseException("bad label type");
            }
        }
        if (savedState) {
            in.restore();
        }

        if (nameBuilder.length() > 0) {
            nameBuilder.deleteCharAt(nameBuilder.length() - 1);
        }
        return nameBuilder.toString();
    }

    /**
     * 对flags进行RCODE的判断，比如当RCODE为3时，抛出一个UnknownHostException
     *
     * @param flags
     * @throws UnknownHostException
     * @throws Exception
     */
    private void check(int flags) throws UnknownHostException, Exception {
        String flagsBinaryString = Integer.toBinaryString(flags);
        if (flagsBinaryString.length() < 4) {
            throw new Exception("exception cause [FBS - " + flagsBinaryString + "]");
        }

        String rcode = flagsBinaryString.substring(flagsBinaryString.length() - 4);

        if (rcode.equals("0011")) {

            throw new UnknownHostException("Unable to resolve host \"" + host + "\": No address associated with hostname");
        } else if (!rcode.equals("0000")) {

            throw new Exception("exception cause [RCODE - " + rcode + "]");
        }
    }

    /**
     * 设置过期时间，过期间隔为一个ttl
     *
     * @param ttl
     */
    private void setExpireTime(long ttl) {
        if (expireTime == 0 && ttl > 0) {
            expireTime = System.currentTimeMillis() + (ttl * 1000);//ttl 为秒
        }
    }
}
