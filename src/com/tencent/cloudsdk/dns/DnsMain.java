/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.dns;

//import android.util.Log;

import com.tencent.cloudsdk.cache.DnsCacheManager;
import com.tencent.record.debug.WnsClientLog;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * DNS解析入口类。主要提供从指定DNS服务器查询更优的HOST的方法
 *
 * @author 
 */
public class DnsMain {

    private final static String TAG = DnsMain.class.getName();

    /**
     * 从指定DNS服务器查询更优的HOST
     *
     * @param domain
     * @return InetAddress数组，包含域名下的所有IP信息，如果域名不对或者查询失败将返回null
     * @author 
     */
    public static InetAddress[] getBetterHostByName(String domain) {

        //这里传过来的domain有可能是一个url，先从url里获取host
        domain = getHostName(domain);
        //WnsClientLog.w("DNSResolve", "hostName:" + domain);
        //WnsClientLog.v(TAG, "get better host for name:" + domain);

        //int rcode = 0;
        //String info = null;
        //long startTime = SystemClock.elapsedRealtime();

        // 防止domain为null或者为空格
        if (domain == null || domain.trim().length() <= 0) {

            /** domain为null或者为空，统计上报参数错误*/
            //AbsReportTool.getInstance().report(domain, startTime, 0, 0, Constants.ERROR_PARAM, null, null);
            return null;
        }

        domain = domain.trim();

        InetAddress[] address = null;

        /** 从缓存里查找 */
        address = DnsCacheManager.getInstance().getCacheItemByHost(domain);
        if (address != null) {

            /**这里取第一个IP用于统计上报*/
            //info = "ip=" + address[0].getHostAddress() + "&dns=cache";
            /**从缓存中获取到address信息，统计上报[成功]*/
            //AbsReportTool.getInstance().report(domain, startTime, 0, 0, 0, null, info);
            return address;
        }

        ///////////////////////////////开始做网络查找////////////////////////////

        Lookup lookup = null;

        // 优先从114查询
        try {

            lookup = new Lookup(DnsConstants.DNS_SERVER_ADDRESS_114);
            address = lookup.run(domain);

            if (address != null && address.length > 0) {

                /**这里取第一个IP用于统计上报*/
                //info = "ip=" + address[0].getHostAddress() + "&dns=114";
                //AbsReportTool.getInstance().report(domain, startTime, 0, 0, 0, null, info);
                return address;
            } else {
                //rcode = Constants.ERROR_UNKNOWN;
            }

        } catch (UnknownHostException e) {
            WnsClientLog.e(TAG, "UnknownHostException cause[" + domain + "][114.114.114.114]." + e.getMessage());
            //rcode = Constants.ERROR_UNKNOWN_HOST;
        } catch (WireParseException e) {
            WnsClientLog.e(TAG, "WireParseException cause[" + domain + "][114.114.114.114]." + e.getMessage());
            //rcode = Constants.ERROR_WIRE_PARSE;
        } catch (SocketTimeoutException e) {
            WnsClientLog.e(TAG, "SocketTimeoutException cause[" + domain + "][114.114.114.114]." + e.getMessage());
            //rcode = Constants.ERROR_SOCKETTIMEOUT;
        } catch (IOException e) {
            WnsClientLog.e(TAG, "IOException cause[" + domain + "][114.114.114.114]." + e.getMessage());
            //rcode = Constants.ERROR_IO;
        } catch (Exception e) {
            WnsClientLog.e(TAG, "Exception cause[" + domain + "][114.114.114.114]." + e.getMessage());
            //rcode = Constants.ERROR_UNKNOWN;
        }

        /**代码走到这里，证明114DNS没有成功解析，这里将114DNS解析错误的错误码上报*/
        //AbsReportTool.getInstance().report(domain, startTime, 0, 0, rcode, null, null);

        // ////////////////////////////////////////////////

        // 114查询不到，转至8.8查询
        try {

            if (lookup == null) {
                lookup = new Lookup(DnsConstants.DNS_SERVER_ADDRESS_8);

            } else {
                lookup.setDnsAddress(DnsConstants.DNS_SERVER_ADDRESS_8);
            }

            address = lookup.run(domain);
            if (address != null && address.length > 0) {

                /**这里取第一个IP用于统计上报*/
                //info = "ip=" + address[0].getHostAddress() + "&dns=8.8";
                //AbsReportTool.getInstance().report(domain, startTime, 0, 0, 0, null, info);
                return address;
            }

        } catch (UnknownHostException e) {
            WnsClientLog.e(TAG, "UnknownHostException cause[" + domain + "][8.8.8.8]." + e.getMessage());
        } catch (WireParseException e) {
            WnsClientLog.e(TAG, "WireParseException cause[" + domain + "][8.8.8.8]." + e.getMessage());
        } catch (SocketTimeoutException e) {
            WnsClientLog.e(TAG, "SocketTimeoutException cause[" + domain + "][8.8.8.8]." + e.getMessage());
        } catch (IOException e) {
            WnsClientLog.e(TAG, "IOException cause[" + domain + "][8.8.8.8]." + e.getMessage());
        } catch (Exception e) {
            WnsClientLog.e(TAG, "Exception cause[" + domain + "][8.8.8.8]." + e.getMessage());
        }

        //8.8解析错误不做上报

        // /////////////////////////////////////////

        // 指定DNS服务器查询不到，从默认的DNS查询
        try {

            //WnsClientLog.w("dnstest", "DNS SERVER：LDNS");
            address = InetAddress.getAllByName(domain);

            if (address != null && address.length > 0) {

                /**这里取第一个IP用于统计上报*/
                //info = "ip=" + address[0].getHostAddress() + "&dns=ldns";
                //AbsReportTool.getInstance().report(domain, startTime, 0, 0, 0, null, info);
                return address;
            }

        } catch (UnknownHostException e) {
            WnsClientLog.e(TAG, "UnknownHostException cause[" + domain + "][LDNS]." + e.getMessage());
        }

        /** 代码走到这里，证明已经将114DNS解析的错误上报，这里不再做解析错误的重复上报*/


        /**所有DNS都查询不到，直接返回null*/
        return null;

    }

    /**
     * 从URL中截取host
     *
     * @param url
     * @return
     */
    public static String getHostName(String url) {
        if (url == null)
            return "";

        url = url.trim();
        String host = url.toLowerCase();
        int end;

        if (host.startsWith("http://")) {
            end = url.indexOf("/", 8);
            if (end > 7) {
                host = url.substring(7, end);
            } else {
                host = url.substring(7);
            }

        } else if (host.startsWith("https://")) {
            end = url.indexOf("/", 9);
            if (end > 8) {
                host = url.substring(8, end);
            } else {
                host = url.substring(8);
            }
        } else {
            end = url.indexOf("/", 1);
            if (end > 1) {
                host = url.substring(0, url.indexOf("/", 1));
            } else {
                host = url;
            }
        }

        return host;
    }
}
