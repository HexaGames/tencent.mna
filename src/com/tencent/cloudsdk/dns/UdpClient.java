/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.dns;

//import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.security.SecureRandom;

/**
 * udp请求封装
 *
 * @author 
 */
final class UdpClient {

    private static final int EPHEMERAL_START = 1024;
    private static final int EPHEMERAL_STOP = 65535;
    private static final int EPHEMERAL_RANGE = EPHEMERAL_STOP - EPHEMERAL_START;

    private static final long TIMEOUT_VALUE = 3 * 1000; //sage:一般3秒没有响应就不会再响应。这里设置为3秒
    private static final int MAX_SIZE = 512;

    private static SecureRandom prng = new SecureRandom();

    public byte[] sendrecv(String dnsAddress, byte[] data) throws IOException, SocketTimeoutException {

        SelectableChannel channel = null;
        SelectionKey key = null;
        try {
            channel = DatagramChannel.open();
            channel.configureBlocking(false);
            Selector selector = Selector.open();

            key = channel.register(selector, SelectionKey.OP_READ);
            DatagramChannel datagramchannel = (DatagramChannel) key.channel();

            int port = prng.nextInt(EPHEMERAL_RANGE) + 1024;
            InetSocketAddress temp = new InetSocketAddress(port);
            datagramchannel.socket().bind(temp);

            InetSocketAddress dnsServerSocketAdress = new InetSocketAddress(InetAddress.getByName(dnsAddress), DnsConstants.DNS_PORT);

            datagramchannel.connect(dnsServerSocketAdress);
            datagramchannel.write(ByteBuffer.wrap(data));

            byte[] tmp = new byte[MAX_SIZE];
            //key.interestOps(SelectionKey.OP_READ);
            long endTime = System.currentTimeMillis() + TIMEOUT_VALUE;
            try {
                while (!key.isReadable())
                    blockUntil(key, endTime);
            } finally {
                if (key.isValid())
                    key.interestOps(0);
            }

            long ret = datagramchannel.read(ByteBuffer.wrap(tmp));
            if (ret > 0) {
                int len = (int) ret;
                data = new byte[len];
                System.arraycopy(tmp, 0, data, 0, len);

                return data;
            }

        } finally {
            if (key != null) {
                key.selector().close();
                key.channel().close();
            }
        }

        return null;

    }

    private static void blockUntil(SelectionKey key, long endTime) throws IOException, SocketTimeoutException {
        long timeout = endTime - System.currentTimeMillis();
        //WnsClientLog.w("dnstest", "timeout:[" + Thread.currentThread().getId() + "]" + timeout);
        int nkeys = 0;
        if (timeout > 0)
            nkeys = key.selector().select(timeout);
        else if (timeout == 0)
            nkeys = key.selector().selectNow();
        if (nkeys == 0)
            throw new SocketTimeoutException();
    }

}
