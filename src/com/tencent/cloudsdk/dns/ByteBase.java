/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.dns;

import java.text.DecimalFormat;

/**
 * byte、string的转换操作类
 *
 * @author 
 */
public class ByteBase {

    private static final DecimalFormat byteFormat = new DecimalFormat();

    /**
     * @param array
     * @return
     */
    public static String byteString(byte[] array, int len) {

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            int b = array[i] & 0xFF;
            if (b <= 0x20 || b >= 0x7f) {
                sb.append('\\');
                sb.append(byteFormat.format(b));
            } else if (b == '"' || b == '(' || b == ')' || b == '.' ||
                    b == ';' || b == '\\' || b == '@' || b == '$') {
                sb.append('\\');
                sb.append((char) b);
            } else
                sb.append((char) b);
        }
        return sb.toString();
    }
}
