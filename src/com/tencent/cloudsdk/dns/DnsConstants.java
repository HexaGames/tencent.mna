/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.dns;

/**
 * 包含dns查询时用到的常量
 *
 * @author 
 */
public class DnsConstants {

    /**
     * 114 DNS Server 地址
     */
    public static final String DNS_SERVER_ADDRESS_114 = "114.114.114.114";

    /**
     * 8 DNS server 地址
     */
    public static final String DNS_SERVER_ADDRESS_8 = "8.8.8.8";

    /**
     * 连接DNS Server端口号
     */
    public static final int DNS_PORT = 53;


    /**
     * DNS Query type, IP address 查询
     */
    public static final int QTYPE_A = 1;
    /**
     * DNS Query type, 别名查询
     */
    public static final int QTYPE_CNAME = 5;

}
