/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.dns;


//import android.util.Log;

import com.tencent.cloudsdk.cache.DnsCacheManager;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * 到 dns server查询指定域名ip地址
 *
 * @author 
 */
public class Lookup {

    private String dnsServerAddress = "";

    public Lookup(String dnsAddress) throws UnknownHostException {
        //WnsClientLog.w("dnstest", "DNS SERVER：" + dnsAddress);
        this.dnsServerAddress = dnsAddress;
    }


    /////////////////////////////////////////////////////////////

    /**
     * 设置DNS ADDRESS
     *
     * @param dnsAddress
     */
    public void setDnsAddress(String dnsAddress) {
        //WnsClientLog.w("dnstest", "DNS SERVER：" + dnsAddress);
        this.dnsServerAddress = dnsAddress;
    }

    /**
     * 查询指定域名ip信息
     *
     * @param domain 域名
     * @return
     * @throws Exception
     */
    public InetAddress[] run(String domain) throws IOException, SocketTimeoutException, WireParseException, UnknownHostException, Exception {

        InetAddress[] address = null;

        //long startTime = System.currentTimeMillis();
        //long endTime;
        //WnsClientLog.w("dnstest", "装包："+startTime);

        RequestPacket reqPacket = new RequestPacket(domain);
        byte[] queryData = reqPacket.getQueryData();
        //endTime = System.currentTimeMillis();
        //WnsClientLog.w("dnstest", ">>装包DONE："+endTime);
        //WnsClientLog.e("dnstest", ">>装包耗时："+ (endTime - startTime));

        if (queryData == null) {
            return null;
        }

        try {

            //startTime = System.currentTimeMillis();
            //WnsClientLog.w("dnstest", "DNS请求："+startTime);
            byte[] responseData = new UdpClient().sendrecv(dnsServerAddress, queryData);
            //endTime = System.currentTimeMillis();
            //WnsClientLog.w("dnstest", ">>DNS请求DONE："+endTime);
            //WnsClientLog.e("dnstest", ">>DNS请求耗时："+(endTime - startTime));

            if (responseData != null) {
                //startTime = System.currentTimeMillis();
                //WnsClientLog.w("dnstest", "解包："+startTime);
                ResponsePacket repPacket = new ResponsePacket(new DNSInput(responseData), domain);
                if (repPacket.getReqId() == reqPacket.getReqId()) {
                    address = repPacket.getByAddress();
                    if (address != null && address.length > 0) {

                        //添加到缓存
                        DnsCacheManager.getInstance().addCache(domain, address
                                , repPacket.getExpireTime());
                    }
                }
                //endTime = System.currentTimeMillis();
                //WnsClientLog.w("dnstest", ">>解包 DONE："+endTime);
                //WnsClientLog.e("dnstest", ">>解包耗时："+ (endTime - startTime));
            }
        } catch (WireParseException e) {

            throw e;
        } catch (SocketTimeoutException e) {

            throw e;
        } catch (IOException e) {

            throw e;
        }

        return address;
    }
}
