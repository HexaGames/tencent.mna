/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk;

import android.content.Context;
import android.net.ConnectivityManager;

import com.tencent.record.debug.WnsClientLog;

import java.lang.reflect.Method;

public class MobileAdmin {
    private static final String TAG = "MobileAdmin";

    private ConnectivityManager mCM;

    public MobileAdmin(Context context) {
        mCM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public boolean GetMobileDataEnable() {
        Class cmClass = mCM.getClass();
        Class[] argClasses = null;
        Object[] argObject = null;

        Boolean isOpen = false;
        try {
            Method method = cmClass.getMethod("getMobileDataEnabled", argClasses);
            isOpen = (Boolean) method.invoke(mCM, argObject);
        } catch (Exception e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
        return isOpen;
    }

    public void setMobileDataEnable(boolean isEnable) {
        Class cmClass = mCM.getClass();
        Class[] argClasses = new Class[1];
        argClasses[0] = boolean.class;

        try {
            Method method = cmClass.getMethod("setMobileDataEnabled", argClasses);
            method.invoke(mCM, isEnable);
        } catch (Exception e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    public void OpenMobileData() {
        setMobileDataEnable(true);

    }

    public void CloseMobileData() {
        setMobileDataEnable(false);
    }
}
