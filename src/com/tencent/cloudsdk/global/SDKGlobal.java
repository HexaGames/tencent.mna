/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.global;

import android.content.Context;

import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.report.imp.StatisticsCircleReport;
import com.tencent.cloudsdk.report.imp.StatisticsReportConfig;
import com.tencent.cloudsdk.report.failreport.FailCountReport;
import com.tencent.cloudsdk.report.httpstatistics.HttpStatisticsData;
import com.tencent.cloudsdk.report.tcpstatistics.TcpStatisticsData;
import com.tencent.cloudsdk.report.tool.ReportTool;
import com.tencent.cloudsdk.report.tool.ReportToolFactory;
import com.tencent.cloudsdk.utils.ClientIdManager;

/**
 * SDK全局的信息
 */
public class SDKGlobal {
    private static ReportToolFactory sReportToolFactory;
    private static TcpStatisticsData sTcpStatisticsData;
    private static HttpStatisticsData sHttpStatisticsData;
    private static StatisticsCircleReport sStatisticsCircleReport;

    public static void init(Context context) {
        // 设置上报工具
        sReportToolFactory = new ReportToolFactory();
        sTcpStatisticsData = new TcpStatisticsData();
        sHttpStatisticsData = new HttpStatisticsData();
        sStatisticsCircleReport = new StatisticsCircleReport(sTcpStatisticsData, sHttpStatisticsData,
                sReportToolFactory.getReportTool(ReportToolFactory.STYLE_ZIP),
                sReportToolFactory.getReportTool(ReportToolFactory.STYLE_ZIP));

        FailCountReport.startReport();
        sStatisticsCircleReport.stopReport();
        sStatisticsCircleReport.startReport();
        ClientIdManager.getInstance();

        AnsSetting.init();
    }

    public static ReportTool getReportTool(int type) {
        return sReportToolFactory.getReportTool(type);
    }

    public static ReportTool getReportTool() {
        return sReportToolFactory.getReportTool(ReportToolFactory.STYLE_NORMAL);
    }

    public static TcpStatisticsData getTCPStatisticData() {
        return sTcpStatisticsData;
    }

    public static HttpStatisticsData getHttpStatisticsData() {
        return sHttpStatisticsData;
    }

    public static StatisticsReportConfig getReportConfig() {
        return sStatisticsCircleReport.getReportConfig();
    }
}
