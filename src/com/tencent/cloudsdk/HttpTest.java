/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk;

import com.tencent.cloudsdk.http.TAndroidHttpClient;
import com.tencent.cloudsdk.http.TDefaultHttpClient;
import com.tencent.cloudsdk.http.TURLStreamHandler;
import com.tencent.cloudsdk.report.HttpStatistics;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.cloudsdk.utils.SpeedConfig;
import com.tencent.record.debug.WnsClientLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 
 */
public class HttpTest extends AbsTest<MainActivity.RSDomainItem> {
    private static final String TAG = "HttpTest";

    private MainActivity.RSDomainItem mItem;
    private int mRound = 1;

    HttpStatistics httpStatistics = new HttpStatistics();

    @Override
    protected void beforeDoTest() {
        SpeedConfig.reset(GlobalContext.getContext(), mItem.domain);
        PrintHandler.clearLog();
        PrintHandler.addLog(TAG, "=======自动化测试HTTP （第 " + (mRound++) + " 轮）=======");
    }

    @Override
    protected void postDoTest() {

    }

    @Override
    protected void postTestFinished() {
        PrintHandler.addLog(TAG, "=======自动化测试HTTP 结束（共 " + (--mRound) + " 轮）=======");
    }

    @Override
    public void setData(MainActivity.RSDomainItem data) {
        mItem = data;
    }

    @Override
    public boolean canRun() {
        return mItem.type == MainActivity.RSDomainItem.TYPE_FOR_HTTP_TEST;
    }

    @Override
    public void doTest() {
        PrintHandler.addLog(TAG, ">>开始测试TDefaultHttpClient的Get请求.");
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, 3000);
        HttpConnectionParams.setSoTimeout(params, 5000);
        TDefaultHttpClient httpClient = new TDefaultHttpClient(params);

        downloadFileWithHttpClient(httpClient, "a.jpg");
        downloadFileWithHttpClient(httpClient, "b.jpg");
        downloadFileWithHttpClient(httpClient, "c.jpg");
        downloadFileWithHttpClient(httpClient, "d.jpg");
        getWithHttpClient(httpClient, "get.php");

        PrintHandler.addLog(TAG, ">>开始测试TDefaultHttpClient的Post请求.");
        postWithHttpClient(httpClient, "post.php", false);
        postWithHttpClient(httpClient, "post.php", true);

        PrintHandler.addLog(TAG, ">>开始测试TAndroidHttpClient的Get请求.");
        HttpClient androidClient = TAndroidHttpClient.newInstance("");

        downloadFileWithHttpClient(androidClient, "a.jpg");
        downloadFileWithHttpClient(androidClient, "b.jpg");
        downloadFileWithHttpClient(androidClient, "c.jpg");
        downloadFileWithHttpClient(androidClient, "d.jpg");
        getWithHttpClient(androidClient, "get.php");

        PrintHandler.addLog(TAG, ">>开始测试TAndroidHttpClient的Post请求.");
        postWithHttpClient(androidClient, "post.php", false);
        postWithHttpClient(androidClient, "post.php", true);
        ((TAndroidHttpClient) androidClient).close();

        PrintHandler.addLog(TAG, ">>开始测试TURLStreamHandler的Get请求.");
        downloadFileWithHttpUrlConnection("a.jpg");
        downloadFileWithHttpUrlConnection("b.jpg");
        downloadFileWithHttpUrlConnection("c.jpg");
        downloadFileWithHttpUrlConnection("d.jpg");
        getWithHttpUrlConnection("get.php");

        PrintHandler.addLog(TAG, ">>开始测试TURLStreamHandler的Post请求.");
        postWithHttpUrlConnection("post.php", false);
        postWithHttpUrlConnection("post.php", true);
    }

    private void downloadFileWithHttpClient(HttpClient httpClient, String fileName) {
        String httpUrl = getUrl(fileName);

        try {
            PrintHandler.addLog(TAG, ">>发送Http Get请求,下载" + fileName);
            HttpGet request = new HttpGet(httpUrl);

            httpStatistics.timeStart();
            HttpResponse response = httpClient.execute(request);

            HttpEntity entity = response.getEntity();
            long length = entity.getContentLength();
            PrintHandler.addLog(TAG, "http返回数据长度为" + length + ", 开始下载");

            InputStream is = entity.getContent();
            byte[] buf = new byte[1024];
            int ch = -1;
            int count = 0;
            while ((ch = is.read(buf)) != -1) {
                count += ch;
            }
            httpStatistics.timeEnd();
            httpStatistics.report(httpUrl, HttpStatistics.ERR_CODE_SUCC, count);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                PrintHandler.addLog(TAG, "http返回码为失败，ret = " + statusCode + ", result = " + new String(buf, 0, count));
            }
            PrintHandler.addLog(TAG, "下载完毕，总下载长度为" + count);
        } catch (ClientProtocolException e) {
            PrintHandler.addLog(TAG, "TDefaultHttpClient get 失败，ClientProtocolException");
            WnsClientLog.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            httpStatistics.timeEnd();
            httpStatistics.report(httpUrl, HttpStatistics.ERR_CODE_FAIL, 0);
            PrintHandler.addLog(TAG, "TDefaultHttpClient get 失败，IOException");
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    private void downloadFileWithHttpUrlConnection(String fileName) {
        String httpUrl = getUrl(fileName);

        TURLStreamHandler urlStreamHandler = new TURLStreamHandler();
        URL url = null;
        try {
            url = new URL(null, httpUrl, urlStreamHandler);
        } catch (MalformedURLException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }

        try {
            httpStatistics.timeStart();
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(6 * 1000);
            PrintHandler.addLog(TAG, ">>发送Http Get请求,下载" + fileName);
            InputStream is = connection.getInputStream();

            byte[] buffer = new byte[1024];
            int len = -1;
            int count = 0;
            while ((len = is.read(buffer)) != -1) {
                count += len;
            }
            httpStatistics.timeEnd();
            httpStatistics.report(httpUrl, HttpStatistics.ERR_CODE_SUCC, count);

            PrintHandler.addLog(TAG, "下载完毕，总下载长度为" + count);
        } catch (IOException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
            httpStatistics.timeEnd();
            httpStatistics.report(httpUrl, HttpStatistics.ERR_CODE_FAIL, 0);
        }
    }

    private void getWithHttpClient(HttpClient httpClient, String fileName) {
        String httpUrl = getUrl(fileName);
        httpUrl += "?test=123456&test2=654321";

        try {
            PrintHandler.addLog(TAG, ">>发送Http Get请求,获取数据从" + httpUrl);
            HttpGet request = new HttpGet(httpUrl);
            HttpResponse response = httpClient.execute(request);

            HttpEntity entity = response.getEntity();

            InputStream is = entity.getContent();
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
            byte[] buf = new byte[1024];
            int len = -1;
            while ((len = is.read(buf)) != -1) {
                bos.write(buf, 0, len);
            }

            String received = new String(bos.toByteArray());
            PrintHandler.addLog(TAG, "下载完毕，收到字符串为：" + received);
        } catch (ClientProtocolException e) {
            PrintHandler.addLog(TAG, ">>TDefaultHttpClient get 失败，ClientProtocolException");
            WnsClientLog.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            PrintHandler.addLog(TAG, ">>TDefaultHttpClient get 失败，IOException");
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    private void getWithHttpUrlConnection(String fileName) {
        String httpUrl = getUrl(fileName);
        httpUrl += "?test=123456&test2=654321";

        TURLStreamHandler urlStreamHandler = new TURLStreamHandler();
        URL url = null;
        try {
            url = new URL(null, httpUrl, urlStreamHandler);
        } catch (MalformedURLException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }

        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(6 * 1000);
            PrintHandler.addLog(TAG, ">>发送Http Get请求，并收取响应：" + fileName);
            InputStream is = connection.getInputStream();

            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
            byte[] buf = new byte[1024];
            int len = -1;
            while ((len = is.read(buf)) != -1) {
                bos.write(buf, 0, len);
            }

            String received = new String(bos.toByteArray());
            PrintHandler.addLog(TAG, "收取响应完毕，收到字符串为：" + received);
        } catch (IOException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    private void postWithHttpClient(HttpClient httpClient, String fileName, boolean withParams) {
        String httpUrl = getUrl(fileName);

        if (withParams) {
            httpUrl += "?urlParam=123456";
        }

        try {
            PrintHandler.addLog(TAG, ">>发送Http Post请求,上传数据向" + httpUrl);
            HttpPost request = new HttpPost(httpUrl);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("test", "12345"));
            request.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
            PrintHandler.addLog(TAG, ">>设置参数 test = 12345");

            HttpResponse response = httpClient.execute(request);

            HttpEntity entity = response.getEntity();

            InputStream is = entity.getContent();
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
            byte[] buf = new byte[1024];
            int len = -1;
            while ((len = is.read(buf)) != -1) {
                bos.write(buf, 0, len);
            }
            String received = new String(bos.toByteArray());
            PrintHandler.addLog(TAG, "下载完毕，收到字符串为：" + received);
        } catch (ClientProtocolException e) {
            PrintHandler.addLog(TAG, ">>TDefaultHttpClient get 失败，ClientProtocolException");
            WnsClientLog.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            PrintHandler.addLog(TAG, ">>TDefaultHttpClient get 失败，IOException");
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    private void postWithHttpUrlConnection(String fileName, boolean withParams) {
        String httpUrl = getUrl(fileName);

        if (withParams) {
            httpUrl += "?urlParam=123456";
        }

        TURLStreamHandler urlStreamHandler = new TURLStreamHandler();
        URL url = null;
        try {
            url = new URL(null, httpUrl, urlStreamHandler);
        } catch (MalformedURLException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }

        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setUseCaches(false);

            PrintHandler.addLog(TAG, ">>发送Http Post请求,上传数据向" + httpUrl);
            OutputStream os = connection.getOutputStream();
            DataOutputStream out = new DataOutputStream(os);
            String content = "test=" + URLEncoder.encode("123456", "utf-8");
            out.writeBytes(content);
            out.flush();
            out.close();
            PrintHandler.addLog(TAG, ">>设置参数为test=123456");

            InputStream is = connection.getInputStream();
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
            byte[] buf = new byte[1024];
            int len = -1;
            while ((len = is.read(buf)) != -1) {
                bos.write(buf, 0, len);
            }

            String received = new String(bos.toByteArray());
            PrintHandler.addLog(TAG, "下载响应，收到字符串为：" + received);
        } catch (IOException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    private String getUrl(String fileName) {
        String domain = mItem.domain;
        int port = mItem.port;

        String httpUrl = "http://" + domain;
        if (port != 80) {
            httpUrl += ":" + port;
        }
        httpUrl += ("/" + fileName);

        return httpUrl;
    }
}
