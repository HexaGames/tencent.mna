/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk;

import com.tencent.cloudsdk.report.TSocketRecvStatistics;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.tsocket.TSocket;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.cloudsdk.utils.SpeedConfig;
import com.tencent.record.debug.WnsClientLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * @author 
 */
public class TcpTest extends AbsTest<MainActivity.RSDomainItem> {
    private static final String TAG = "TcpTest";

    private MainActivity.RSDomainItem mCurItem;
    private int mRound = 1;

    @Override
    public void setData(MainActivity.RSDomainItem data) {
        mCurItem = data;
    }

    @Override
    protected void beforeDoTest() {
        SpeedConfig.reset(GlobalContext.getContext(), mCurItem.domain);
        PrintHandler.clearLog();
        PrintHandler.addLog(TAG, "=======自动化测试TCP （第 " + (mRound++) + " 轮）=======");
    }

    @Override
    protected void postDoTest() {

    }

    @Override
    protected void postTestFinished() {
        PrintHandler.addLog(TAG, "=======自动化测试TCP 结束（共 " + (--mRound) + " 轮）=======");
    }

    @Override
    public boolean canRun() {
        return mCurItem.type == MainActivity.RSDomainItem.TYPE_FOR_TCP_TEST;
    }

    @Override
    public void doTest() {
        try {
            TSocket tSocket = new TSocket();
            tSocket.connect(new InetSocketAddress(mCurItem.domain, mCurItem.port));
            OutputStream os = tSocket.getOutputStream();
            InputStream in = tSocket.getInputStream();
            os.write(("GET / HTTP/1.1\r\n").getBytes());
            os.write(("HOST: " + mCurItem.domain + "\r\n").getBytes());
            os.write("User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31\r\n"
                    .getBytes());
            os.write("\r\n\r\n".getBytes());
            os.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line = br.readLine();
            WnsClientLog.i("Jie", ">>line=" + line);
            br.close();
            tSocket.close();

        } catch (UnknownHostException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }
}
