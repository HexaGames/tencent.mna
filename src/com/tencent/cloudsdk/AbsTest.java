/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * 测试加入停止，运行等状态
 *
 * @author 
 */
public abstract class AbsTest<T> implements Test<T> {
    protected HandlerThread mHandlerThread;
    private Handler mHandler;

    private HashMap<Integer, Boolean> mCancels = new HashMap<Integer, Boolean>();
    private int mMaxId;

    AbsTest() {
        mHandlerThread = new HandlerThread("AbsTest");
        mHandlerThread.start();
        mHandler = new TestHandler(this, mHandlerThread.getLooper());
    }

    @Override
    public void start() {
        stop();
        int id = mMaxId++;
        mCancels.put(id, false);
        Message msg = Message.obtain(mHandler, TestHandler.MSG_DO_TEST, id, 0);
        msg.sendToTarget();
    }

    @Override
    public void stop() {
        mCancels.clear();
        mHandler.removeMessages(TestHandler.MSG_DO_TEST);
    }

    abstract protected void beforeDoTest();

    abstract protected void postDoTest();

    abstract protected void postTestFinished();

    protected int getTimeInterval() {
        return 60000;
    }

    private static class TestHandler extends Handler {
        private WeakReference<AbsTest> mTestRef;
        public static final int MSG_DO_TEST = 1;

        private TestHandler(AbsTest absTest, Looper looper) {
            super(looper);
            this.mTestRef = new WeakReference<AbsTest>(absTest);
        }

        @Override
        public void handleMessage(Message msg) {
            AbsTest test = mTestRef.get();
            if (test == null) {
                return;
            }

            switch (msg.what) {
                case MSG_DO_TEST:
                    test.beforeDoTest();
                    test.doTest();
                    test.postDoTest();

                    Boolean cancel = (Boolean) test.mCancels.get(msg.arg1);
                    if (cancel != null && !cancel) {
                        Message message = Message.obtain(msg);
                        sendMessageDelayed(message, test.getTimeInterval());
                    } else {
                        test.postTestFinished();
                        test.mCancels.remove(msg.arg1);
                    }
                    break;
            }
        }
    }
}
