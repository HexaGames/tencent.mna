/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.tsocket;

//import android.util.Log;

import com.tencent.cloudsdk.utils.SpeedConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.Random;

public class TSocketMain {

    private static String[] domain = {"ocspeed2.tc.qq.com:80", "cmatjrstest.tc.qq.com:8080"};
    private static Random random = new Random();

    public static void test() {
        TSocket tSocket = null;
        try {

            String[] dm = getDomain().split(":");
            String sDomain = dm[0];
            int nPort = Integer.valueOf(dm[1]);

            SpeedConfig.reset(GlobalContext.getContext(), sDomain);

            //WnsClientLog.i("tsocket", "TESTING [" + sDomain + "][" + nPort + "]");
            tSocket = new TSocket();
            tSocket.connect(sDomain, nPort, 15000);
            tSocket.setSoTimeout(30000);
            OutputStream os = tSocket.getOutputStream();
            InputStream in = tSocket.getInputStream();

            PrintWriter pw = new PrintWriter(os);
            pw.println("0");
            pw.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line = br.readLine();
            //WnsClientLog.w("tsocket", ">>line="+line);
            br.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (tSocket != null && !tSocket.isClosed()) {
            try {
                tSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static String getDomain() {

        int index = random.nextInt(domain.length);
        if (index < domain.length) {
            return domain[index];
        }

        return domain[0];
    }
}
