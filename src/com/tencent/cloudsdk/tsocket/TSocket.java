/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.tsocket;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.ansquery.QueryLogic;
import com.tencent.cloudsdk.ansquery.QueryMain;
import com.tencent.cloudsdk.cache.MemoryCache;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.FailCountItem;
import com.tencent.cloudsdk.data.ListIpItem;
import com.tencent.cloudsdk.data.SingleIpItem;
import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.report.failreport.FailCountReport;
import com.tencent.cloudsdk.report.tcpstatistics.TcpStatisticsData;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.CommonAsynThread;
import com.tencent.cloudsdk.utils.FormatTransfer;
import com.tencent.cloudsdk.utils.IpUtils;
import com.tencent.cloudsdk.utils.NullReturnException;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.cloudsdk.utils.SpeedConfig;
import com.tencent.record.debug.WnsClientLog;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class TSocket {
    private final static String TAG = "TSocket";
    private Socket mSocket;
    private TSocketOutputStream mOutputStream;

    private String mDomain;
    private String mIp;
    private int mPort;
    private int mIpType;

    private static final Map<String, FailCountItem> sFailCountCache = Collections.synchronizedMap(new MemoryCache<String, FailCountItem>(256));

    // 注：正式时将此构造函数调整为protect，以防止外部调用
    protected TSocket(String domain, String rsIp, int rsPort, String ocIp, int ocPort, int timeout)
            throws IOException {
        if (GlobalContext.getContext() == null) {
            throw new IllegalArgumentException(
                    "You should call GlobalContext.setContext(ctx) first");
        }

        setDest(domain, ocIp, ocPort, IpUtils.TYPE_OC);
        mSocket = new Socket();
        // 默认将nodelay设置为true
        setNoDelay();
        ownConnect(ocIp, ocPort, timeout);
        sendTcpProtocolHeader(rsIp, rsPort);

    }

    private void setDest(String domain, String ip, int port, int ipType) {
        mDomain = domain;
        mIp = ip;
        mPort = port;
        mIpType = ipType;
    }

    /**
     * Creates a new unconnected socket.
     */
    public TSocket() {
        if (GlobalContext.getContext() == null) {
            throw new IllegalArgumentException(
                    "You should call GlobalContext.setContext(ctx) first");
        }

        setDest(null, null, -1, -1);
        mSocket = new Socket();
        // 默认将nodelay设置为true
        setNoDelay();
    }

    /**
     * Creates a new streaming socket connected to the target host specified by
     * the parameters domain and port.
     */
    public TSocket(String domain, int port) throws UnknownHostException, IOException {
        if (GlobalContext.getContext() == null) {
            throw new IllegalArgumentException(
                    "You should call GlobalContext.setContext(ctx) first");
        }

        setDest(domain.trim(), null, port, -1);
        mSocket = new Socket();
        // 默认将nodelay设置为true
        setNoDelay();

        if (IpUtils.isIpFormat(mDomain)) { // 如果是ip，则直连
            mIp = mDomain;
            //WnsClientLog.i(TAG, ">>>传的是Ip，进入直连通道");
            PrintHandler.addLog(TAG, "传的是Ip，进入直连通道");
            InetSocketAddress inetSocketAddress = new InetSocketAddress(mDomain, port);
            mSocket.connect(inetSocketAddress);
            //WnsClientLog.i(TAG, ">>>传的是Ip，连接完成");
            mOutputStream = new TSocketOutputStream(mSocket.getOutputStream(), mDomain, mIp, mIpType);
        } else {
            connect(mDomain, port);
        }
    }

    private void sendTcpProtocolHeader(String dstIp, int dstPort) throws IOException {
        byte[] header = new byte[16];
        header[0] = (byte) 0x80; // version
        header[1] = 0x20; // ttl
        header[2] = 0x00;// header length 占两字节
        header[3] = 0x10;// header length 占两字节
        byte[] ipBytes = IpUtils.ipv4Address2BinaryArray(dstIp);
        // 以下是dstIp
        header[4] = ipBytes[0];
        header[5] = ipBytes[1];
        header[6] = ipBytes[2];
        header[7] = ipBytes[3];
        byte[] portBytes = FormatTransfer.toHH((short) dstPort);
        header[8] = portBytes[0];
        header[9] = portBytes[1];
        // 以下是用户ip和端口，默认置0
        for (int i = 10; i < 16; i++) {
            header[i] = 0x00;
        }
        mOutputStream = new TSocketOutputStream(mSocket.getOutputStream(), mDomain, mIp, mIpType);
        DataOutputStream dos = new DataOutputStream(mOutputStream);
        dos.write(header);
        dos.flush();
    }

    /**
     * Connects this socket to the given domain and port with the specified
     * timeout. The connecting method will block until the connection is
     * established or an error occurred.
     *
     * @param domain  the domain of the remote host to connect to.
     * @param port    the port of the remote host to connect to.
     * @param timeout the timeout value in milliseconds or 0 for an infinite
     *                timeout.
     * @throws IllegalArgumentException if the given SocketAddress is invalid or
     *                                  not supported or the timeout value is negative.
     * @throws IOException              if the socket is already connected or an error occurs
     *                                  while connecting.
     */
    public void connect(String domain, int port, int timeout) throws IOException {
        setDest(domain, null, port, -1);
        try {
            //long startTime = System.currentTimeMillis();
            List<SingleIpItem> ipItems = IpUtils.getIpInfosFromDB(GlobalContext.getContext(),
                    domain, false);
            ListIpItem listIpItem = IpUtils.getAllIpInfos(ipItems);
            SingleIpItem firstItem = ipItems.get(0);

            if (firstItem.type == IpUtils.TYPE_OC) { // 如果获取到的第一个是oc节点，则进入加速通道
                mIp = firstItem.ip;
                mIpType = firstItem.type;

                PrintHandler.addLog(TAG, "选择的是OCIP\nIP:" + firstItem.ip + "\nport:" + firstItem.port
                        + "\nsp:" + firstItem.sp_type);
                ownConnect(firstItem.ip, firstItem.port, timeout);
                // 发送请求头，取同运营商下对应的源站ip
                SingleIpItem rsItem = getSameSpRsItem(listIpItem, firstItem);
                sendTcpProtocolHeader(rsItem.ip, port);
                // 调用测速线程
                SpeedThreadManager.getInstance().execute(domain, port, false);
            } else { // 如果不是oc节点，则通过查询到的ip直连
                if (firstItem.type == IpUtils.TYPE_RS_SPEED) { // 异常情况：如果第一个是测速站点，那也走直连
                    mIp = null;
                    mIpType = -1;

                    PrintHandler.addLog(TAG, "选择的是SpeedIp，直连域名：" + domain);
                    ownConnect(domain, port, timeout);
                } else if (firstItem.type == IpUtils.TYPE_RS) { // 如果第一个是源站，那就拿源站ip直接访问
                    mIp = firstItem.ip;
                    mIpType = firstItem.type;

                    PrintHandler.addLog(TAG, "选择的是RsIp\nIP:" + firstItem.ip + "\nport:" + port + "\nsp:"
                            + firstItem.sp_type);
                    ownConnect(firstItem.ip, port, timeout);
                } else {
                    mIp = null;
                    mIpType = -1;

                    PrintHandler.addLog(TAG, "从OC查询获取到的结果异常，走直连\nIP:" + firstItem.ip + "\nport:" + port
                            + "\nsp:"
                            + firstItem.sp_type + "\n");
                    ownConnect(domain, port, timeout);
                }
                // 调用测速线程
                SpeedThreadManager.getInstance().execute(domain, port, false);
            }

        } catch (NullReturnException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
            //WnsClientLog.i(TAG, ">>>从ans查询过程有异常，走域名直连");
            PrintHandler.addLog(TAG, "从ans查询过程有异常，走域名直连");
            ownConnect(domain, port, timeout);
        }
    }

    private SingleIpItem getSameSpRsItem(ListIpItem listIpItem, SingleIpItem ocItem)
            throws NullReturnException {
        int sp = ocItem.sp_type;
        List<SingleIpItem> rsItemList = IpUtils.getRsIpItemList(listIpItem);
        for (SingleIpItem singleIpItem : rsItemList) {
            if (singleIpItem.sp_type == sp) {
                //WnsClientLog.i(TAG, ">>>获取到了与OC相同运营商的RSIP");
                PrintHandler.addLog(TAG, "获取到了与OC相同运营商的RSIP\nip:" + singleIpItem.ip);
                return singleIpItem;
            }
        }
        //WnsClientLog.i(TAG, ">>>未获取到与OC相同运营商的RSIP");
        PrintHandler.addLog(TAG, "未获取到与OC相同运营商的RSIP\nip:" + rsItemList.get(0).ip);
        return rsItemList.get(0);
    }

    private void ownConnect(String domain, int port, int timeout) throws IOException {
        // 如果直接用域名连接，没有得到OC和RS
        boolean domainIsIp = IpUtils.isIpFormat(domain);

        try {
            InetSocketAddress inetSocketAddress = new InetSocketAddress(domain, port);
            mSocket.connect(inetSocketAddress, timeout);
            mOutputStream = new TSocketOutputStream(mSocket.getOutputStream(), mDomain, mIp, mIpType);
        } catch (IOException e) {
            if (domainIsIp) {
                int errCode = FailCountReport.ERR_CODE_OTHERS;
                if (e instanceof SocketTimeoutException) {
                    errCode = FailCountReport.ERR_CODE_TIMEOUT;
                }

                int networkType = Common.getNetworkType();
                if (networkType != AnsQueryConstants.NETWORK_TYPE_UNCONNECTED) {
                    PrintHandler.addLog(TAG, String.format("连接失败，ip = %s, port = %d, errCode = %d", domain, port, errCode));

                    final FailCountItem failCountItem = new FailCountItem();
                    failCountItem.ip = domain;
                    failCountItem.port = port;
                    failCountItem.time = System.currentTimeMillis();
                    failCountItem.proto = ReportConstanst.PROTO_TCP;
                    failCountItem.ipType = mIpType;
                    failCountItem.sp = Common.getSP();
                    failCountItem.network = networkType;
                    failCountItem.errCode = errCode;

                    increaseFailCount(failCountItem);
                    OcCacheManager.getInstance(GlobalContext.getContext()).increaseFailCount(failCountItem, 1);

                    int maxCount = AnsSetting.g().getMaxReportConnFailCount();
                    FailCountItem oldItem = getFailCountItem(domain, port, errCode);
                    if (oldItem.count >= maxCount) {
                        PrintHandler.addLog(TAG, String.format("连接连续失败超过%d次，清空该域名缓存，并重新查询, 并上报错误", maxCount));
                        handleReQuery(port);
                        handleConnectionFailReport(failCountItem);

                        sFailCountCache.remove(getFailCountCacheKey(domain, port, errCode));
                    }

                    SDKGlobal.getTCPStatisticData().statisticConn(mDomain, domain, mIpType, TcpStatisticsData.RET_FAIL);
                } else {
                    WnsClientLog.i(TAG, "handleConnectionFailReport:No network when connect, so no need handle this fail.");
                }
            }
            throw e;
        }

        SDKGlobal.getTCPStatisticData().statisticConn(mDomain, domain, mIpType, TcpStatisticsData.RET_SUCC);

        sFailCountCache.remove(getFailCountCacheKey(domain, port, FailCountReport.ERR_CODE_OTHERS));
        sFailCountCache.remove(getFailCountCacheKey(domain, port, FailCountReport.ERR_CODE_TIMEOUT));
    }

    private void handleConnectionFailReport(FailCountItem failCountItem) {
        final FailCountItem finalFailCountItem = (FailCountItem) failCountItem.clone();
        final int maxCount = AnsSetting.g().getMaxReportConnFailCount();
        finalFailCountItem.count = maxCount;
        CommonAsynThread.handler.post(new Runnable() {
            @Override
            public void run() {
                FailCountReport report = new FailCountReport();
                report.report(finalFailCountItem);
                OcCacheManager.getInstance(GlobalContext.getContext()).increaseFailCount(finalFailCountItem, -maxCount);
            }
        });
    }

    private void handleReQuery(int port) {
        OcCacheManager.getInstance(GlobalContext.getContext()).clearDomain(mDomain);
        SpeedConfig.reset(GlobalContext.getContext(), mDomain);

        QueryLogic ql = new QueryLogic(mDomain);
        final String domain = mDomain;
        final int finalPort = port;
        ql.setQueryMainListener(new QueryMain.QueryFinishListener() {
            @Override
            public void onFinish() {
                SpeedThreadManager.getInstance().execute(domain, finalPort, false);
            }
        });
        ql.ansQuery(false);
    }

    private String getFailCountCacheKey(String ip, int port, int errCode) {
        return ip + ":" + port + ":" + errCode;
    }

    private FailCountItem getFailCountItem(String ip, int port, int errCode) {
        return sFailCountCache.get(getFailCountCacheKey(ip, port, errCode));
    }

    private void increaseFailCount(FailCountItem item) {
        String key = getFailCountCacheKey(item.ip, item.port, item.errCode);
        FailCountItem oldItem = getFailCountItem(item.ip, item.port, item.errCode);
        if (oldItem == null) {
            FailCountItem newItem = (FailCountItem) item.clone();
            newItem.count = 1;
            sFailCountCache.put(key, newItem);
        } else {
            oldItem.count = oldItem.count + 1;
        }
    }

    /**
     * connected to the target host specified by the parameters domain and port
     *
     * @param domain the domain of the remote host to connect to.
     * @param port   the port of the remote host to connect to.
     * @throws IllegalArgumentException if the given SocketAddress is invalid or
     *                                  not supported or the timeout value is negative.
     * @throws IOException              if the socket is already connected or an error occurs
     *                                  while connecting.
     */
    public void connect(String domain, int port) throws IOException {
        connect(domain, port, 0);
    }

    /**
     * Connects this socket to the given remote host address and port specified
     * by the SocketAddress remoteAddr.
     *
     * @param remoteAddr the address and port of the remote host to connect to.
     * @throws IllegalArgumentException if the given SocketAddress is invalid or
     *                                  not supported or the timeout value is negative.
     * @throws IOException              if the socket is already connected or an error occurs
     *                                  while connecting.
     */
    public void connect(SocketAddress remoteAddr) throws IOException {
        connect(remoteAddr, 0);
    }

    /**
     * Connects this socket to the given remote host address and port specified
     * by the SocketAddress remoteAddr with the specified timeout. The
     * connecting method will block until the connection is established or an
     * error occurred.
     *
     * @param remoteAddr the address and port of the remote host to connect to.
     * @param timeout    the timeout value in milliseconds or 0 for an infinite
     *                   timeout.
     * @throws IllegalArgumentException if the given SocketAddress is invalid or
     *                                  not supported or the timeout value is negative.
     * @throws IOException              if the socket is already connected or an error occurs
     *                                  while connecting.
     */
    public void connect(SocketAddress remoteAddr, int timeout)
            throws IOException {
        if (!(remoteAddr instanceof InetSocketAddress)) {
            throw new IllegalArgumentException("Remote address not an InetSocketAddress: " +
                    remoteAddr.getClass());
        }
        InetSocketAddress inetSocketAddress = (InetSocketAddress) remoteAddr;
        mDomain = inetSocketAddress.getHostName();
        mPort = inetSocketAddress.getPort();
        mIp = null;
        mIpType = -1;

        if (IpUtils.isIpFormat(mDomain)) { // 如果是ip，则直连
            //WnsClientLog.i(TAG, ">>>传的是Ip，进入直连通道");
            PrintHandler.addLog(TAG, "传的是Ip，进入直连通道");
            mSocket.connect(inetSocketAddress, timeout);
            //WnsClientLog.i(TAG, ">>>传的是Ip，连接完成");
            mOutputStream = new TSocketOutputStream(mSocket.getOutputStream(), mDomain, mIp, mIpType);
        } else {
            connect(mDomain, mPort, timeout);
        }
    }

    /**
     * Binds this socket to the given local host address and port specified by
     * the SocketAddress localAddr. If localAddr is set to null, this socket
     * will be bound to an available local address on any free port.
     *
     * @param localAddr the specific address and port on the local machine to
     *                  bind to.
     * @throws IllegalArgumentException if the given SocketAddress is invalid or
     *                                  not supported.
     * @throws IOException              if the socket is already bound or an error occurs
     *                                  while binding.
     */
    public void bind(SocketAddress localAddr) throws IOException {
        mSocket.bind(localAddr);
    }

    /**
     * Returns this socket's SocketChannel, if one exists. A channel is
     * available only if this socket wraps a channel. (That is, you can go from
     * a channel to a socket and back again, but you can't go from an arbitrary
     * socket to a channel.) In practice, this means that the socket must have
     * been created by accept() or open().
     */
    public SocketChannel getChannel() {
        return mSocket.getChannel();
    }

    /**
     * Returns the IP address of the target host this socket is connected to, or
     * null if this socket is not yet connected.
     */
    public InetAddress getInetAddress() {
        if (mSocket == null || !mSocket.isConnected()) {
            return null;
        }
        try {
            if (IpUtils.isIpFormat(mDomain)) {
                InetAddress inetAddress = InetAddress.getByAddress(IpUtils
                        .ipv4Address2BinaryArray(mDomain));
                return inetAddress;
            } else {
                if (mIp == null) {
                    return InetAddress.getByName(mDomain);
                } else {
                    InetAddress inetAddress = InetAddress.getByAddress(mDomain,
                            IpUtils.ipv4Address2BinaryArray(mIp));
                    return inetAddress;
                }
            }
        } catch (UnknownHostException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Returns an input stream to read data from this socket.
     *
     * @return the byte-oriented input stream.
     * @throws IOException if an error occurs while creating the input stream or
     *                     the socket is in an invalid state.
     */
    public InputStream getInputStream() throws IOException {
        InputStream is = mSocket.getInputStream();
        return new TSocketInputStream(is, mDomain, mIp, mIpType);
    }

    /**
     * Returns this socket's SO_KEEPALIVE setting.
     *
     * @throws SocketException
     */
    public boolean getKeepAlive() throws SocketException {
        return mSocket.getKeepAlive();
    }

    /**
     * Returns the local IP address this socket is bound to, or InetAddress.ANY
     * if the socket is unbound.
     */
    public InetAddress getLocalAddress() {
        return mSocket.getLocalAddress();
    }

    /**
     * Returns the local port this socket is bound to, or -1 if the socket is
     * unbound.
     */
    public int getLocalPort() {
        return mSocket.getLocalPort();
    }

    /**
     * Returns the local address and port of this socket as a SocketAddress or
     * null if the socket is unbound. This is useful on multihomed hosts.
     */
    public SocketAddress getLocalSocketAddress() {
        return mSocket.getLocalSocketAddress();
    }

    /**
     * Returns this socket's SO_OOBINLINE setting.
     *
     * @throws SocketException
     */
    public boolean getOOBInline() throws SocketException {
        return mSocket.getOOBInline();
    }

    /**
     * Returns an output stream to write data into this socket.
     *
     * @return the byte-oriented output stream.
     * @throws IOException if an error occurs while creating the output stream
     *                     or the socket is in an invalid state.
     */
    public OutputStream getOutputStream() throws IOException {
        return mOutputStream;
    }

    /**
     * @return this socket's receive buffer size.
     * @throws SocketException
     */
    public synchronized int getReceiveBufferSize() throws SocketException {
        return mSocket.getReceiveBufferSize();
    }

    /**
     * Returns the port number of the target host this socket is connected to,
     * or 0 if this socket is not yet connected.
     */
    public int getPort() {
        if (mSocket != null && mSocket.isConnected()) {
            return mPort;
        }
        return 0;
    }

    /**
     * Returns the remote address and port of this socket as a SocketAddress or
     * null if the socket is not connected.
     *
     * @return the remote socket address and port.
     */
    public SocketAddress getRemoteSocketAddress() {
        if (mSocket == null || !mSocket.isConnected()) {
            return null;
        }
        SocketAddress socketAddress = new InetSocketAddress(mDomain, mPort);
        return socketAddress;
    }

    /**
     * Returns this socket's SO_REUSEADDR setting.
     *
     * @throws SocketException
     */
    public boolean getReuseAddress() throws SocketException {
        return mSocket.getReuseAddress();
    }

    /**
     * Returns this socket's send buffer size.
     *
     * @throws SocketException
     */
    public synchronized int getSendBufferSize() throws SocketException {
        return mSocket.getSendBufferSize();
    }

    /**
     * Returns this socket's linger timeout in seconds, or -1 for no linger
     * (i.e. close will return immediately).
     *
     * @throws SocketException
     */
    public int getSoLinger() throws SocketException {
        return mSocket.getSoLinger();
    }

    /**
     * Returns this socket's receive timeout.
     *
     * @throws SocketException
     */
    public synchronized int getSoTimeout() throws SocketException {
        return mSocket.getSoTimeout();
    }

    /**
     * Returns this socket's SocketOptions#TCP_NODELAY setting.
     *
     * @throws SocketException
     */
    public boolean getTcpNoDelay() throws SocketException {
        return mSocket.getTcpNoDelay();
    }

    /**
     * Closes the socket. It is not possible to reconnect or rebind to this
     * socket thereafter which means a new socket instance has to be created.
     *
     * @throws IOException if an error occurs while closing the socket.
     */
    public synchronized void close() throws IOException {
        mSocket.close();
    }

    /**
     * Returns this socket's setting.
     *
     * @throws SocketException
     */
    public int getTrafficClass() throws SocketException {
        return mSocket.getTrafficClass();
    }

    /**
     * Returns whether this socket is bound to a local address and port.
     *
     * @return true if the socket is bound to a local address, false otherwise.
     */
    public boolean isBound() {
        return mSocket.isBound();
    }

    /**
     * Returns whether this socket is closed.
     *
     * @return true if the socket is closed, false otherwise.
     */
    public boolean isClosed() {
        return mSocket.isClosed();
    }

    /**
     * Returns whether this socket is connected to a remote host.
     *
     * @return true if the socket is connected, false otherwise.
     */
    public boolean isConnected() {
        return mSocket.isConnected();
    }

    /**
     * Returns whether the incoming channel of the socket has already been
     * closed.
     *
     * @return true if reading from this socket is not possible anymore, false
     * otherwise.
     */
    public boolean isInputShutdown() {
        return mSocket.isInputShutdown();
    }

    /**
     * Returns whether the outgoing channel of the socket has already been
     * closed. Returns true if writing to this socket is not possible anymore,
     * false otherwise.
     */
    public boolean isOutputShutdown() {
        return mSocket.isOutputShutdown();
    }

    /**
     * Sends the given single byte data which is represented by the lowest octet
     * of value as "TCP urgent data".
     *
     * @param value the byte of urgent data to be sent.
     * @throws IOException if an error occurs while sending urgent data.
     */
    public void sendUrgentData(int value) throws IOException {
        mSocket.sendUrgentData(value);
    }

    /**
     * Sets this socket's SO_KEEPALIVE option.
     *
     * @throws SocketException
     */
    public void setKeepAlive(boolean keepAlive) throws SocketException {
        mSocket.setKeepAlive(keepAlive);
    }

    /**
     * Sets this socket's SO_OOBINLINE option.
     *
     * @throws SocketException
     */
    public void setOOBInline(boolean oobinline) throws SocketException {
        mSocket.setOOBInline(oobinline);
    }

    /**
     * Sets this socket's receive buffer size.
     *
     * @throws SocketException
     */
    public synchronized void setReceiveBufferSize(int size) throws SocketException {
        mSocket.setReceiveBufferSize(size);
    }

    /**
     * Sets this socket's SO_REUSEADDR option.
     *
     * @throws SocketException
     */
    public void setReuseAddress(boolean reuse) throws SocketException {
        mSocket.setReuseAddress(reuse);
    }

    /**
     * Sets this socket's send buffer size.
     *
     * @throws SocketException
     */
    public synchronized void setSendBufferSize(int size) throws SocketException {
        mSocket.setSendBufferSize(size);
    }

    /**
     * Sets this socket's linger timeout in seconds. If on is false, timeout is
     * irrelevant.
     *
     * @throws SocketException
     */
    public void setSoLinger(boolean on, int timeout) throws SocketException {
        mSocket.setSoLinger(on, timeout);
    }

    /**
     * Sets this socket's read timeout in milliseconds. Use 0 for no timeout. To
     * take effect, this option must be set before the blocking method was
     * called.
     *
     * @throws SocketException
     */
    public synchronized void setSoTimeout(int timeout) throws SocketException {
        mSocket.setSoTimeout(timeout);
    }

    /**
     * Sets this socket's TCP_NODELAY option.
     *
     * @throws SocketException
     */
    public void setTcpNoDelay(boolean on) throws SocketException {
        mSocket.setTcpNoDelay(on);
    }

    /**
     * Sets this socket's IP_TOS value for every packet sent by this socket.
     *
     * @throws SocketException
     */
    public void setTrafficClass(int value) throws SocketException {
        mSocket.setTrafficClass(value);
    }

    /**
     * Closes the input stream of this socket. Any further data sent to this
     * socket will be discarded. Reading from this socket after this method has
     * been called will return the value EOF.
     *
     * @throws IOException     if an error occurs while closing the socket input
     *                         stream.
     * @throws SocketException if the input stream is already closed.
     */
    public void shutdownInput() throws IOException {
        mSocket.shutdownInput();
    }

    /**
     * Closes the output stream of this socket. All buffered data will be sent
     * followed by the termination sequence. Writing to the closed output stream
     * will cause an IOException.
     *
     * @throws IOException     if an error occurs while closing the socket output
     *                         sztream.
     * @throws SocketException if the output stream is already closed.
     */
    public void shutdownOutput() throws IOException {
        mSocket.shutdownOutput();
    }

    private void setNoDelay() {
        try {
            mSocket.setTcpNoDelay(true);
        } catch (SocketException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

}
