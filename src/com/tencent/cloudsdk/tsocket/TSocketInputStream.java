/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.tsocket;

import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.tcpstatistics.TcpStatisticsData;

import java.io.IOException;
import java.io.InputStream;

/**
 * 为了统计方便，自定义的InputStream
 */
public class TSocketInputStream extends InputStream {
    public static final int MAX_RECV_BUFFER = 1024 * 1024;

    private InputStream mIs;
    private String mDomain;
    private String mIp;
    private int mIpType;

    public void setDomain(String domain) {
        this.mDomain = domain;
    }

    public void setIp(String ip) {
        this.mIp = ip;
    }

    public void setIpType(int ipType) {
        this.mIpType = ipType;
    }

    public TSocketInputStream(InputStream is, String domain, String ip, int ipType) {
        mIs = is;
        setDomain(domain);
        setIp(ip);
        setIpType(ipType);
    }

    @Override
    public int available() throws IOException {
        return mIs.available();
    }

    @Override
    public void close() throws IOException {
        mIs.close();
    }

    @Override
    public void mark(int readlimit) {
        mIs.mark(readlimit);
    }

    @Override
    public boolean markSupported() {
        return mIs.markSupported();
    }

    @Override
    public int read(byte[] buffer) throws IOException {
        long now = System.currentTimeMillis();
        int ret = 0;
        try {
            ret = mIs.read(buffer);
            if (ret >= 0 && ret < MAX_RECV_BUFFER) {
                int delay = (int) (System.currentTimeMillis() - now);
                SDKGlobal.getTCPStatisticData().statisticRecv(mDomain, mIp, mIpType, TcpStatisticsData.RET_SUCC, ret, delay);
            }
        } catch (IOException e) {
            int delay = (int) (System.currentTimeMillis() - now);
            SDKGlobal.getTCPStatisticData().statisticRecv(mDomain, mIp, mIpType, TcpStatisticsData.RET_FAIL, 0, delay);
            throw e;
        }

        return ret;
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {
        long now = System.currentTimeMillis();
        int ret = 0;
        try {
            ret = mIs.read(buffer, offset, length);
            if (ret >= 0 && ret < MAX_RECV_BUFFER) {
                int delay = (int) (System.currentTimeMillis() - now);
                SDKGlobal.getTCPStatisticData().statisticRecv(mDomain, mIp, mIpType, TcpStatisticsData.RET_SUCC, ret, delay);
            }
        } catch (IOException e) {
            int delay = (int) (System.currentTimeMillis() - now);
            SDKGlobal.getTCPStatisticData().statisticRecv(mDomain, mIp, mIpType, TcpStatisticsData.RET_FAIL, 0, delay);
            throw e;
        }

        return ret;
    }

    @Override
    public synchronized void reset() throws IOException {
        mIs.reset();
    }

    @Override
    public long skip(long byteCount) throws IOException {
        return mIs.skip(byteCount);
    }

    @Override
    public int read() throws IOException {
        long now = System.currentTimeMillis();
        int ret;
        try {
            ret = mIs.read();
            if (ret >= 0) {
                int delay = (int) (System.currentTimeMillis() - now);
                SDKGlobal.getTCPStatisticData().statisticRecv(mDomain, mIp, mIpType, TcpStatisticsData.RET_SUCC, ret, delay);
            }
        } catch (IOException e) {
            int delay = (int) (System.currentTimeMillis() - now);
            SDKGlobal.getTCPStatisticData().statisticRecv(mDomain, mIp, mIpType, TcpStatisticsData.RET_FAIL, 0, delay);
            throw e;
        }

        return ret;
    }
}
