/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.tsocket;

import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.cloudsdk.utils.SpeedConfig;

import java.util.HashMap;
import java.util.Map;

public class SpeedThreadManager {
    private final static String TAG = SpeedThreadManager.class.getSimpleName();
    private static SpeedThreadManager instance;

    private SpeedThreadManager() {
    }

    ;
    private Map<String, MutilSpeedThread> domainThreadMap = new HashMap<String, MutilSpeedThread>();

    public synchronized static SpeedThreadManager getInstance() {
        if (instance == null) {
            instance = new SpeedThreadManager();
        }
        return instance;
    }

    public synchronized void execute(String domain, int port, boolean isHttp) { // 调整为按网络类型+域名进行测速
        String key = Common.getKey(domain);
        if (domainThreadMap.containsKey(key)) {
            //WnsClientLog.i(Tag, ">>map contain this domain>>>");
            PrintHandler.addLog(TAG, ">>当前有测速线程在执行<<<");
            return;
        }
        if (SpeedConfig.isExpired(GlobalContext.getContext(), domain)) { // 检测是否过期，
            MutilSpeedThread speedThread = new MutilSpeedThread();
            domainThreadMap.put(key, speedThread);
            speedThread.execute(domain, port, isHttp);
        }
    }


//    public void remove(String domain){
//        String key = Common.getKey(domain);
//        if (domainThreadMap.containsKey(key)) {
//            domainThreadMap.remove(key);
//            WnsClientLog.i(Tag, ">>>map has remove this domain>>>");
//        }else {
//            WnsClientLog.i(Tag, ">>remove , map not contain this domain>>>");
//        }
//    }

    public void removeKey(String key) {
        if (domainThreadMap.containsKey(key)) {
            domainThreadMap.remove(key);
            //WnsClientLog.i(Tag, ">>>map has remove this domain>>>");
        } else {
            //WnsClientLog.i(Tag, ">>remove , map not contain this domain>>>");
        }
    }

    public void saveKey(String key, MutilSpeedThread thread) {
        domainThreadMap.put(key, thread);
    }

}
