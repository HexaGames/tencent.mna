/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.tsocket;

import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.tcpstatistics.TcpStatisticsData;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 为了方便统计，将OutputStream封装
 */
public class TSocketOutputStream extends OutputStream {
    private OutputStream mOs;
    private String mDomain;
    private String mIp;
    private int mIpType;

    public void setDomain(String domain) {
        this.mDomain = domain;
    }

    public void setIp(String ip) {
        this.mIp = ip;
    }

    public void setIpType(int ipType) {
        this.mIpType = ipType;
    }

    public TSocketOutputStream(OutputStream os, String domain, String ip, int ipType) {
        mOs = os;
        setDomain(domain);
        setIp(ip);
        setIpType(ipType);
    }

    @Override
    public void close() throws IOException {
        mOs.close();
    }

    @Override
    public void flush() throws IOException {
        mOs.flush();
    }

    @Override
    public void write(byte[] buffer) throws IOException {
        long now = System.currentTimeMillis();
        try {
            mOs.write(buffer);
        } catch (IOException e) {
            int delay = (int) (System.currentTimeMillis() - now);
            SDKGlobal.getTCPStatisticData().statisticSend(mDomain, mIp, mIpType, TcpStatisticsData.RET_FAIL, buffer.length, delay);
            throw e;
        }
        int delay = (int) (System.currentTimeMillis() - now);
        SDKGlobal.getTCPStatisticData().statisticSend(mDomain, mIp, mIpType, TcpStatisticsData.RET_SUCC, buffer.length, delay);
    }

    @Override
    public void write(byte[] buffer, int offset, int count) throws IOException {
        long now = System.currentTimeMillis();
        try {
            mOs.write(buffer, offset, count);
        } catch (IOException e) {
            int delay = (int) (System.currentTimeMillis() - now);
            SDKGlobal.getTCPStatisticData().statisticSend(mDomain, mIp, mIpType, TcpStatisticsData.RET_FAIL, count, delay);
            throw e;
        }
        int delay = (int) (System.currentTimeMillis() - now);
        SDKGlobal.getTCPStatisticData().statisticSend(mDomain, mIp, mIpType, TcpStatisticsData.RET_SUCC, count, delay);
    }

    @Override
    public void write(int oneByte) throws IOException {
        long now = System.currentTimeMillis();
        try {
            mOs.write(oneByte);
        } catch (IOException e) {
            int delay = (int) (System.currentTimeMillis() - now);
            SDKGlobal.getTCPStatisticData().statisticSend(mDomain, mIp, mIpType, TcpStatisticsData.RET_FAIL, 1, delay);
            throw e;
        }
        int delay = (int) (System.currentTimeMillis() - now);
        SDKGlobal.getTCPStatisticData().statisticSend(mDomain, mIp, mIpType, TcpStatisticsData.RET_SUCC, 1, delay);
    }
}
