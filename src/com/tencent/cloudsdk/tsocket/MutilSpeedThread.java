/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk.tsocket;

import android.os.Bundle;

import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.data.ListIpItem;
import com.tencent.cloudsdk.data.ReportItem;
import com.tencent.cloudsdk.data.SingleIpItem;
import com.tencent.cloudsdk.data.SpeedResult;
import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.utils.ClientIdManager;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.IpUtils;
import com.tencent.cloudsdk.utils.NullReturnException;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.cloudsdk.utils.SpeedConfig;
import com.tencent.record.debug.WnsClientLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class MutilSpeedThread {
    private final static String TAG = MutilSpeedThread.class.getSimpleName();
    private final static int MaxCount = 3;
    private final static int sleepTime = 3000;
    //private final static int SoTimeOut = 15000;

    private String mkey;
    private String mDomain;
    private int mPort;
    private HashMap<SingleIpItem, SpeedResult> ipStatusMap = new HashMap<SingleIpItem, SpeedResult>(); // 为避免ip相同，端口不同的情况，使用singleIpItem
    private List<SingleIpItem> ocList;
    private List<SingleIpItem> rsSpeedList;
    private List<SingleIpItem> rsList;
    private String orgFirstIp;
    /**
     * oc与rsSpeed映射关系
     */
    // private Map<SingleIpItem, SingleIpItem> speed_oc_map = new
    // HashMap<SingleIpItem, SingleIpItem>();
    private Map<SingleIpItem, SingleIpItem> oc_speed_map = new HashMap<SingleIpItem, SingleIpItem>();
    /**
     * speed与rs映射关系
     */
    private Map<SingleIpItem, SingleIpItem> speed_rs_map = new HashMap<SingleIpItem, SingleIpItem>();

    // private Map<String, String> speed_oc_ipMap = new HashMap<String,
    // String>(); // 存放测速ip对应的ocip映射
    private Map<String, String> speed_rs_ipMap = new HashMap<String, String>(); // 存放测速ip对应的源站ip映射
    private Map<String, Integer> speed_sp_map = new HashMap<String, Integer>(); // 存放ip对应的运营商映射

    private boolean isHttp;

    public synchronized void execute(String domain, int port, boolean isHttp) {
        mDomain = domain;
        mPort = port;
        this.isHttp = isHttp;
        mkey = Common.getKey(mDomain);
        SpeedThreadManager.getInstance().saveKey(mkey, this);
        //WnsClientLog.i(Tag, ">>>>connect timeout=" + SpeedConfig.getTimeout());
        // 判断当前域名下是否有测速线程
        Thread thread = new Thread(new SpeedRunnable());
        thread.start();
    }

    private class SpeedRunnable implements Runnable {
        public void run() {
            try {
                String data = "0";
                /*for(int i = 0; i < 1000;i++){
                    data += UUID.randomUUID().toString();
                }*/
                //WnsClientLog.i(Tag, "data=" + data);
                initParams(isHttp);

                //没有OC且只有一个源站时，不走测速
                if (ocList.size() == 0 && rsSpeedList.size() == 1) {
                    SpeedThreadManager.getInstance().removeKey(mkey);
                    return;
                }
                // 以下开启并发测速线程
                //WnsClientLog.i(Tag, ">>oc_speed_map.size:" + oc_speed_map.size());
                for (SingleIpItem ocItem : oc_speed_map.keySet()) {
                    SingleIpItem speedItem = oc_speed_map.get(ocItem);
                    int sp = ocItem.sp_type;
                    Thread ocThread = new Thread(new ConnectRunnable(sp, ocItem, speedItem, data)); // 加速通道测速线程
                    ocThread.start();
                }

                for (SingleIpItem speedItem : rsSpeedList) {
                    Thread rsThread = new Thread(new ConnectRunnable(speedItem.sp_type, null,
                            speedItem, data)); // 直连测速线程
                    rsThread.start();
                }
                if (oc_speed_map.size() + rsSpeedList.size() == 0) {
                    SpeedThreadManager.getInstance().removeKey(mkey);
                }
            } catch (NullReturnException e) {
                WnsClientLog.e(TAG, e.getMessage(), e);
                // 未能获取到ocip或rsip，均无法执行测速，不上报
                // 移除域名线程映射，以便以次可以查询
                SpeedThreadManager.getInstance().removeKey(mkey);
            }
        }
    }

    // 查询到所有oc和rsSpeed列表，并生成映射关系
    private void initParams(boolean isHttp) throws NullReturnException {
        List<SingleIpItem> ipItems = IpUtils.getIpInfosFromDB(GlobalContext.getContext(), mDomain, isHttp);
        orgFirstIp = ipItems.get(0).ip;
        ListIpItem listIpItem = IpUtils.getAllIpInfos(ipItems);
        ocList = listIpItem.ocItemList;
        rsSpeedList = listIpItem.rsSpeedItemList;
        rsList = listIpItem.rsItemList;
        for (SingleIpItem speedItem : rsSpeedList) {
            speed_sp_map.put(speedItem.ip, speedItem.sp_type);
            for (SingleIpItem rsIpItem : rsList) {
                if (speedItem.sp_type == rsIpItem.sp_type) {
                    speed_rs_map.put(speedItem, rsIpItem);
                    speed_rs_ipMap.put(speedItem.ip, rsIpItem.ip);
                }
            }
        }
        for (SingleIpItem ocIpItem : ocList) {
            for (SingleIpItem speedItem : rsSpeedList) {
                if (ocIpItem.sp_type == speedItem.sp_type) {
                    oc_speed_map.put(ocIpItem, speedItem);
                }
            }
            if (!oc_speed_map.containsKey(ocIpItem)) {
                oc_speed_map.put(ocIpItem, rsSpeedList.get(0));
            }
        }
    }

    private synchronized void doFinish(String domain) {
        // 保存本次测速时间
        SpeedConfig.saveLastTest(GlobalContext.getContext(), domain,
                System.currentTimeMillis());
        // 移除域名线程映射，以便以次可以查询
        SpeedThreadManager.getInstance().removeKey(mkey);
        if (ipStatusMap.size() == oc_speed_map.size() + rsSpeedList.size()) { // 如果已经全部测速完成
            // 刷新数据库中的记录
            updateFastIpFlag();
            // 对比相同运营商下的测速，合并上报
            mergeReport();
            // 清除本次记录
            // ipStatusMap.clear();
            clearAll();
        }
    }

    private void clearAll() {
        ipStatusMap.clear();
        oc_speed_map.clear();
        speed_rs_ipMap.clear();
        speed_rs_map.clear();
        speed_sp_map.clear();
    }

    /**
     * 合并相同运营商的数据进行上报
     */
    private void mergeReport() {
        SpeedResult speedResultItem = null;
        SpeedResult ocResultItem = null;
        ReportItem reportItem;
        for (SingleIpItem ocItem : oc_speed_map.keySet()) {
            for (SingleIpItem item : ipStatusMap.keySet()) {
                if (oc_speed_map.get(ocItem).ip.equals(ipStatusMap.get(item).speedIp)) {
                    if (ipStatusMap.get(item).ip_type == IpUtils.TYPE_OC) {
                        ocResultItem = ipStatusMap.get(item);
                    }
                    if (ipStatusMap.get(item).ip_type == IpUtils.TYPE_RS_SPEED) {
                        speedResultItem = ipStatusMap.get(item);
                    }
                }
            }
            if (ocResultItem != null && speedResultItem != null) {
                reportItem = ocResultItem.reportItem;
                reportItem.tc = speedResultItem.reportItem.tc;
                reportItem.ret = speedResultItem.reportItem.ret;
                reportItem.ctc = speedResultItem.reportItem.ctc;
                reportItem.stc = speedResultItem.reportItem.stc;
                reportItem.rtc = speedResultItem.reportItem.rtc;
                SDKGlobal.getReportTool().report(itemToBundle(reportItem));
            }
        }

        // 以下上报未能匹配到oc的speed数据
        SpeedResult tempItem;
        for (SingleIpItem item : ipStatusMap.keySet()) {
            tempItem = ipStatusMap.get(item);
            if (oc_speed_map.size() > 0) {
                boolean flag = true;
                for (SingleIpItem ocItem : oc_speed_map.keySet()) {
                    if (!tempItem.speedIp.equals(oc_speed_map.get(ocItem).ip)) {
                        flag = flag & true;
                    } else {
                        flag = flag & false;
                    }
                }
                if (flag) {
                    SDKGlobal.getReportTool().report(itemToBundle(tempItem.reportItem));
                }
            } else {
                SDKGlobal.getReportTool().report(itemToBundle(tempItem.reportItem));
            }
        }
    }

    private Bundle itemToBundle(ReportItem reportItem) {
        Bundle bundle = new Bundle();
        bundle.putInt(ReportConstanst.SPEED_TEST_TYPEID, ReportConstanst.TYPE_ID_SPEED_TEST);
        bundle.putLong(ReportConstanst.SPEED_TEST_RESOURCE_IP, reportItem.sip);
        bundle.putInt(ReportConstanst.SPEED_TEST_RESOURCE_PORT, reportItem.sport);
        bundle.putLong(ReportConstanst.SPEED_TEST_OC_IP, reportItem.ocip);
        bundle.putInt(ReportConstanst.SPEED_TEST_PKG, reportItem.pkg);
        bundle.putLong(ReportConstanst.SPEED_TEST_TC, reportItem.tc);
        bundle.putInt(ReportConstanst.SPEED_TEST_RET, reportItem.ret);
        bundle.putLong(ReportConstanst.SPEED_TEST_OCTC, reportItem.octc);
        bundle.putInt(ReportConstanst.SPEED_TEST_OCRET, reportItem.ocret);
        bundle.putInt(ReportConstanst.SPEED_TEST_NT, reportItem.nt);
        bundle.putInt(ReportConstanst.SPEED_TEST_SP, reportItem.sp);
        bundle.putInt(ReportConstanst.SPEED_TEST_OS, ReportConstanst.OS);
        bundle.putString(ReportConstanst.SPEED_TEST_OSV, android.os.Build.VERSION.RELEASE);
        bundle.putString(ReportConstanst.SPEED_TEST_SDK, ReportConstanst.SDK_VERSION);
        bundle.putLong(ReportConstanst.SPEED_TEST_CTC, reportItem.ctc);
        bundle.putLong(ReportConstanst.SPEED_TEST_OCCTC, reportItem.occtc);
        bundle.putLong(ReportConstanst.SPEED_TEST_STC, reportItem.stc);
        bundle.putLong(ReportConstanst.SPEED_TEST_OCSTC, reportItem.ocstc);
        bundle.putLong(ReportConstanst.SPEED_TEST_RTC, reportItem.rtc);
        bundle.putLong(ReportConstanst.SPEED_TEST_OCRTC, reportItem.ocrtc);
        bundle.putInt(ReportConstanst.SPEED_CLIENT_SEQ, ClientIdManager.getInstance().getClientId());
        bundle.putString(ReportConstanst.SPEED_DOMAIN, mDomain);
        showLog(reportItem);
        return bundle;
    }

    private void showLog(ReportItem reportItem) {
        StringBuilder sb = new StringBuilder();
        sb.append(">>>测速结果<<<\n");
        sb.append("RSIP:" + IpUtils.longToIP(reportItem.sip) + "\n");
        sb.append("OCIP:" + IpUtils.longToIP(reportItem.ocip) + "\n");
        sb.append("测速包大小:" + reportItem.pkg + "\n");
        sb.append("直连耗时:" + reportItem.tc + "\n");
        sb.append("OC加速耗时:" + reportItem.octc + "\n");
        sb.append("网络类型:" + reportItem.nt + "\n");
        sb.append("运营商类型:" + reportItem.sp + "\n");
        sb.append("系统版本:" + android.os.Build.VERSION.RELEASE + "\n");
        sb.append("直连建立连接时间:" + reportItem.ctc + "\n");
        sb.append("OC加速建立连接时间:" + reportItem.occtc + "\n");
        sb.append("直连发送耗时:" + reportItem.stc + "\n");
        sb.append("OC加速发送耗时:" + reportItem.ocstc + "\n");
        sb.append("直连接收耗时:" + reportItem.rtc + "\n");
        sb.append("OC加速接收耗时:" + reportItem.ocrtc + "\n");
        PrintHandler.addLog(TAG, sb.toString());
    }

    /**
     * 将最快的ip对应数据库中的标识刷新
     */
    private void updateFastIpFlag() {
        long minTime = Integer.MAX_VALUE;
        SpeedResult fastItem = null;
        SpeedResult tempItem;
        for (SingleIpItem item : ipStatusMap.keySet()) {
            tempItem = ipStatusMap.get(item);
            if (minTime > tempItem.avgTime) {
                minTime = tempItem.avgTime;
                fastItem = tempItem;
            }
        }
        if (fastItem == null || fastItem.errCount >= MaxCount) {
            //WnsClientLog.i(Tag, ">>>异常次数："+(fastItem!=null?fastItem.errCount:null));
            return;
        }
        String newFirstIp = fastItem.ip_type == IpUtils.TYPE_OC ? fastItem.ip : fastItem.rsIp;

        //TCP和HTTP的数据都更新
        IpUtils.updateFastFlag(mDomain, orgFirstIp, newFirstIp, true);
        IpUtils.updateFastFlag(mDomain, orgFirstIp, newFirstIp, false);
    }

    /*private void getAvgTime(Object socket, String data) throws IOException {
        if (!(socket instanceof Socket || socket instanceof TSocket)) {
            return;
        }
        OutputStream os = null;
        InputStream is = null;
        if (socket instanceof Socket) {
            os = ((Socket) socket).getOutputStream();
            is = ((Socket) socket).getInputStream();
        }
        if (socket instanceof TSocket) {
            os = ((TSocket) socket).getOutputStream();
            is = ((TSocket) socket).getInputStream();
        }
        PrintWriter pw = new PrintWriter(os);
        pw.println(data);
        pw.flush();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String result = reader.readLine();
        reader.close();
        //WnsClientLog.i(Tag, "echo result=" + result);
        PrintHandler.addLog(">>>echo:" + result);
        pw.close();
        os.close();
        is.close();
        if (socket instanceof Socket) {
            ((Socket) socket).close();
        }
        if (socket instanceof TSocket) {
            ((TSocket) socket).close();
        }
    }*/

    /**
     * 直连测速
     */
    private class ConnectRunnable implements Runnable {
        private String mRsSpeedIp;
        private int mRsSpeedPort;
        private String mOcIp;
        private int mOcPort;
        private String mData;
        private long tc;
        private int ret;
        private long ctc;
        private long stc;
        private long rtc;

        private long r_sip; // 源站ip
        private int r_sport; // 源站端口
        private long r_ocip; // ocip
        private int r_pkg;// 数据包大小,byte为单位
        private long r_tc; // 直连延时，等同于avgRsSpeedTime;
        private int r_ret;// 直连返回码
        private long r_octc; // oc加速延时，等同于avgOsTime;
        private int r_ocret;// oc加速返回码
        private int r_sp; // 运营商

        private long r_ctc; // 直连连接耗时
        private long r_occtc; // 加速连接耗时

        private long r_stc; // 直连发送耗时
        private long r_ocstc;// 加速发送耗时
        private long r_rtc;// 直接接收耗时
        private long r_ocrtc;// 加速接收耗时

        private static final int packetCount = 4;

        public ConnectRunnable(int sp_type, SingleIpItem ocItem, SingleIpItem speedItem, String data) {
            mRsSpeedIp = speedItem.ip;
            mRsSpeedPort = speedItem.port;
            mOcIp = ocItem == null ? "" : ocItem.ip;
            if (isHttp) {
                mOcPort = AnsSetting.g().getHttpSpeedTestPort();
            } else {
                mOcPort = ocItem == null ? 0 : ocItem.port;
            }

            mData = data;
            //WnsClientLog.i(Tag, "mRsSpeedip:" + mRsSpeedIp);
            r_sip = IpUtils.parseIpAddress(speed_rs_ipMap.get(mRsSpeedIp));
            r_sport = mPort;
            r_ocip = IpUtils.parseIpAddress(mOcIp);
            r_pkg = data.length() * packetCount;
            r_sp = sp_type;
        }

        public void run() {
            int count = 0;
            int errCount = 0;
            long timeTotalStart = 0, timeTotalEnd = 0;
            long timeConnectStart = 0, timeConnectEnd = 0;
            long timeSendStart = 0, timeSendEnd = 0;
            long timeRecvStart = 0, timeRecvEnd = 0;
            long totalTime = 0, totalConnectTime = 0, totalSendTime = 0, totalRecvTime = 0;
            while (count < MaxCount) {
                PrintHandler.addLog(TAG, ">>>正在测速第" + (count + 1) + "轮<<<");
                Object socket = null;
                try {
                    timeTotalStart = System.currentTimeMillis();
                    OutputStream os = null;
                    InputStream is = null;
                    /*------------------建立连接开始---------------------------*/
                    timeConnectStart = System.currentTimeMillis();
                    int timeout = SpeedConfig.getTimeout();
                    try {
                        if (r_ocip == 0) {
                            // socket = new Socket(mRsSpeedIp, mRsSpeedPort);
                            socket = new Socket();
                            PrintHandler.addLog(TAG, "### 源站测速连接超时时间:" + timeout);
                            ((Socket) socket).connect(new InetSocketAddress(mRsSpeedIp, mRsSpeedPort), timeout);
                            ((Socket) socket).setSoTimeout(timeout * 2);
                            ((Socket) socket).setTcpNoDelay(true);
                            PrintHandler.addLog(TAG, "### 源站测速接收超时时间:" + timeout * 2);
                        } else {
                            PrintHandler.addLog(TAG, "### OC测速连接超时时间:" + timeout);
                            socket = new TSocket(mDomain, mRsSpeedIp, mRsSpeedPort, mOcIp, mOcPort, timeout);
                            ((TSocket) socket).setSoTimeout(timeout * 2);
                            ((TSocket) socket).setTcpNoDelay(true);
                            PrintHandler.addLog(TAG, "### OC测速接收超时时间:" + timeout * 2);
                        }
                    } catch (IOException e) {
                        timeConnectEnd = System.currentTimeMillis();
                        PrintHandler.addLog(TAG, "### ERR 超时:" + e.getMessage());
                        if (e instanceof SocketTimeoutException) {
                            ret += ReportConstanst.REPORT_SOCKET_TIMEOUT;
                        } else {
                            ret += ReportConstanst.REPORT_CONNECT_ERR;
                        }
                        throw e;
                    }
                    timeConnectEnd = System.currentTimeMillis();

                    /*------------------建立连接结束---------------------------*/

                    /*------------------发送数据开始---------------------------*/
                    timeSendStart = System.currentTimeMillis();
                    try {
                        if (socket instanceof Socket) {
                            os = ((Socket) socket).getOutputStream();
                        }
                        if (socket instanceof TSocket) {
                            os = ((TSocket) socket).getOutputStream();
                        }
                    } catch (IOException e) {
                        timeSendEnd = System.currentTimeMillis();
                        ret += ReportConstanst.REPORT_SEND_ERR;
                        throw e;
                    }
                    PrintWriter pw = new PrintWriter(os);
                    for (int i = 0; i < packetCount; i++) {
                        pw.println(mData);
                        pw.flush();
                    }
                    timeSendEnd = System.currentTimeMillis();

                    /*------------------发送数据结束---------------------------*/

                    /*------------------接收数据开始---------------------------*/
                    timeRecvStart = System.currentTimeMillis();
                    try {
                        if (socket instanceof Socket) {
                            is = ((Socket) socket).getInputStream();
                        }
                        if (socket instanceof TSocket) {
                            is = ((TSocket) socket).getInputStream();
                        }
                    } catch (IOException e) {
                        timeRecvEnd = System.currentTimeMillis();
                        ret += ReportConstanst.REPORT_RECV_ERR;
                        throw e;
                    }
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    //String result;
                    try {
                        //循环去读所有的响应包
                        for (int i = 0; i < packetCount; i++) {
                            String result = reader.readLine();
                            if (!mData.equals(result)) {
                                WnsClientLog.i(TAG, String.format("echo result wrong, should be = %s,but is =%s", mData, result));
                                throw new IOException(String.format("echo result wrong, should be = %s,but is =%s", mData, result));
                            }
                        }
                        reader.close();
                    } catch (IOException e) {
                        timeRecvEnd = System.currentTimeMillis();
                        ret += ReportConstanst.REPORT_RECV_ERR;
                        //WnsClientLog.i(Tag, "读包错误 ERR=" + e.getMessage());
                        throw e;
                    }
                    timeRecvEnd = System.currentTimeMillis();

                    /*------------------接收数据结束---------------------------*/
                    pw.close();
                    try {
                        os.close();
                    } catch (IOException e) {
                        ret += ReportConstanst.REPORT_OS_CLOSE_ERR;
                        throw e;
                    }
                    try {
                        is.close();
                    } catch (IOException e) {
                        ret += ReportConstanst.REPORT_IS_CLOSE_ERR;
                        throw e;
                    }

                } catch (UnknownHostException e) {
                    // 修改直连异常上报标识，理论上是不会走到这一步的
                    WnsClientLog.e(TAG, e.getMessage(), e);
                    errCount = MaxCount;
                    break;
                } catch (IOException e) {
                    // 修改直连异常上报标识
                    WnsClientLog.e(TAG, e.getMessage(), e);
                    errCount = MaxCount;
                    break;
                } catch (Exception e) {
                    //其他错误
                    WnsClientLog.e(TAG, e.getMessage(), e);
                    errCount = MaxCount;
                    break;
                } finally {
                    timeTotalEnd = System.currentTimeMillis();
                    totalTime += (timeTotalEnd - timeTotalStart);
                    totalConnectTime += (timeConnectEnd - timeConnectStart);
                    totalSendTime += (timeSendEnd - timeSendStart);
                    totalRecvTime += (timeRecvEnd - timeRecvStart);
                    if (socket instanceof Socket) {
                        PrintHandler.addLog(TAG, "直连建立连接耗时：" + (timeConnectEnd - timeConnectStart)
                                + "\n" + "直连发送数据耗时：" + (timeSendEnd - timeSendStart) + "\n"
                                + "直连接收数据耗时：" + (timeRecvEnd - timeRecvStart) + "\n" + "直连总耗时："
                                + (timeTotalEnd - timeTotalStart));
                    }
                    if (socket instanceof TSocket) {
                        PrintHandler.addLog(TAG, "OC加速建立连接耗时：" + (timeConnectEnd - timeConnectStart)
                                + "\n" + "OC加速发送数据耗时：" + (timeSendEnd - timeSendStart) + "\n"
                                + "OC加速接收数据耗时：" + (timeRecvEnd - timeRecvStart) + "\n" + "OC加速总耗时："
                                + (timeTotalEnd - timeTotalStart));
                    }
                    if (socket != null) {
                        try {
                            if (socket instanceof Socket) {
                                ((Socket) socket).close();
                            }
                            if (socket instanceof TSocket) {
                                ((TSocket) socket).close();
                            }
                        } catch (IOException e) {
                            WnsClientLog.e(TAG, e.getMessage(), e);
                        }
                    }
                }


                count++;
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    WnsClientLog.e(TAG, e.getMessage(), e);
                }
            }
            if (count != 0) {
                tc = totalTime / count;
                ctc = totalConnectTime / count;
                stc = totalSendTime / count;
                rtc = totalRecvTime / count;
            }
            if (r_ocip == 0) {
                r_ret = ret;
                r_tc = tc;
                r_ctc = ctc;
                r_stc = stc;
                r_rtc = rtc;
            } else {
                r_ocret = ret;
                r_octc = tc;
                r_occtc = ctc;
                r_ocstc = stc;
                r_ocrtc = rtc;
            }

            ReportItem reportItem = new ReportItem();
            reportItem.ocip = r_ocip;
            reportItem.ocret = r_ocret;
            reportItem.octc = r_octc;
            reportItem.pkg = r_pkg;
            reportItem.ret = r_ret;
            reportItem.sip = r_sip;
            reportItem.sp = r_sp;
            reportItem.sport = r_sport;
            reportItem.tc = r_tc;
            reportItem.nt = Common.getNetworkType();
            reportItem.ctc = r_ctc;
            reportItem.occtc = r_occtc;
            reportItem.stc = r_stc;
            reportItem.ocstc = r_ocstc;
            reportItem.rtc = r_rtc;
            reportItem.ocrtc = r_ocrtc;

            SpeedResult speedResult = new SpeedResult();
            speedResult.avgTime = tc;
            speedResult.ip = (r_ocip == 0) ? mRsSpeedIp : mOcIp;
            speedResult.ip_type = (r_ocip == 0) ? IpUtils.TYPE_RS_SPEED : IpUtils.TYPE_OC;
            speedResult.reportItem = reportItem;
            speedResult.rsIp = speed_rs_ipMap.get(mRsSpeedIp);
            speedResult.speedIp = mRsSpeedIp;
            speedResult.errCount = errCount;

            SingleIpItem singleIpItem = new SingleIpItem();
            singleIpItem.ip = speedResult.ip;
            singleIpItem.type = (r_ocip == 0) ? IpUtils.TYPE_RS_SPEED : IpUtils.TYPE_OC;
            ipStatusMap.put(singleIpItem, speedResult);

            // doReport(reportItem);
            doFinish(mDomain);
        }
    }
}
