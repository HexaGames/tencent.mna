/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.data;

import com.tencent.cloudsdk.utils.FormatTransfer;

/**
 * address实体类
 *
 * @author 
 */
public class AddressEntity {

    /**
     * 数据流，限长为长度8 + 2，IP信息+端口号
     */
    private byte[] data;

    private String ip; //IP
    private short type; //类型
    private short sp; //运营商
    private short priority; //优先级
    private int port = 0;//端口号

    /**
     * 创建一个{@link AddressEntity}对象
     *
     * @param data 字节流，限长为8位字节
     */
    public AddressEntity(byte[] data) {
        this.data = data;

        parse();
    }

    /**
     * 创建一个{@link AddressEntity}对象
     *
     * @param ip       ip地址
     * @param type     ip类型
     * @param sp       运营商
     * @param priority 优先级
     */
    public AddressEntity(String ip, int port, short type, short sp, short priority) {
        this.ip = ip;
        this.port = port;
        this.type = type;
        this.sp = sp;
        this.priority = priority;
    }

    ////////////////////////////////

    public String getIp() {
        return ip;
    }

    public short getType() {
        return type;
    }

    public short getSp() {
        return sp;
    }

    public short getPriority() {
        return priority;
    }

    public int getPort() {
        return port;
    }

    ////////////////////////////////////////////

    /**
     * 解析响应包
     */
    private void parse() {

        /**解析IP*/
        StringBuilder ipBuilder = new StringBuilder();
        for (short i = 0; i < 4; i++) {
            ipBuilder.append(String.valueOf(FormatTransfer.byteToShort(data[i])));
            ipBuilder.append(".");
        }
        if (ipBuilder.length() > 0) {
            ipBuilder.deleteCharAt(ipBuilder.length() - 1);
        }
        this.ip = ipBuilder.toString();


        /**解析类型*/
        this.type = FormatTransfer.byteToShort(data[4]);

        /**解析运营商标识码*/
        this.sp = FormatTransfer.byteToShort(data[5]);

        /**解析优先级*/
        this.priority = FormatTransfer.byteToShort(data[6]);

        /**
         *data[7]为保留字节
         * */

        /**解析端口号*/
        this.port = FormatTransfer.bytesToU16(new byte[]{data[8], data[9]});
    }

}
