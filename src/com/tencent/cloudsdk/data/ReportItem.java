/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.data;


public class ReportItem {
    public long sip;  //源站ip
    public int sport; //源站端口
    public long ocip; // ocip
    public int pkg;  // 发包大小
    public long tc;  // 直连耗时
    public int ret;   // 直连返回码
    public long octc; // 加速耗时
    public int ocret;  // 加速返回码
    public int nt;  // 网络类型
    public int sp; // 运营商

    public long ctc;  // 直连连接耗时
    public long occtc; // 加速连接耗时

    public long stc; // 直连发送耗时
    public long ocstc; // 加速发送耗时

    public long rtc; // 直连接收耗时
    public long ocrtc; // 加速接收耗时


}
