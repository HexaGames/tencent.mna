/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.data;

import com.tencent.record.debug.WnsClientLog;

/**
 * @author 
 */
public class FailCountItem implements Cloneable {
    public String ip;
    public int port;
    public int errCode;
    public int count;
    public int ipType;
    public int network;
    public int sp;
    public int proto;
    public long time;

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            WnsClientLog.e("FailCountItem", e.getMessage(), e);
        }

        return null;
    }
}
