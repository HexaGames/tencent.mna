/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.data;

public class SpeedResult {
    public ReportItem reportItem;
    public long avgTime;
    public String ip; // 连接时的ip，可能是oc也可能是测速ip
    public int ip_type;// 标识上面的ip是oc还是测速ip
    public String rsIp;
    public String speedIp;

    public int errCount;// 测速时异常次数

}
