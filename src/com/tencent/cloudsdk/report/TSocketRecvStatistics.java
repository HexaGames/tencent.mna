/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.AddressEntity;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.httpstatistics.HttpStatisticsData;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.tsocket.TSocketInputStream;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.CommonAsynThread;
import com.tencent.cloudsdk.utils.IpUtils;

/**
 * 用于上报大于1M的TSocket收发包的统计信息 <br/>
 * 由于TCP收发的Buffer不能大于1M，因此，当收发包大于1M时，需要通过主动调用该类的方法达到统计目的。<br/>
 * 开放给SDK调用者。
 */
public class TSocketRecvStatistics {
    public static final int ERR_CODE_SUCC = HttpStatisticsData.RET_SUCC;
    public static final int ERR_CODE_FAIL = HttpStatisticsData.RET_FAIL;

    private long timeStart;
    private long delay;

    /**
     * 开始计时，用于计算延时。
     */
    public void timeStart() {
        timeStart = System.currentTimeMillis();
    }

    /**
     * 终止计时，计算出延时
     */
    public void timeEnd() {
        delay = System.currentTimeMillis() - timeStart;
    }

    /**
     * 上报统计数据
     */
    public void report(String domain, int errCode, long size) {
        if (domain == null || IpUtils.isIpFormat(domain) || size < TSocketInputStream.MAX_RECV_BUFFER) {
            return;
        }

        CommonAsynThread.handler.post(new ReportRunnable(domain, errCode, size));
    }

    private void reportInternal(String domain, int errCode, long size) {
        int networkType = Common.getNetworkType();
        int sp = AnsQueryConstants.E_MOBILE_UNKNOWN;
        if (networkType != AnsQueryConstants.NETWORK_TYPE_WIFI) {
            sp = Common.getSP();
        }

        OcAddress ocAddress = OcCacheManager.getInstance(GlobalContext.getContext()).get(domain, sp, false);
        if (ocAddress == null || ocAddress.addressList == null || ocAddress.addressList.size() == 0) {
            return;
        }

        AddressEntity address = ocAddress.addressList.get(0);
        String ip = address.getIp();

        SDKGlobal.getTCPStatisticData().statisticRecv(domain, ip, address.getType(), errCode, size, (int) delay);
    }

    private class ReportRunnable implements Runnable {
        private String mDomain;
        private int mErrCode;
        private long mSize;

        public ReportRunnable(String domain, int errCode, long size) {
            mDomain = domain;
            mErrCode = errCode;
            mSize = size;
        }

        @Override
        public void run() {
            reportInternal(mDomain, mErrCode, mSize);
        }
    }
}
