/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.httpstatistics;

import com.tencent.cloudsdk.cache.MemoryCache;
import com.tencent.cloudsdk.utils.IpUtils;
import com.tencent.cloudsdk.utils.PrintHandler;

import java.util.Collections;
import java.util.Map;

/**
 * HTTP统计数据的缓存
 */
public class HttpStatisticsData {
    private static final String TAG = "HttpStatisticsData";
    public static final int RET_FAIL = -1;
    public static final int RET_SUCC = 0;

    private Map<String, HttpStatisticsItem> mData = Collections.synchronizedMap(new MemoryCache<String, HttpStatisticsItem>(64));

    public void statisticRequest(String domain, String ip, int ipType, int ret, long size, long delay) {
        if (!checkParam(domain, ip, ipType)) {
            return;
        }

        HttpStatisticsItem item = insertItemIfNeed(domain, ip, ipType);
        item.time = System.currentTimeMillis();
        if (ret != RET_SUCC) {
            item.requestFailTimes++;
        }
        item.requestTimes++;

        if (size > 0 && size <= 1024) {
            if (ret != RET_SUCC) {
                item.request1KFailTimes++;
            } else {
                item.request1KTotalDelay = delay + item.request1KTotalDelay;
            }
            item.request1KTimes++;
        } else if (size > 1024 && size <= 4 * 1024) {
            if (ret != RET_SUCC) {
                item.request4KFailTimes++;
            } else {
                item.request4KTotalDelay = delay + item.request4KTotalDelay;
            }
            item.request4KTimes++;
        } else if (size > 4 * 1024 && size <= 10 * 1024) {
            if (ret != RET_SUCC) {
                item.request10KFailTimes++;
            } else {
                item.request10KTotalDelay = delay + item.request10KTotalDelay;
            }
            item.request10KTimes++;
        } else if (size > 10 * 1024 && size <= 1024 * 1024) {
            if (ret != RET_SUCC) {
                item.request1MFailTimes++;
            } else {
                item.request1MTotalDelay = delay + item.request1MTotalDelay;
            }
            item.request1MTimes++;
        } else {
            if (ret != RET_SUCC) {
                item.requestOtherFailTimes++;
            } else {
                item.requestOtherTotalDelay = delay + item.requestOtherTotalDelay;
            }
            item.requestOtherTimes++;
        }

        PrintHandler.addLog(TAG, "填充数据，最终" + domain+ "的数据为：" + item);
    }

    private HttpStatisticsItem insertItemIfNeed(String domain, String ip, int ipType) {
        String key = getKey(domain, ip);
        HttpStatisticsItem item = mData.get(key);
        if (item == null) {
            item = new HttpStatisticsItem();
            item.domain = domain;
            item.ip = IpUtils.parseIpAddress(ip);
            item.ipType = ipType;
            item.time = System.currentTimeMillis();
            mData.put(key, item);
        }

        return item;
    }

    public void clear() {
        mData.clear();
    }

    public String getKey(String domain, String ip) {
        return new StringBuilder(domain).append(ip).toString();
    }

    Map<String, HttpStatisticsItem> getData() {
        return mData;
    }

    private boolean checkParam(String domain, String ip, int ipType) {
        return domain != null
                && !domain.equals("")
                && !IpUtils.isIpFormat(domain)
                && ip != null
                && IpUtils.isIpFormat(ip)
                && ipType != -1;
    }
}
