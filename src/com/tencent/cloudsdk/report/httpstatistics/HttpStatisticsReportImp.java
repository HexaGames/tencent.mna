/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.httpstatistics;

import android.os.Build;
import android.os.Bundle;

import com.tencent.cloudsdk.report.imp.StatisticReportLimit;
import com.tencent.cloudsdk.report.imp.StatisticsReportConfig;
import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.report.core.ReportImp;
import com.tencent.cloudsdk.report.tool.ReportTool;
import com.tencent.cloudsdk.utils.PrintHandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 关于HTTP统计的上报的执行
 */
public class HttpStatisticsReportImp implements ReportImp {
    private static final String TAG = "HttpStatisticsReportImp";

    public static final String KEY_TYPE_ID = "typeid";
    public static final String TIME = "time";
    public static final String DEVICE_MODE = "dt";
    public static final String DOMAIN = "dm";
    public static final String DEVICE_TYPE = "mt";
    public static final String SYSTEM_VERSION = "sv";
    public static final String REPORT_METHOD = "rm";
    public static final String IP = "ip";
    public static final String IP_TYPE = "ipt";
    public static final String REQUEST_TIMES = "rt";
    public static final String REQUEST_FAIL_TIMES = "rf";

    public static final String REQUEST_1K_TIMES = "rt0";
    public static final String REQUEST_1K_FAIL_TIMES = "rf0";
    public static final String REQUEST_1K_DELAY = "rl0";
    public static final String REQUEST_4K_TIMES = "rt1";
    public static final String REQUEST_4K_FAIL_TIMES = "rf1";
    public static final String REQUEST_4K_DELAY = "rl1";
    public static final String REQUEST_10K_TIMES = "rt2";
    public static final String REQUEST_10K_FAIL_TIMES = "rf2";
    public static final String REQUEST_10K_DELAY = "rl2";
    public static final String REQUEST_1M_TIMES = "rt3";
    public static final String REQUEST_1M_FAIL_TIMES = "rf3";
    public static final String REQUEST_1M_DELAY = "rl3";
    public static final String REQUEST_OTHER_TIMES = "rt4";
    public static final String REQUEST_OTHER_FAIL_TIMES = "rf4";
    public static final String REQUEST_OTHER_DELAY = "rl4";

    public static final int REPORT_METHOD_ALL = 1;
    public static final int REPORT_METHOD_SIMPLE = 0;
    public static final int TYPE_ID = 1000201;

    private HttpStatisticsData mData;
    private StatisticsReportConfig mReportConfig;
    private ReportTool mReportTool;
    private StatisticReportLimit mReportLimit;

    public HttpStatisticsReportImp(HttpStatisticsData data, StatisticsReportConfig reportConfig, ReportTool reportTool) {
        mData = data;
        mReportConfig = reportConfig;
        mReportTool = reportTool;
        mReportLimit = new StatisticReportLimit();
    }

    @Override
    public void doOnNetworkInvalid() {
        mData.clear();
    }

    @Override
    public void report() {
        reportAll(false);
    }

    @Override
    public void reportSimply() {
        reportAll(true);
    }

    private void reportAll(boolean isSimple) {
        List<Bundle> bundles = new ArrayList<Bundle>();
        Map<String, HttpStatisticsItem> data = mData.getData();
        if (data == null || data.size() <= 0) {
            return;
        }
        // 虽然Map是线程同步的，但是仍然可能出现ConcurrentModificationException问题，这里复制一份。
        data = new LinkedHashMap<String, HttpStatisticsItem>(data);

        for (Map.Entry<String, HttpStatisticsItem> entry : data.entrySet()) {
            HttpStatisticsItem value = entry.getValue();
            long now = System.currentTimeMillis();
            if (now - value.time > mReportConfig.getClearInterval()) {
                continue;
            }

            Bundle bundle = createBundle(value, isSimple);
            bundles.add(bundle);
        }

        PrintHandler.addLog(TAG, "上报并清空HTTP统计数据：" + bundles);
        mData.clear();
        mReportTool.asynReport(bundles, mReportLimit);
    }

    private Bundle createBundle(HttpStatisticsItem value, boolean simple) {
        Bundle bundle = new Bundle(40);
        bundle.putInt(KEY_TYPE_ID, TYPE_ID);
        bundle.putLong(TIME, value.time / 1000);
        bundle.putString(DEVICE_MODE, Build.MODEL);
        bundle.putString(DOMAIN, value.domain);
        bundle.putInt(DEVICE_TYPE, ReportConstanst.OS);
        bundle.putString(SYSTEM_VERSION, android.os.Build.VERSION.RELEASE);
        bundle.putInt(REPORT_METHOD, simple ? REPORT_METHOD_SIMPLE : REPORT_METHOD_ALL);
        bundle.putLong(IP, value.ip);
        bundle.putInt(IP_TYPE, value.ipType);

        bundle.putInt(REQUEST_TIMES, value.requestTimes);
        bundle.putInt(REQUEST_FAIL_TIMES, value.requestFailTimes);

        if (!simple) {
            bundle.putInt(REQUEST_1K_TIMES, value.request1KTimes);
            bundle.putInt(REQUEST_1K_FAIL_TIMES, value.request1KFailTimes);
            int succCount = value.request1KTimes - value.request1KFailTimes;
            bundle.putLong(REQUEST_1K_DELAY, succCount <= 0 ? 0 : value.request1KTotalDelay / succCount);

            bundle.putInt(REQUEST_4K_TIMES, value.request4KTimes);
            bundle.putInt(REQUEST_4K_FAIL_TIMES, value.request4KFailTimes);
            succCount = value.request4KTimes - value.request4KFailTimes;
            bundle.putLong(REQUEST_4K_DELAY, succCount <= 0 ? 0 : value.request4KTotalDelay / succCount);

            bundle.putInt(REQUEST_10K_TIMES, value.request10KTimes);
            bundle.putInt(REQUEST_10K_FAIL_TIMES, value.request10KFailTimes);
            succCount = value.request10KTimes - value.request10KFailTimes;
            bundle.putLong(REQUEST_10K_DELAY, succCount <= 0 ? 0 : value.request10KTotalDelay / succCount);

            bundle.putInt(REQUEST_1M_TIMES, value.request1MTimes);
            bundle.putInt(REQUEST_1M_FAIL_TIMES, value.request1MFailTimes);
            succCount = value.request1MTimes - value.request1MFailTimes;
            bundle.putLong(REQUEST_1M_DELAY, succCount <= 0 ? 0 : value.request1MTotalDelay / succCount);

            bundle.putInt(REQUEST_OTHER_TIMES, value.requestOtherTimes);
            bundle.putInt(REQUEST_OTHER_FAIL_TIMES, value.requestOtherFailTimes);
            succCount = value.requestOtherTimes - value.requestOtherFailTimes;
            bundle.putLong(REQUEST_OTHER_DELAY, succCount <= 0 ? 0 : value.requestOtherTotalDelay / succCount);
        }

        return bundle;
    }
}
