/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.httpstatistics;

/**
 * HTTP统计上报的某一项
 */
public class HttpStatisticsItem {
    public String domain;
    public long ip;

    public int ipType;

    public long time;

    // 关于请求
    public int requestTimes;
    public int requestFailTimes;

    public int request1KTimes;
    public int request1KFailTimes;
    public long request1KTotalDelay;

    public int request4KTimes;
    public int request4KFailTimes;
    public long request4KTotalDelay;

    public int request10KTimes;
    public int request10KFailTimes;
    public long request10KTotalDelay;

    public int request1MTimes;
    public int request1MFailTimes;
    public long request1MTotalDelay;

    public int requestOtherTimes;
    public int requestOtherFailTimes;
    public long requestOtherTotalDelay;

    @Override
    public String toString() {
        return new StringBuilder("domain=").append(domain).append(",\n")
                .append("ip=").append(ip).append(",\n")
                .append("ipType=").append(ipType).append(",\n")
                .append("time=").append(time).append(",\n")
                .append("requestTimes=").append(requestTimes).append(",\n")
                .append("requestFailTimes=").append(requestFailTimes).append(",\n")
                .append("request1KTimes=").append(request1KTimes).append(",\n")
                .append("request1KFailTimes=").append(request1KFailTimes).append(",\n")
                .append("request1KTotalDelay=").append(request1KTotalDelay).append(",\n")
                .append("request4KTimes=").append(request4KTimes).append(",\n")
                .append("request4KFailTimes=").append(request4KFailTimes).append(",\n")
                .append("request4KTotalDelay=").append(request4KTotalDelay).append(",\n")
                .append("request10KTimes=").append(request10KTimes).append(",\n")
                .append("request10KFailTimes=").append(request10KFailTimes).append(",\n")
                .append("request10KTotalDelay=").append(request10KTotalDelay).append(",\n")
                .append("request1MTimes=").append(request1MTimes).append(",\n")
                .append("request1MFailTimes=").append(request1MFailTimes).append(",\n")
                .append("request1MTotalDelay=").append(request1MTotalDelay).append(",\n")
                .append("requestOtherTimes=").append(requestOtherTimes).append(",\n")
                .append("requestOtherFailTimes=").append(requestOtherFailTimes).append(",\n")
                .append("requestOtherTotalDelay=").append(requestOtherTotalDelay).toString();
    }
}
