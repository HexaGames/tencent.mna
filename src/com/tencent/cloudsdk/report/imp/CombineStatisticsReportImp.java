/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.imp;

import com.tencent.cloudsdk.report.core.ReportImp;
import com.tencent.cloudsdk.report.httpstatistics.HttpStatisticsData;
import com.tencent.cloudsdk.report.httpstatistics.HttpStatisticsReportImp;
import com.tencent.cloudsdk.report.tcpstatistics.TcpStatisticsData;
import com.tencent.cloudsdk.report.tcpstatistics.TcpStatisticsReportImp;
import com.tencent.cloudsdk.report.tool.ReportTool;

/**
 * 同时包含TCP和HTTP的统计上报执行
 */
public class CombineStatisticsReportImp implements ReportImp {
    private TcpStatisticsReportImp mTcpImp;
    private HttpStatisticsReportImp mHttpImp;

    public CombineStatisticsReportImp(TcpStatisticsData tcpData, HttpStatisticsData httpData, StatisticsReportConfig config, ReportTool tcpReportTool, ReportTool httpReportTool) {
        mTcpImp = new TcpStatisticsReportImp(tcpData, config, tcpReportTool);
        mHttpImp = new HttpStatisticsReportImp(httpData, config, httpReportTool);
    }

    @Override
    public void doOnNetworkInvalid() {
        mTcpImp.doOnNetworkInvalid();
        mHttpImp.doOnNetworkInvalid();
    }

    @Override
    public void report() {
        mTcpImp.report();
        mHttpImp.report();
    }

    @Override
    public void reportSimply() {
        mTcpImp.reportSimply();
        mHttpImp.reportSimply();
    }
}
