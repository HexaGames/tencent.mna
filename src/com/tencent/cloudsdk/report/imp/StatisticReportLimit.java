/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.imp;

import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.report.tool.ReportLimit;

/**
 * 上报的限制条件，这里主要限制了最大的字节数
 */
public class StatisticReportLimit implements ReportLimit {
    @Override
    public long batchSizeLimit() {
        return AnsSetting.g().getStatisticMaxBuffer();
    }
}
