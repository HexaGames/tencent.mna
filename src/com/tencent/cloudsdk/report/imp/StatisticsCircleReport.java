/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.imp;

import com.tencent.cloudsdk.report.core.AbsCircleReport;
import com.tencent.cloudsdk.report.core.ReportConfig;
import com.tencent.cloudsdk.report.core.ReportImp;
import com.tencent.cloudsdk.report.httpstatistics.HttpStatisticsData;
import com.tencent.cloudsdk.report.tcpstatistics.TcpStatisticsData;
import com.tencent.cloudsdk.report.tool.ReportTool;

/**
 * TCP统计的周期上报
 */
public class StatisticsCircleReport extends AbsCircleReport {
    private CombineStatisticsReportImp mCombineStatisticsReportImp;
    private StatisticsReportConfig mReportConfig;

    public StatisticsCircleReport(TcpStatisticsData data, HttpStatisticsData httpData, ReportTool tcpReportTool, ReportTool httpReportTool) {
        if (mReportConfig == null) {
            mReportConfig = new StatisticsReportConfig();
        }

        mCombineStatisticsReportImp = new CombineStatisticsReportImp(data, httpData, mReportConfig, tcpReportTool, httpReportTool);
    }

    public StatisticsReportConfig getReportConfig() {
        return mReportConfig;
    }

    @Override
    protected ReportImp createReportImp() {
        return mCombineStatisticsReportImp;
    }

    @Override
    protected ReportConfig createReportConfig() {
        if (mReportConfig == null) {
            mReportConfig = new StatisticsReportConfig();
        }

        return mReportConfig;
    }
}
