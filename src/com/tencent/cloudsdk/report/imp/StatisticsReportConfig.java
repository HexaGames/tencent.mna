/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.imp;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.report.core.ReportConfig;

/**
 * TCP统计的配置
 */
public class StatisticsReportConfig implements ReportConfig {
    private boolean onlyWifiEnabled = false;

    @Override
    public int getMod(int network) {
        int mod;
        if (isOnlyWifiEnabled()) {
            switch (network) {
                case AnsQueryConstants.NETWORK_TYPE_WIFI:
                    mod = MOD_ENABLE;
                    break;
                case AnsQueryConstants.NETWORK_TYPE_UNCONNECTED:
                case AnsQueryConstants.NETWORK_TYPE_UNKNOWN:
                case AnsQueryConstants.NETWORK_TYPE_4G:
                case AnsQueryConstants.NETWORK_TYPE_3G:
                case AnsQueryConstants.NETWORK_TYPE_2G:
                    mod = MOD_DISABLE;
                    break;
                default:
                    mod = MOD_DISABLE;
                    break;
            }
        } else {
            switch (network) {
                case AnsQueryConstants.NETWORK_TYPE_4G:
                case AnsQueryConstants.NETWORK_TYPE_3G:
                case AnsQueryConstants.NETWORK_TYPE_WIFI:
                case AnsQueryConstants.NETWORK_TYPE_UNKNOWN:
                    mod = MOD_ENABLE;
                    break;
                case AnsQueryConstants.NETWORK_TYPE_UNCONNECTED:
                    mod = MOD_DISABLE;
                    break;
                case AnsQueryConstants.NETWORK_TYPE_2G:
                    mod = MOD_SIMPLE;
                    break;
                default:
                    mod = MOD_ENABLE;
                    break;
            }
        }

        return mod;
    }

    @Override
    public long getInterval() {
        return AnsSetting.g().getStatisticInterval();
    }

    public void enableOnlyWifi(boolean enable) {
        onlyWifiEnabled = enable;
    }

    @Override
    public boolean isOnlyWifiEnabled() {
        return onlyWifiEnabled;
    }

    /**
     * 超过该时间的记录将被忽略而不上报
     */
    public long getClearInterval() {
        return AnsSetting.g().getStatisticClearBufInterval();
    }
}
