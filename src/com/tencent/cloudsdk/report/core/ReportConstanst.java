/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.core;

import com.tencent.cloudsdk.utils.DebugCfg;

/**
 * 定义上报的一些常量
 *
 * @author 
 */
public class ReportConstanst {

    /**
     * 上报系统IP/端口
     */
    public final static String REPORT_NAME = DebugCfg.REPORT_DEBUG ? "119.147.195.52" : "c.yun.qq.com";
    public final static int REPORT_PORT = DebugCfg.REPORT_DEBUG ? 80 : 8192;

//    public final static String REPORT_IP = "c.yun.qq.com";
//    public final static int REPORT_PORT = 8192;

    /**
     * 超时
     */
    public final static int TIMEOUT = 15 * 1000;

    /**
     * SDK版本
     */
    public final static String SDK_VERSION = "1.1.2";

    /**
     * 系统类型，android默认为1
     */
    public final static int OS = 1;

    /**
     * 上报的类型ID，由后台分配
     */
    public static final int TYPE_ID_SPEED_TEST = 1000193;
    public static final int TYPE_ID_HTTP_SPEED_TEST = 1000190;
    public static final int TYPE_ID_OC_QUERY = 1000157;

    public static final int PROTO_TCP = 0;
    public static final int PROTO_HTTP = 1;

    /////////////测速上报字段定义//////////// 
    public static final String SPEED_TEST_TYPEID = "typeid";
    /**
     * 源站IP
     */
    public static final String SPEED_TEST_RESOURCE_IP = "sip";
    /**
     * 源站端口
     */
    public static final String SPEED_TEST_RESOURCE_PORT = "sport";
    /**
     * OCIP
     */
    public static final String SPEED_TEST_OC_IP = "ocip";
    /**
     * 测速包大小
     */
    public static final String SPEED_TEST_PKG = "pkg";
    /**
     * 直连耗时
     */
    public static final String SPEED_TEST_TC = "tc";
    /**
     * 直连返回码
     */
    public static final String SPEED_TEST_RET = "ret";
    /**
     * oc加速耗时
     */
    public static final String SPEED_TEST_OCTC = "octc";
    /**
     * oc加速返回码
     */
    public static final String SPEED_TEST_OCRET = "ocret";
    /**
     * 网络类型
     */
    public static final String SPEED_TEST_NT = "nt";
    /**
     * 运营商
     */
    public static final String SPEED_TEST_SP = "sp";
    /**
     * 操作系统，android为1
     */
    public static final String SPEED_TEST_OS = "os";
    /**
     * 操作系统版本
     */
    public static final String SPEED_TEST_OSV = "osv";
    /**
     * SDK版本
     */
    public static final String SPEED_TEST_SDK = "sdkv";
    /**
     * 直连连接耗时
     */
    public static final String SPEED_TEST_CTC = "ctc";
    /**
     * oc连接耗时
     */
    public static final String SPEED_TEST_OCCTC = "occtc";
    /**
     * 直连发送耗时
     */
    public static final String SPEED_TEST_STC = "stc";
    /**
     * OC发送耗时
     */
    public static final String SPEED_TEST_OCSTC = "ocstc";
    /**
     * 直连读取耗时
     */
    public static final String SPEED_TEST_RTC = "rtc";
    /**
     * oc读取耗时
     */
    public static final String SPEED_TEST_OCRTC = "ocrtc";
    /**
     * 客户端标识序列号
     */
    public static final String SPEED_CLIENT_SEQ = "seq";
    /**
     * 连接的域名
     */
    public static final String SPEED_DOMAIN = "domain";


    // HTTP测速额外字段
    public static final String SPEED_UPLOAD_LENGTH = "uppkg";
    public static final String SPEED_DOWNLOAD_LENGTH = "downpkg";
    public static final String SPEED_METHOD = "method";
    public static final String SPEED_PROTO = "proto";

    public static final int METHOD_POST = 0;
    public static final int METHOD_GET = 1;

    /////////////OC查询上报字段定义////////////   


    /**
     * 类型ID
     */
    public static final String OC_QUERY_TYPEID = "typeid";
    /**
     * ANS查询返回码
     */
    public static final String OC_QUERY_RET = "ret";
    /**
     * ANS查询耗时
     */
    public static final String OC_QUERY_TC = "tc";
    /**
     * 域名
     */
    public static final String OC_QUERY_DM = "dm";
    /**
     * ANS查询server的IP
     */
    public static final String OC_QUERY_SERVER_IP = "aip";
    /**
     * 运营商
     */
    public static final String OC_QUERY_SP = "sp";
    /**
     * 网络类型
     */
    public static final String OC_QUERY_NETWORK_TYPE = "nt";
    /**
     * 操作系统
     */
    public static final String OC_QUERY_OS = "os";
    /**
     * 操作系统版本号
     */
    public static final String OC_QUERY_OS_VERSION = "osv";
    /**
     * SDK版本号
     */
    public static final String OC_QUERY_SDK_VERSION = "sdkv";
    /**
     * ANS协议版本号
     */
    public static final String OC_QUERY_ANS_VERSION = "apv";
    /**
     * 客户端IP
     */
    public static final String OC_QUERY_CLIENT_IP = "cip";
    /**
     * 上报时间戳
     */
    public static final String OC_QUERY_TIME = "time";
    /**
     * DNS解析耗时
     */
    public static final String DNS_QUERY_TIME = "dtc";
    /**
     * DNS解析结果
     */
    public static final String DNS_QUERY_STATUS = "dret";
    /**
     * 建立连接耗时
     */
    public static final String OC_QUERY_CONNECT_TIME = "ctc";
    /**
     * 发送耗时
     */
    public static final String OC_QUERY_SEND_TIME = "stc";
    /**
     * 接收耗时
     */
    public static final String OC_QUERY_RECV_TIME = "rtc";
    /**
     * 客户端标识序列号
     */
    public static final String OC_QUERY_CLIENT_SEQ = "seq";

    /**
     * 异常状态
     */
    public final static int REPORT_OK = 0;
    public final static int REPORT_UNKNOWHOST = 1;  // 理论上不会出现
    public final static int REPORT_CONNECT_ERR = 2;//连接异常
    public final static int REPORT_SEND_ERR = 4;//发送数据异常
    public final static int REPORT_RECV_ERR = 8;//接收数据异常
    public final static int REPORT_OS_ERR = 16;//创建OutPutStream异常
    public final static int REPORT_IS_ERR = 32; // 创建InputStream异常
    public final static int REPORT_OS_CLOSE_ERR = 64; //OutPutStream关闭异常
    public final static int REPORT_IS_CLOSE_ERR = 128; // InputStream关闭异常
    public final static int REPORT_SOCKET_CLOSE_ERR = 256; //socket关闭异常
    public final static int REPORT_RESPONSE_ERR = 512; //返回结果异常
    public final static int REPORT_SOCKET_TIMEOUT = 1024;
    public final static int REPORT_DEFAULT_IOEXCEPTION = 2048;
    public final static int REPORT_DEFAULT_OTHER_EXCEPTION = 4096;
    public final static int REPORT_DNS_ERR = 8192;// DNS解析失败
}
