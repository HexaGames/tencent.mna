/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.core;

import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.CommonAsynThread;
import com.tencent.cloudsdk.utils.PrintHandler;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 周期性的上报
 */
public abstract class AbsCircleReport implements CircleReport {
    private static final String TAG = "AbsCircleReport";
    private AtomicBoolean isCanceled = new AtomicBoolean(false);
    private Set<Runnable> mRunnables = Collections.synchronizedSet(new HashSet<Runnable>());
    private ReportConfig mReportConfig;

    public AbsCircleReport() {
        mReportConfig = createReportConfig();
    }

    /**
     * 开始周期上报
     */
    @Override
    public void startReport() {
        if (mRunnables.isEmpty()) {
            isCanceled.set(false);
            nextCircle();
        }
    }

    /**
     * 结束周期上报
     */
    @Override
    public void stopReport() {
        if (!mRunnables.isEmpty()) {
            isCanceled.set(true);
            for (Runnable runnable : mRunnables) {
                CommonAsynThread.handler.removeCallbacks(runnable);
            }
        }
    }

    @Override
    public long getInterval() {
        return mReportConfig.getInterval();
    }

    private void nextCircle() {
        long delay = getInterval();
        ReportRunnable runnable = new ReportRunnable();
        mRunnables.add(runnable);
        CommonAsynThread.handler.postDelayed(runnable, delay);
        PrintHandler.addLog(TAG, delay + "ms后开始下一次上报。");
    }

    private void reportDependsNetwork() {
        int network = Common.getNetworkType();
        int mod = mReportConfig.getMod(network);
        ReportImp reportImp = createReportImp();
        if (mod == ReportConfig.MOD_DISABLE) {
            PrintHandler.addLog(TAG, "网络不符合条件，不上报!");
            reportImp.doOnNetworkInvalid();
        } else if (mod == ReportConfig.MOD_ENABLE) {
            PrintHandler.addLog(TAG, "开始全量上报!");
            reportImp.report();
        } else if (mod == ReportConfig.MOD_SIMPLE) {
            PrintHandler.addLog(TAG, "开始简单上报!");
            reportImp.reportSimply();
        }
    }

    private class ReportRunnable implements Runnable {
        @Override
        public void run() {
            PrintHandler.addLog(TAG, "周期统计上报时间到，开始上报。");
            reportDependsNetwork();

            if (!isCanceled.get()) {
                nextCircle();
            }
            mRunnables.remove(this);
        }
    }

    protected abstract ReportImp createReportImp();

    protected abstract ReportConfig createReportConfig();
}
