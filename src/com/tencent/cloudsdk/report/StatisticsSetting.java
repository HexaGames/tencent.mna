/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report;

import com.tencent.cloudsdk.global.SDKGlobal;

/**
 * 上报统计的设置 <br/>
 * 开放给SDK调用者
 */
public class StatisticsSetting {
    /**
     * 设置只在Wifi情况下才可以上报统计
     *
     * @param enabled 为true时，只在wifi下上报统计。
     */
    public static void enableOnlyWifi(boolean enabled) {
        SDKGlobal.getReportConfig().enableOnlyWifi(enabled);
    }
}
