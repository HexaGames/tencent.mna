/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.AddressEntity;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.httpstatistics.HttpStatisticsData;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.CommonAsynThread;
import com.tencent.record.debug.WnsClientLog;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * 开放给SDK调用者，通过主动调用获取HTTP统计数据
 */
public class HttpStatistics {
    public static final int ERR_CODE_SUCC = HttpStatisticsData.RET_SUCC;
    public static final int ERR_CODE_FAIL = HttpStatisticsData.RET_FAIL;

    private static final String TAG = "HttpStatistics";

    private long timeStart;
    private long delay;

    /**
     * 开始计时，用于计算延时
     */
    public void timeStart() {
        timeStart = System.currentTimeMillis();
    }

    /**
     * 结束计时，计算出延时
     */
    public void timeEnd() {
        delay = System.currentTimeMillis() - timeStart;
    }

    /**
     * 上报统计数据
     */
    public void report(String originUrl, int errCode, long size) {
        CommonAsynThread.handler.post(new ReportRunnable(originUrl, errCode, size));
    }

    private void reportInternal(String originUrl, int errCode, long size) {
        HttpStatisticsData data = SDKGlobal.getHttpStatisticsData();
        String domain;
        try {
            URL url = new URL(originUrl);
            domain = url.getHost();
        } catch (MalformedURLException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
            return;
        }

        int networkType = Common.getNetworkType();
        int sp = AnsQueryConstants.E_MOBILE_UNKNOWN;
        if (networkType != AnsQueryConstants.NETWORK_TYPE_WIFI) {
            sp = Common.getSP();
        }

        OcAddress ocAddress = OcCacheManager.getInstance(GlobalContext.getContext()).get(domain, sp, true);
        if (ocAddress == null || ocAddress.addressList == null || ocAddress.addressList.size() == 0) {
            return;
        }

        AddressEntity address = ocAddress.addressList.get(0);
        String ip = address.getIp();

        data.statisticRequest(domain, ip, address.getType(), errCode, size, delay);
    }

    private class ReportRunnable implements Runnable {
        private String mOriginUrl;
        private int mErrCode;
        private long mSize;

        public ReportRunnable(String originUrl, int errCode, long size) {
            mOriginUrl = originUrl;
            mErrCode = errCode;
            mSize = size;
        }

        @Override
        public void run() {
            reportInternal(mOriginUrl, mErrCode, mSize);
        }
    }
}
