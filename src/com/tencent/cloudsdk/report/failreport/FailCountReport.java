/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.failreport;

import android.os.Bundle;

import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.FailCountItem;
import com.tencent.cloudsdk.global.SDKGlobal;
import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.ClientIdManager;
import com.tencent.cloudsdk.utils.CommonAsynThread;
import com.tencent.cloudsdk.utils.IpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 连接失败次数上报
 * <p/>
 * TODO 从AbsCircleReport中继承
 *
 * @author 
 */
public class FailCountReport {
    private static final String TAG = "FailCountReport";
    private static AtomicBoolean sIsRunning = new AtomicBoolean(false);

    private static final int TYPE_ID = 1000184;

    // 超时的错误码和IOS一致，由于IOS的错误码更多，这里定一个-1
    public static final int ERR_CODE_TIMEOUT = 102;
    public static final int ERR_CODE_OTHERS = -1;

    private static final String KEY_TYPE_ID = "typeid";
    private static final String KEY_SDK_VERSION = "sdkv";
    private static final String KEY_CLIENT_IP = "cip";
    private static final String KEY_DESTINATION_IP = "dip";
    private static final String KEY_DESTINATION_PORT = "dport";
    private static final String KEY_DESTINATION_TYPE = "dtype";
    private static final String KEY_NETWORK_TYPE = "nt";
    private static final String KEY_SP = "sp";
    private static final String KEY_OS = "os";
    private static final String KEY_OS_VERSION = "osv";
    private static final String KEY_ERROR_CODE = "ec";
    private static final String KEY_ERROR_COUNT = "cnt";
    private static final String KEY_PROTOCOL = "proto";
    private static final String KEY_TIME = "time";
    private static final String KEY_CLIENT_SEQ = "seq";

    public static void startReport() {
        if (sIsRunning.compareAndSet(false, true)) {
            reportCycle();
        }
    }

    static void reportCycle() {
        long delay = AnsSetting.g().getMaxReportConnFailInterval();
        ReportRunnable runnable = new ReportRunnable();
        CommonAsynThread.handler.postDelayed(runnable, delay);
    }

    public void report(FailCountItem item) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_TYPE_ID, TYPE_ID);
        bundle.putString(KEY_SDK_VERSION, ReportConstanst.SDK_VERSION);
        bundle.putLong(KEY_DESTINATION_IP, IpUtils.parseIpAddress(item.ip));
        bundle.putInt(KEY_DESTINATION_PORT, item.port);
        bundle.putInt(KEY_DESTINATION_TYPE, item.ipType);
        bundle.putInt(KEY_NETWORK_TYPE, item.network);
        bundle.putInt(KEY_SP, item.sp);
        bundle.putInt(KEY_OS, ReportConstanst.OS);
        bundle.putString(KEY_OS_VERSION, android.os.Build.VERSION.RELEASE);
        bundle.putInt(KEY_ERROR_COUNT, item.count);
        bundle.putInt(KEY_ERROR_CODE, item.errCode);
        bundle.putInt(KEY_PROTOCOL, item.proto);
        bundle.putInt(KEY_CLIENT_SEQ, ClientIdManager.getInstance().getClientId());

        List<Bundle> bundles = new ArrayList<Bundle>();
        bundles.add(bundle);
        SDKGlobal.getReportTool().asynReport(bundles);
    }

    private static class ReportRunnable implements Runnable {

        @Override
        public void run() {
            FailCountReport.sIsRunning.set(false);
            FailCountReport report = new FailCountReport();
            List<FailCountItem> items = OcCacheManager.getInstance(GlobalContext.getContext()).getFailCountItemsNeedReportAndClear();
            for (FailCountItem item : items) {
                report.report(item);
            }

            reportCycle();
        }
    }
}
