/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.tool;

import android.os.Bundle;

import com.tencent.cloudsdk.utils.FormatTransfer;
import com.tencent.record.debug.WnsClientLog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

/**
 * 压缩，堆叠的上报方式
 */
public class ZipReportTool extends AbsReportTool {
    private static final String TAG = "ZipReportTool";

    @Override
    protected byte[] pkgFrame(Bundle bundle) {
        String data = pkgBundle(bundle);
        byte[] zips = gzipString(data);
        zips = addLengthAndGzipFlag(zips);
        return zips;
    }

    @Override
    protected byte[] pkgBatchFrame(List<Bundle> bundles, ReportLimit limit) {
        String tmp = null;
        String unZipStr = null;
        boolean zipError = false;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        GZIPOutputStream gzipOutputStream = null;
        try {
            gzipOutputStream = new GZIPOutputStream(bos);
        } catch (IOException e) {
            zipError = true;
            WnsClientLog.e(TAG, e.getMessage(), e);
        }

        for (Bundle bundle : bundles) {
            String data = pkgBundle(bundle);
            if (tmp == null) {
                tmp = data;
                unZipStr = data;
            } else {
                tmp = "\n" + data;
                unZipStr = unZipStr + "\n" + data;
            }

            if (!zipError) {
                try {
                    gzipOutputStream.write(tmp.getBytes());
                    gzipOutputStream.flush();
                } catch (IOException e) {
                    zipError = true;
                    WnsClientLog.e(TAG, e.getMessage(), e);
                }
            }

            if (limit != null) {
                if (zipError && unZipStr.getBytes().length > limit.batchSizeLimit()) {
                    break;
                } else if (!zipError && bos.toByteArray().length > limit.batchSizeLimit()) {
                    break;
                }
            }
        }

        try {
            if (gzipOutputStream != null) {
                gzipOutputStream.close();
            }
        } catch (IOException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }

        if (zipError) {
            return addLength(unZipStr.getBytes());
        } else {
            return addLengthAndGzipFlag(bos.toByteArray());
        }
    }

    @Override
    protected boolean supportBatch() {
        return true;
    }

    private String pkgBundle(Bundle bundle) {
        StringBuilder sBuilder = new StringBuilder();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            for (String key : keys) {
                if (sBuilder.length() == 0) {
                    sBuilder.append(key + "=" + bundle.get(key));
                } else {
                    sBuilder.append("&" + key + "=" + bundle.get(key));
                }
            }
        }

        return sBuilder.toString();
    }

    private byte[] gzipString(String data) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(bos);
            gzipOutputStream.write(data.getBytes());
            gzipOutputStream.close();
        } catch (IOException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }

        return bos.toByteArray();
    }

    private byte[] addLength(byte[] data) {
        byte[] length = FormatTransfer.toHH((short) (data.length));
        ByteBuffer buffer = ByteBuffer.allocate(2 + data.length);
        buffer.put(length);
        buffer.put(data);
        return buffer.array();
    }

    private byte[] addLengthAndGzipFlag(byte[] data) {
        short dataLen = (short) (data.length + 5);
        byte[] length = FormatTransfer.toHH(dataLen);
        ByteBuffer buffer = ByteBuffer.allocate(2 + dataLen);
        buffer.put(length);
        buffer.put("gzip\0".getBytes());
        buffer.put(data);
        return buffer.array();
    }
}
