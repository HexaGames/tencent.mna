/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.tool;

import android.os.Bundle;

import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.utils.CommonAsynThread;
import com.tencent.record.debug.WnsClientLog;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;


public abstract class AbsReportTool implements ReportTool {
    private static final String TAG = "AbsReportTool";

    @Override
    public void report(Bundle bundle) {
        if (bundle == null) {
            return;
        }

        byte[] byteArray = pkgFrame(bundle);
        transferReport(byteArray);
    }

    @Override
    public void batchReport(List<Bundle> bundles, ReportLimit limit) {
        if (supportBatch()) {
            byte[] byteArray = pkgBatchFrame(bundles, limit);
            transferReport(byteArray);
        } else {
            for (Bundle bundle : bundles) {
                report(bundle);
            }
        }
    }

    @Override
    public void batchReport(List<Bundle> bundles) {
        batchReport(bundles, null);
    }

    @Override
    public void asynReport(List<Bundle> bundles, ReportLimit limit) {
        CommonAsynThread.handler.post(new AsynReportRunnable(bundles, limit));
    }

    @Override
    public void asynReport(List<Bundle> bundles) {
        asynReport(bundles, null);
    }

    private class AsynReportRunnable implements Runnable {
        private List<Bundle> bundles;
        private ReportLimit limit;

        public AsynReportRunnable(List<Bundle> bundles, ReportLimit limit) {
            this.bundles = bundles;
            this.limit = limit;
        }

        @Override
        public void run() {
            batchReport(bundles, limit);
        }
    }

    protected void transferReport(byte[] byteArray) {
        try {
            Socket socket = new Socket(ReportConstanst.REPORT_NAME, ReportConstanst.REPORT_PORT);
            socket.setSoTimeout(ReportConstanst.TIMEOUT);
            OutputStream os = socket.getOutputStream();
            InputStream is = socket.getInputStream();

            DataOutputStream dos = new DataOutputStream(os);
            dos.write(byteArray);
            dos.flush();

            byte[] response = new byte[2];
            is.read(response);

            dos.close();
            os.close();
            socket.close();
        } catch (UnknownHostException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            WnsClientLog.e(TAG, e.getMessage(), e);
        }
    }

    protected abstract byte[] pkgFrame(Bundle bundle);

    protected abstract byte[] pkgBatchFrame(List<Bundle> bundles, ReportLimit limit);

    protected abstract boolean supportBatch();
}
