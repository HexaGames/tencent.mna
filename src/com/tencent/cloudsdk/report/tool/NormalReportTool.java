/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.tool;

import android.os.Bundle;

import com.tencent.cloudsdk.utils.FormatTransfer;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Set;

/**
 * 字符串无压缩的上报工具
 */
public class NormalReportTool extends AbsReportTool {
    private static final String TAG = "NormalReportTool";

    @Override
    protected byte[] pkgFrame(Bundle bundle) {
        String data = pkgBundle(bundle);

        byte[] length = FormatTransfer.toHH((short) (data.length()));
        ByteBuffer buffer = ByteBuffer.allocate(2 + data.getBytes().length);
        buffer.put(length);
        buffer.put(data.getBytes());
        return buffer.array();
    }

    @Override
    protected byte[] pkgBatchFrame(List<Bundle> bundles, ReportLimit limit) {
        throw new RuntimeException("pkgBatchFrame not supported!");
    }

    @Override
    protected boolean supportBatch() {
        return false;
    }

    private String pkgBundle(Bundle bundle) {
        StringBuilder sBuilder = new StringBuilder();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            for (String key : keys) {
                if (sBuilder.length() == 0) {
                    sBuilder.append(key + "=" + bundle.get(key));
                } else {
                    sBuilder.append("&" + key + "=" + bundle.get(key));
                }
            }
        }

        return sBuilder.toString();
    }
}
