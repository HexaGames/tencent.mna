/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.tool;

import android.os.Bundle;

import java.util.List;

/**
 * 上报的工具
 */
public interface ReportTool {
    void report(Bundle bundle);

    void batchReport(List<Bundle> bundles, ReportLimit limit);

    void batchReport(List<Bundle> bundles);

    /**
     * 对于异步本来不应该放到接口里，但是为了之前的兼容暂时放在里面
     */
    void asynReport(List<Bundle> bundles, ReportLimit limit);

    void asynReport(List<Bundle> bundles);
}
