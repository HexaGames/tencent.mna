/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.tcpstatistics;

import android.os.Build;
import android.os.Bundle;

import com.tencent.cloudsdk.report.core.ReportConstanst;
import com.tencent.cloudsdk.report.core.ReportImp;
import com.tencent.cloudsdk.report.imp.StatisticReportLimit;
import com.tencent.cloudsdk.report.imp.StatisticsReportConfig;
import com.tencent.cloudsdk.report.tool.ReportTool;
import com.tencent.cloudsdk.utils.PrintHandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 关于TCP统计的上报的执行
 */
public class TcpStatisticsReportImp implements ReportImp {
    private static final String TAG = "TcpStatisticsReportImp";

    public static final String KEY_TYPE_ID = "typeid";
    public static final String TIME = "time";
    public static final String DEVICE_MODE = "dt";
    public static final String DOMAIN = "dm";
    public static final String DEVICE_TYPE = "mt";
    public static final String SYSTEM_VERSION = "sv";
    public static final String REPORT_METHOD = "rm";
    public static final String IP = "ip";
    public static final String IP_TYPE = "ipt";
    public static final String CONN_TIMES = "ct";
    public static final String CONN_FAIL_TIMES = "cf";
    public static final String RECV_TIMES = "rt";
    public static final String RECV_FAIL_TIMES = "rf";
    public static final String SEND_TIMES = "st";
    public static final String SEND_FAIL_TIMES = "sf";

    public static final String RECV_1K_TIMES = "rt0";
    public static final String RECV_1K_DELAY = "rl0";
    public static final String SEND_1K_TIMES = "st0";
    public static final String SEND_1K_FAIL_TIMES = "sf0";
    public static final String SEND_1K_DELAY = "sl0";
    public static final String RECV_4K_TIMES = "rt1";
    public static final String RECV_4K_DELAY = "rl1";
    public static final String SEND_4K_TIMES = "st1";
    public static final String SEND_4K_FAIL_TIMES = "sf1";
    public static final String SEND_4K_DELAY = "sl1";
    public static final String RECV_10K_TIMES = "rt2";
    public static final String RECV_10K_DELAY = "rl2";
    public static final String SEND_10K_TIMES = "st2";
    public static final String SEND_10K_FAIL_TIMES = "sf2";
    public static final String SEND_10K_DELAY = "sl2";
    public static final String RECV_1M_TIMES = "rt3";
    public static final String RECV_1M_DELAY = "rl3";
    public static final String SEND_1M_TIMES = "st3";
    public static final String SEND_1M_FAIL_TIMES = "sf3";
    public static final String SEND_1M_DELAY = "sl3";
    public static final String RECV_OTHER_TIMES = "rt4";
    public static final String RECV_OTHER_SIZE = "rs4";
    public static final String RECV_OTHER_DELAY = "rl4";
    public static final String SEND_OTHER_TIMES = "st4";
    public static final String SEND_OTHER_FAIL_TIMES = "sf4";
    public static final String SEND_OTHER_DELAY = "sl4";

    public static final int REPORT_METHOD_ALL = 1;
    public static final int REPORT_METHOD_SIMPLE = 0;
    public static final int TYPE_ID = 1000200;

    private TcpStatisticsData mData;
    private StatisticsReportConfig mReportConfig;
    private ReportTool mReportTool;
    private StatisticReportLimit mReportLimit;

    public TcpStatisticsReportImp(TcpStatisticsData data, StatisticsReportConfig reportConfig, ReportTool tcpReportTool) {
        mData = data;
        mReportConfig = reportConfig;
        mReportTool = tcpReportTool;
        mReportLimit = new StatisticReportLimit();
    }

    @Override
    public void doOnNetworkInvalid() {
        mData.clear();
    }

    /**
     * 执行TCP上报
     */
    @Override
    public void report() {
        reportAll(false);
    }

    /**
     * 以精简的方式执行TCP上报
     */
    @Override
    public void reportSimply() {
        reportAll(true);
    }

    private void reportAll(boolean isSimple) {
        List<Bundle> bundles = new ArrayList<Bundle>();
        Map<String, TcpStatisticsItem> data = mData.getData();
        if (data == null || data.size() <= 0) {
            return;
        }
        // 虽然Map是线程同步的，但是仍然可能出现ConcurrentModificationException问题，这里复制一份。
        data = new LinkedHashMap<String, TcpStatisticsItem>(data);

        for (Map.Entry<String, TcpStatisticsItem> entry : data.entrySet()) {
            TcpStatisticsItem value = entry.getValue();
            long now = System.currentTimeMillis();
            if (now - value.time > mReportConfig.getClearInterval()) {
                continue;
            }

            Bundle bundle = createBundle(value, isSimple);
            bundles.add(bundle);
        }

        PrintHandler.addLog(TAG, "上报并清空TCP统计数据：" + bundles);
        mData.clear();
        mReportTool.asynReport(bundles, mReportLimit);
    }

    private Bundle createBundle(TcpStatisticsItem value, boolean simple) {
        Bundle bundle = new Bundle(40);
        bundle.putInt(KEY_TYPE_ID, TYPE_ID);
        bundle.putLong(TIME, value.time / 1000);
        bundle.putString(DEVICE_MODE, Build.MODEL);
        bundle.putString(DOMAIN, value.domain);
        bundle.putInt(DEVICE_TYPE, ReportConstanst.OS);
        bundle.putString(SYSTEM_VERSION, android.os.Build.VERSION.RELEASE);
        bundle.putInt(REPORT_METHOD, simple ? REPORT_METHOD_SIMPLE : REPORT_METHOD_ALL);
        bundle.putLong(IP, value.ip);
        bundle.putInt(IP_TYPE, value.ipType);
        bundle.putInt(CONN_TIMES, value.connectTimes);
        bundle.putInt(CONN_FAIL_TIMES, value.connectFailTimes);
        bundle.putInt(RECV_TIMES, value.recvTimes);
        bundle.putInt(RECV_FAIL_TIMES, value.recvFailTimes);
        bundle.putInt(SEND_TIMES, value.sendTimes);
        bundle.putInt(SEND_FAIL_TIMES, value.sendFailTimes);

        if (!simple) {
            bundle.putInt(RECV_1K_TIMES, value.recv1KTimes);
            int succCount = value.recv1KTimes;
            bundle.putInt(RECV_1K_DELAY, succCount <= 0 ? 0 : value.recv1KTotalDelay / succCount);
            bundle.putInt(SEND_1K_TIMES, value.send1KTimes);
            bundle.putInt(SEND_1K_FAIL_TIMES, value.send1KFailTimes);
            succCount = value.send1KTimes - value.send1KFailTimes;
            bundle.putInt(SEND_1K_DELAY, succCount <= 0 ? 0 : value.send1KTotalDelay / succCount);

            bundle.putInt(RECV_4K_TIMES, value.recv4KTimes);
            succCount = value.recv4KTimes;
            bundle.putInt(RECV_4K_DELAY, succCount <= 0 ? 0 : value.recv4KTotalDelay / succCount);
            bundle.putInt(SEND_4K_TIMES, value.send4KTimes);
            bundle.putInt(SEND_4K_FAIL_TIMES, value.send4KFailTimes);
            succCount = value.send4KTimes - value.send4KFailTimes;
            bundle.putInt(SEND_4K_DELAY, succCount <= 0 ? 0 : value.send4KTotalDelay / succCount);

            bundle.putInt(RECV_10K_TIMES, value.recv10KTimes);
            succCount = value.recv10KTimes;
            bundle.putInt(RECV_10K_DELAY, succCount <= 0 ? 0 : value.recv10KTotalDelay / succCount);
            bundle.putInt(SEND_10K_TIMES, value.send10KTimes);
            bundle.putInt(SEND_10K_FAIL_TIMES, value.send10KFailTimes);
            succCount = value.send10KTimes - value.send10KFailTimes;
            bundle.putInt(SEND_10K_DELAY, succCount <= 0 ? 0 : value.send10KTotalDelay / succCount);

            bundle.putInt(RECV_1M_TIMES, value.recv1MTimes);
            succCount = value.recv1MTimes;
            bundle.putInt(RECV_1M_DELAY, succCount <= 0 ? 0 : value.recv1MTotalDelay / succCount);
            bundle.putInt(SEND_1M_TIMES, value.send1MTimes);
            bundle.putInt(SEND_1M_FAIL_TIMES, value.send1MFailTimes);
            succCount = value.send1MTimes - value.send1MFailTimes;
            bundle.putInt(SEND_1M_DELAY, succCount <= 0 ? 0 : value.send1MTotalDelay / succCount);

            bundle.putInt(RECV_OTHER_TIMES, value.recvOtherTimes);
            succCount = value.recvOtherTimes;
            bundle.putInt(RECV_OTHER_DELAY, succCount <= 0 ? 0 : value.recvOtherTotalDelay / succCount);
            bundle.putLong(RECV_OTHER_SIZE, succCount <= 0 ? 0 : value.recvOtherTotalSize / succCount);
            bundle.putInt(SEND_OTHER_TIMES, value.sendOtherTimes);
            bundle.putInt(SEND_OTHER_FAIL_TIMES, value.sendOtherFailTimes);
            succCount = value.sendOtherTimes - value.sendOtherFailTimes;
            bundle.putInt(SEND_OTHER_DELAY, succCount <= 0 ? 0 : value.sendOtherTotalDelay / succCount);
        }

        return bundle;
    }

    /**
     * 为测试用，请勿调用
     */
    void testReportAll(boolean simple) {
        reportAll(simple);
    }

    /**
     * 为测试用，请勿调用
     */
    Bundle testCreateBundle(TcpStatisticsItem value, boolean simple) {
        return createBundle(value, simple);
    }
}
