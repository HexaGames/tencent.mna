/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.tcpstatistics;

/**
 * TCP统计上报的某一项
 */
public class TcpStatisticsItem {
    public String domain;
    public long ip;

    public int ipType;

    public long time;

    // 关于连接
    public int connectTimes;
    public int connectFailTimes;

    // 关于接收
    public int recvTimes;
    public int recvFailTimes;

    public int recv1KTimes;
    public int recv1KTotalDelay;

    public int recv4KTimes;
    public int recv4KTotalDelay;

    public int recv10KTimes;
    public int recv10KTotalDelay;

    public int recv1MTimes;
    public int recv1MTotalDelay;

    public int recvOtherTimes;
    public long recvOtherTotalSize;
    public int recvOtherTotalDelay;

    // 关于发送
    public int sendTimes;
    public int sendFailTimes;

    public int send1KTimes;
    public int send1KFailTimes;
    public int send1KTotalDelay;

    public int send4KTimes;
    public int send4KFailTimes;
    public int send4KTotalDelay;

    public int send10KTimes;
    public int send10KFailTimes;
    public int send10KTotalDelay;

    public int send1MTimes;
    public int send1MFailTimes;
    public int send1MTotalDelay;

    public int sendOtherTimes;
    public int sendOtherFailTimes;
    public int sendOtherTotalDelay;

    @Override
    public String toString() {
        return new StringBuilder("domain=").append(domain).append(",\n")
                .append("ip=").append(ip).append(",\n")
                .append("ipType=").append(ipType).append(",\n")
                .append("time=").append(time).append(",\n")
                .append("connectTimes=").append(connectTimes).append(",\n")
                .append("connectFailTimes=").append(connectFailTimes).append(",\n")
                .append("recvTimes=").append(recvTimes).append(",\n")
                .append("recvFailTimes=").append(recvFailTimes).append(",\n")
                .append("recv1KTimes=").append(recv1KTimes).append(",\n")
                .append("recv1KTotalDelay=").append(recv1KTotalDelay).append(",\n")
                .append("recv4KTimes=").append(recv4KTimes).append(",\n")
                .append("recv4KTotalDelay=").append(recv4KTotalDelay).append(",\n")
                .append("recv10KTimes=").append(recv10KTimes).append(",\n")
                .append("recv10KTotalDelay=").append(recv10KTotalDelay).append(",\n")
                .append("recv1MTimes=").append(recv1MTimes).append(",\n")
                .append("recv1MTotalDelay=").append(recv1MTotalDelay).append(",\n")
                .append("recvOtherTimes=").append(recvOtherTimes).append(",\n")
                .append("recvOtherTotalSize=").append(recvOtherTotalSize).append(",\n")
                .append("recvOtherTotalDelay=").append(recvOtherTotalDelay).append(",\n")
                .append("sendTimes=").append(sendTimes).append(",\n")
                .append("sendFailTimes=").append(sendFailTimes).append(",\n")
                .append("send1KTimes=").append(send1KTimes).append(",\n")
                .append("send1KFailTimes=").append(send1KFailTimes).append(",\n")
                .append("send1KTotalDelay=").append(send1KTotalDelay).append(",\n")
                .append("send4KTimes=").append(send4KTimes).append(",\n")
                .append("send4KFailTimes=").append(send4KFailTimes).append(",\n")
                .append("send4KTotalDelay=").append(send4KTotalDelay).append(",\n")
                .append("send10KTimes=").append(send10KTimes).append(",\n")
                .append("send10KFailTimes=").append(send10KFailTimes).append(",\n")
                .append("send10KTotalDelay=").append(send10KTotalDelay).append(",\n")
                .append("send1MTimes=").append(send1MTimes).append(",\n")
                .append("send1MFailTimes=").append(send1MFailTimes).append(",\n")
                .append("send1MTotalDelay=").append(send1MTotalDelay).append(",\n")
                .append("sendOtherTimes=").append(sendOtherTimes).append(",\n")
                .append("sendOtherFailTimes=").append(sendOtherFailTimes).append(",\n")
                .append("sendOtherTotalDelay=").append(sendOtherTotalDelay).toString();
    }
}
