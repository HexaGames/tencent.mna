/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */


package com.tencent.cloudsdk.report.tcpstatistics;

import com.tencent.cloudsdk.cache.MemoryCache;
import com.tencent.cloudsdk.utils.IpUtils;
import com.tencent.cloudsdk.utils.PrintHandler;

import java.util.Collections;
import java.util.Map;

/**
 * TCP统计数据的缓存
 */
public class TcpStatisticsData {
    private static final String TAG = "TcpStatisticsData";
    public static final int RET_FAIL = -1;
    public static final int RET_SUCC = 0;

    private Map<String, TcpStatisticsItem> mData = Collections.synchronizedMap(new MemoryCache<String, TcpStatisticsItem>(64));

    /**
     * 统计连接
     */
    public void statisticConn(String domain, String ip, int ipType, int ret) {
        if (!checkParam(domain, ip, ipType)) {
            return;
        }

        TcpStatisticsItem item = insertItemIfNeed(domain, ip, ipType);
        item.time = System.currentTimeMillis();
        if (ret == RET_FAIL) {
            item.connectFailTimes++;
        }
        item.connectTimes++;
    }

    public void clear() {
        mData.clear();
    }

    /**
     * 统计发包
     */
    public void statisticSend(String domain, String ip, int ipType, int ret, long size, int delay) {
        if (!checkParam(domain, ip, ipType)) {
            return;
        }

        TcpStatisticsItem item = insertItemIfNeed(domain, ip, ipType);
        item.time = System.currentTimeMillis();
        if (ret != RET_SUCC) {
            item.sendFailTimes++;
        }
        item.sendTimes++;

        if (size > 0 && size <= 1024) {
            if (ret != RET_SUCC) {
                item.send1KFailTimes++;
            } else {
                item.send1KTotalDelay = delay + item.send1KTotalDelay;
            }
            item.send1KTimes++;
        } else if (size > 1024 && size <= 4 * 1024) {
            if (ret != RET_SUCC) {
                item.send4KFailTimes++;
            } else {
                item.send4KTotalDelay = delay + item.send4KTotalDelay;
            }
            item.send4KTimes++;
        } else if (size > 4 * 1024 && size <= 10 * 1024) {
            if (ret != RET_SUCC) {
                item.send10KFailTimes++;
            } else {
                item.send10KTotalDelay = delay + item.send10KTotalDelay;
            }
            item.send10KTimes++;
        } else if (size > 10 * 1024 && size <= 1024 * 1024) {
            if (ret != RET_SUCC) {
                item.send1MFailTimes++;
            } else {
                item.send1MTotalDelay = delay + item.send1MTotalDelay;
            }
            item.send1MTimes++;
        } else {
            if (ret != RET_SUCC) {
                item.sendOtherFailTimes++;
            } else {
                item.sendOtherTotalDelay = delay + item.sendOtherTotalDelay;
            }
            item.sendOtherTimes++;
        }

        PrintHandler.addLog(TAG, "发送填充数据，最终" + domain + "的数据为：" + item);
    }

    /**
     * 统计收包
     */
    public void statisticRecv(String domain, String ip, int ipType, int ret, long size, int delay) {
        if (!checkParam(domain, ip, ipType)) {
            return;
        }

        TcpStatisticsItem item = insertItemIfNeed(domain, ip, ipType);
        item.time = System.currentTimeMillis();
        if (ret != RET_SUCC) {
            item.recvFailTimes++;
        }
        item.recvTimes++;

        // 如果为失败，按理size就是无效的。因此不进行下面的统计。
        if (ret == RET_FAIL) {
            PrintHandler.addLog(TAG, "接收填充数据，最终" + domain + "的数据为：" + item);
            return;
        }

        if (size > 0 && size <= 1024) {
            if (ret == RET_SUCC) {
                item.recv1KTotalDelay = item.recv1KTotalDelay + delay;
            }
            item.recv1KTimes++;
        } else if (size > 1024 && size <= 4 * 1024) {
            if (ret == RET_SUCC) {
                item.recv4KTotalDelay = item.recv4KTotalDelay + delay;
            }
            item.recv4KTimes++;
        } else if (size > 4 * 1024 && size <= 10 * 1024) {
            if (ret == RET_SUCC) {
                item.recv10KTotalDelay = item.recv10KTotalDelay + delay;
            }
            item.recv10KTimes++;
        } else if (size > 10 * 1024 && size <= 1024 * 1024) {
            if (ret == RET_SUCC) {
                item.recv1MTotalDelay = item.recv1MTotalDelay + delay;
            }
            item.recv1MTimes++;
        } else {
            if (ret == RET_SUCC) {
                item.recvOtherTotalDelay = item.recvOtherTotalDelay + delay;
                item.recvOtherTotalSize = item.recvOtherTotalSize + size;
            }
            item.recvOtherTimes++;
        }
        PrintHandler.addLog(TAG, "接收填充数据，最终" + domain + "的数据为：" + item);
    }

    public String getKey(String domain, String ip) {
        return new StringBuilder(domain).append(ip).toString();
    }

    Map<String, TcpStatisticsItem> getData() {
        return mData;
    }

    boolean checkParam(String domain, String ip, int ipType) {
        return domain != null
                && !domain.equals("")
                && !IpUtils.isIpFormat(domain)
                && ip != null
                && IpUtils.isIpFormat(ip)
                && ipType >= 0;
    }

    private TcpStatisticsItem insertItemIfNeed(String domain, String ip, int ipType) {
        String key = getKey(domain, ip);
        TcpStatisticsItem item = mData.get(key);
        if (item == null) {
            item = new TcpStatisticsItem();
            item.domain = domain;
            item.ip = IpUtils.parseIpAddress(ip);
            item.ipType = ipType;
            item.time = System.currentTimeMillis();
            mData.put(key, item);
        }

        return item;
    }
}
