/**
 * Tencent is pleased to support the open source community by making QcloudMna available.
 * Copyright (C) 2014 THL A29 Limited, a Tencent company. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */



package com.tencent.cloudsdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.tencent.cloudsdk.ansquery.AnsQueryConstants;
import com.tencent.cloudsdk.ansquery.QueryMain;
import com.tencent.cloudsdk.anssetting.AnsSetting;
import com.tencent.cloudsdk.anssetting.AnsSettingStorage;
import com.tencent.cloudsdk.cache.OcCacheManager;
import com.tencent.cloudsdk.data.AddressEntity;
import com.tencent.cloudsdk.data.OcAddress;
import com.tencent.cloudsdk.tsocket.GlobalContext;
import com.tencent.cloudsdk.utils.ClientIdManager;
import com.tencent.cloudsdk.utils.Common;
import com.tencent.cloudsdk.utils.PrintHandler;
import com.tencent.cloudsdk.utils.SpeedConfig;
import com.tencent.record.debug.WnsClientLog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends Activity implements OnItemSelectedListener {
    private static final String TAG = MainActivity.class.getName();

    private List<RSDomainItem> mRsItems = new ArrayList<RSDomainItem>();
    private RSDomainItem mCurItem;

    private WifiAdmin mWifiAdmin;
    private TextView mTestResult;

    private List<Test<RSDomainItem>> mTests = new ArrayList<Test<RSDomainItem>>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        populateTests();
        populateRSDomainItems();
        mCurItem = mRsItems.get(0);

        GlobalContext.setContext(this);
        PrintHandler.setHandler(mHandler);
        setContentView(R.layout.activity_main);
        mWifiAdmin = new WifiAdmin(this);

        mTestResult = (TextView) findViewById(R.id.tv_result);

        Spinner mRsDomainSpinner = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<RSDomainItem> adapter = new ArrayAdapter<RSDomainItem>(this,
                android.R.layout.simple_spinner_item, mRsItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mRsDomainSpinner.setAdapter(adapter);
        mRsDomainSpinner.setOnItemSelectedListener(this);

        setTitle(getTitle() + ", Client id = " + ClientIdManager.getInstance().getClientId());
        Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler(this));
    }

    public static class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        private WeakReference<MainActivity> activityWeakReference;

        MyUncaughtExceptionHandler(MainActivity activity) {
            activityWeakReference = new WeakReference<MainActivity>(activity);
        }

        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            WnsClientLog.e("MyUncaughtExceptionHandler", ex.getMessage(), ex);
            WnsClientLog.ensureLogsToFile();

            MainActivity activity = activityWeakReference.get();
            if (activity != null) {
                activity.finish();
            }

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            }, 2000);
        }
    }

    /**
     * 点击OC查询测试
     */
    public void onClickOcQueryTest(View v) {
        mTestResult.setText("");
        GlobalContext.setContext(MainActivity.this);
        boolean isHttp = mCurItem.type == RSDomainItem.TYPE_FOR_HTTP_SPEED_TEST
                || mCurItem.type == RSDomainItem.TYPE_FOR_HTTP_TEST;
        new QueryThread(mCurItem.domain, isHttp).start();
    }

    /**
     * 点击清空缓存
     */
    public void onClickClearCache(View v) {
        OcCacheManager.getInstance(GlobalContext.getContext()).clear();
        AnsSettingStorage ansSettingStorage = new AnsSettingStorage(getApplicationContext());
        ansSettingStorage.clear();
        mTestResult.setText("");
        PrintHandler.addLog(TAG, ">>>缓存清除成功！");
    }

    /**
     * 点击OC加速测试
     */
    public void onClickOCSpeedTest(View view) {
        mTestResult.setText("");
        new Thread() {
            public void run() {
                SpeedConfig.reset(GlobalContext.getContext(), mCurItem.domain);
                if (mCurItem.type == RSDomainItem.TYPE_FOR_HTTP_SPEED_TEST) {
                    HttpSpeedTest httpSpeedTest = new HttpSpeedTest();
                    httpSpeedTest.setData(mCurItem);
                    httpSpeedTest.doTest();
                } else if (mCurItem.type == RSDomainItem.TYPE_FOR_HTTP_TEST) {
                    HttpTest httpTest = new HttpTest();
                    httpTest.setData(mCurItem);
                    httpTest.doTest();
                } else if (mCurItem.type == RSDomainItem.TYPE_FOR_TCP_TEST) {
                    TcpTest tcpTest = new TcpTest();
                    tcpTest.setData(mCurItem);
                    tcpTest.doTest();
                }
            }
        }.start();
    }

    /**
     * 当点击统计时
     */
    public void onClickStatistic(View view) {
        Intent intent = new Intent(this, StatisticsTestActivity.class);
        startActivity(intent);
    }

    /**
     * 点击重置测速时间
     */
    public void onClickSpeedTimeReset(View view) {
        SpeedConfig.reset(GlobalContext.getContext(), mCurItem.domain);
        mTestResult.setText("");
        PrintHandler.addLog(TAG, ">>>测速时间已重置<<<");
    }

    /**
     * 点击打开关闭wifi
     */
    public void onClickSwitchWifi(View view) {
        if (Common.getNetworkType() == AnsQueryConstants.NETWORK_TYPE_WIFI) {
            mWifiAdmin.closeWifi();
        } else {
            mWifiAdmin.openWifi();
        }
    }

    /**
     * 点击停止用例
     */
    public void onClickStopCase(View v) {
        for (Test<RSDomainItem> test : mTests) {
            test.stop();
        }
    }

    /**
     * 点击运行用例
     */
    public void onClickRunCase(View v) {
        for (Test<RSDomainItem> test : mTests) {
            test.setData(mCurItem);
            test.stop();
            if (test.canRun()) {
                test.start();
            }
        }
    }

    /**
     * 点击重置版本号
     */
    public void onClickResetVersion(View v) {
        if (GlobalContext.getContext() != null) {
            SharedPreferences sp = GlobalContext.getContext().getSharedPreferences(AnsSetting.SPNAMW, Context.MODE_PRIVATE);
            Editor e = sp.edit();
            e.putInt(AnsSetting.SP_VER_KEY, 0);
            e.commit();
            AnsSetting.setVersion(0);
            mTestResult.setText("");
            PrintHandler.addLog(TAG, ">>>配置版本号已重置为0");
        }
    }

    /**
     * 点击加载用例文件
     */
    public void onClickLoadCaseFile(View view) {
        CaseRunner.GetInstance(MainActivity.this).LoadCaseFile();
    }

    /**
     * 当点击Spinner，选择某个源站后
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        WnsClientLog.v(TAG, "###onItemSelected: position=" + position);
        RSDomainItem item = (RSDomainItem) parent.getItemAtPosition(position);
        if (item == null) {
            return;
        }

        mCurItem = item;
    }

    /**
     * 当点击Spinner，没有选择某个源站后
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class QueryThread extends Thread {
        private String domain;
        private boolean isHttp;

        public QueryThread(String domain, boolean isHttp) {
            this.domain = domain;
            this.isHttp = isHttp;
        }

        @Override
        public void run() {
            OcAddress address = QueryMain.queryFromOC(domain, isHttp);
            if (address != null) {
                WnsClientLog.w(TAG, "domain:" + address.domain + " [networkType:" + address.sp
                        + "][expireTime:" + address.expireTime + "][retestTimeSpan:"
                        + address.retestTimeSpan + "]");
                for (AddressEntity add : address.addressList) {
                    PrintHandler.addLog(TAG, ">>>查询结果: [IP:" + add.getIp() + "]\n[port:" + add.getPort()
                            + "]\n[type:" + add.getType() + "]\n[sp:" + add.getSp() + "]\n[pri:"
                            + add.getPriority() + "]");
                }
            }
            super.run();
        }
    }

    public Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case PrintHandler.WHAT_ADD_LOG:
                    String msgOld = ((PrintHandler.Log) msg.obj).msg;
                    SpannableString msgNew = new SpannableString(msgOld);
                    String str = "((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)";
                    Pattern pat = Pattern.compile(str);
                    Matcher matcher = pat.matcher(msgOld);

                    if (matcher.find()) {
                        msgNew.setSpan(new ForegroundColorSpan(Color.BLUE), matcher.start(), matcher.end(), 0);
                        msgNew.setSpan(new BackgroundColorSpan(Color.LTGRAY), matcher.start(), matcher.end(), 0);
                    }

                    mTestResult.append(msgNew);
                    mTestResult.append("\n");

                    break;
                case PrintHandler.WHAT_CLEAR_LOG:
                    mTestResult.setText("");
                    break;
            }
        }

    };

    /**
     * 填充源站
     */
    private void populateRSDomainItems() {
        RSDomainItem item = new RSDomainItem();
        item.display = "ocspeed2.tc.qq.com:80 (TCP)";
        item.port = 80;
        item.type = RSDomainItem.TYPE_FOR_TCP_TEST;
        item.domain = "ocspeed2.tc.qq.com";
        mRsItems.add(item);

        item = new RSDomainItem();
        item.display = "cmatjrstest.tc.qq.com:8080 (TCP)";
        item.port = 8080;
        item.type = RSDomainItem.TYPE_FOR_TCP_TEST;
        item.domain = "cmatjrstest.tc.qq.com";
        mRsItems.add(item);

        item = new RSDomainItem();
        item.display = "mnatest.qcloud.com:8080 (HTTP)";
        item.port = 8080;
        item.type = RSDomainItem.TYPE_FOR_HTTP_TEST;
        item.domain = "mnatest.qcloud.com";
        mRsItems.add(item);

        item = new RSDomainItem();
        item.display = "tjrstest.tc.qq.com:80 (HTTP)";
        item.port = 80;
        item.type = RSDomainItem.TYPE_FOR_HTTP_TEST;
        item.domain = "tjrstest.tc.qq.com";
        mRsItems.add(item);

        item = new RSDomainItem();
        item.display = "tjrstest.tc.qq.com:80 (HTTP测速)";
        item.port = 80;
        item.type = RSDomainItem.TYPE_FOR_HTTP_SPEED_TEST;
        item.domain = "tjrstest.tc.qq.com";
        mRsItems.add(item);
    }

    private void populateTests() {
        mTests.add(new TcpTest());
        mTests.add(new HttpTest());
        mTests.add(new HttpSpeedTest());
    }

    /**
     * 源站的某一项
     */
    public static class RSDomainItem {
        public static final int TYPE_FOR_HTTP_TEST = 1;
        public static final int TYPE_FOR_HTTP_SPEED_TEST = 2;
        public static final int TYPE_FOR_TCP_TEST = 3;
        public static final int TYPE_FOR_CPLUS_TEST = 4;

        public String domain;
        public int port;
        public int type;
        public String display;

        @Override
        public String toString() {
            return display;
        }
    }
}
